#!/root/anaconda3/envs/my_env/bin/python

import pandas as pd
import sys
import os

# df =  pd.read_csv('http://data.ntpc.gov.tw/od/zipfiledl?oid=A7F473A4-A633-4D5A-8DA7-CB1E62597A08&ft=zip')

infile = sys.argv[1]
col = sys.argv[2]
filename = os.path.splitext(infile)[0]
# print(filename)
# sys.exit()
outfile = filename+'_parsed.csv'

df =  pd.read_csv(infile)
df = df[pd.notnull(df[col])]
name = df[col]
# name = df[df['Name']]
# print(type(name))

name.to_csv(outfile, index=False)
