# cwd: /Users/changteanhsu/Git/breaktime_projects
# public key must be stored at remote server first to avoid entering password
scp root@192.168.21.101:/home/lance/projects/ads-related/*.py ./ads-related/
scp root@192.168.21.101:/home/lance/projects/biz/*.py ./biz/
scp root@192.168.21.101:/home/lance/projects/dashboard/*.py ./dashboard/
scp root@192.168.21.101:/home/lance/projects/database/*.py ./database/
scp root@192.168.21.101:/home/lance/projects/dataloss/*.py ./dataloss/
scp root@192.168.21.101:/home/lance/projects/footprint-related/*.py ./footprint-related/
# copy all under google/
scp -r root@192.168.21.101:/home/lance/projects/google/* ./google/
scp root@192.168.21.101:/home/lance/projects/mariadb/*.py ./mariadb/
scp root@192.168.21.101:/home/lance/projects/nlp/*.py ./nlp/
scp root@192.168.21.101:/home/lance/projects/nlppages/*.py ./nlppages/
scp -r root@192.168.21.101:/home/lance/tools/* ./tools/
scp root@192.168.21.101:/home/lance/projects/webpages/*.py ./webpages/

git config --global user.name "Lance"
git config --global user.email "virtuouslycan@gmail.com"
git remote add origin git@gitlab.com:Lance0404/breaktime_projects.git
git add .
git commit -m "Commiting changes"
git push -u origin master

