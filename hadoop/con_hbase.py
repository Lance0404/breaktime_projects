'''
# filter-string ref:
https://stackoverflow.com/questions/18016773/using-multiple-column-filters-with-happybase

# operator ref:
http://hbase.apache.org/book.html#_compare_operator

# thrift
http://hbase.apache.org/book/thrift.html

Hadoop conf:
Edge Node:
192.168.21.191
Master Node:
192.168.21.201
192.168.21.202
Worder Note:
192.168.21.210
192.168.21.211
192.168.21.212
'''

import sys
import os
import happybase
# import binascii
sys.path.append('../tools/')
from common.logger import logger, dirmaster
filepath = os.path.abspath(__file__)
logger = logger(filepath)

logger.info('Start!')


# ================== params ==================
batch_size = 10000


# ============================================
logger.info(f'batch_size: {batch_size!s}')

connection = happybase.Connection('192.168.21.201', 9090)
table = connection.table('footprints_201805')

# column family
# cf = 'footprints_201805'
cf = 'footprints'

if True:
    urlkw = '電信服務'
    # urlkw = binascii.b2a_hex(b'電信服務')
    # syntax: SingleColumnValueFilter ('cf', 'col name', '(<, ⇐, =, !=, >, >=)', '(regexstring, substring)': expression )
    startdt = b'2018-05-06T09:25:26'
    enddr = ''
    f_list = [
        "SingleColumnValueFilter ('footprints','hostname',=,'regexstring:^zi.media$')",
        # f"SingleColumnValueFilter ('footprints','url',=,':^.*{urlkw}.*$')",
        # f"SingleColumnValueFilter ('footprints','url',=,'substring:{urlkw}')",
        # f"SingleColumnValueFilter ('footprints','updated',=,'regexstring:^{startdt}$')",
        # f"TimeStampsFilter ('footprints','updated',=,'binary:{startdt}')",
    ]

    if len(f_list) > 0:
        fstr = ' AND '.join([f'({i!s})' for i in f_list])
    logger.info(fstr)

    # sys.exit()
    scanner = table.scan(
        # limit=,
        batch_size=batch_size,
        filter=fstr,
        # returns every value as tuple (value, timestamp)
        include_timestamp=True
    )
    # b'footprints:@timestamp': (b'2018-05-06T15:16:24.000Z', 1528436742328),
    count = 0
    for key, data in scanner:
        key = key.decode("utf-8")
        # print(f'keys: {key!s}')
        print(data[b'footprints:updated'])
        break

        # print(data[b'footprints:updated'])
        # print(data['footprints:updated'])
        # for k, v in data.items():
        #     if k.decode("utf-8") == 'footprints:updated':
        #         #     print(k.decode("utf-8"))
        #         print(v.decode("utf-8"))
        #         print(v)
        #         break
        #     else:
        #         continue
        # break
        count += 1
        if count % 10000 == 0:
            logger.info(f'num of count: {count!s}')
    logger.info(f'num of count: {count!s}')


if 0:
    rowkeys = ['000000000000000000000000000000001525194441',
               '000000000000000000000000000000001525626909']
    cols = [f'{cf}:{i}' for i in ['@timestamp',
                                  '@version', 'hostname', 'title', 'meta_description', 'content', 'url']]
    print(cols)
    rows = table.rows(rowkeys, columns=cols)
    count = 0
    for key, data in rows:
        print(key, data)
        count += 1
        print(str(count))
        if count > 5:
            break

sys.exit()

logger.info('Done!')


# 之前找到也是用這個
# scan 'mobile01' , {FILTER => "SingleColumnValueFilter('mobile01','_type', =, 'substring:doc')"}


# hbase_table.scan(filter="SingleColumnValueFilter ('blah','blouh',=,'regexstring:^batman$')")
# keys: 000000000000000000000000000000001525194441
# keys: 000000000000000000000000000000001525626909
# keys: 0000396d3d9871a1846ea81de90285401525242224
# keys: 00003d02d164e28a1ed11326fd5050271525325561
# keys: 00003d02d164e28a1ed11326fd5050271525325569


# b'000000000000000000000000000000001525194441'
# b'000000000000000000000000000000001525626909'
# b'0000396d3d9871a1846ea81de90285401525242224'
# b'00003d02d164e28a1ed11326fd5050271525325561'
# b'00003d02d164e28a1ed11326fd5050271525325569'

# less tmp.txt | grep 'keys'
# keys: %{id}
# keys: 0000000b709f7a6594766e0caf01ed3a31687e42
# keys: 0000006bdad3430b3d6f080f057a19503125333e
# keys: 0000006ea4860be2ef3c70928191e63eaff14724
# keys: 0000007586a5b298178b2c5359b846effdb07e3f
