from confluent_kafka import Producer

# print(Producer.__doc__)

# p = Producer({'bootstrap.servers': 'localhost:9092'})
# p = Producer({'bootstrap.servers': '192.168.21.202:6667'})
# p.produce('mytopic', key='hello', value='world')
# ret = p.flush(30)
# print(ret)


def acked(err, msg):
    if err is not None:
        print("Failed to deliver message: {0}: {1}"
              .format(msg.value(), err.str()))
    else:
        print("Message produced: {0}".format(msg.value()))


# p = Producer({'bootstrap.servers': 'localhost:9092'})
p = Producer({'bootstrap.servers': '192.168.21.202:6667'})

try:
    for val in range(1, 1000):
        # p.produce('mytopic', 'myvalue #{0}'
                  # .format(val))
        # p.produce('mytopic', key='hello', value='world')
        p.produce('mytopic', 'myvalue #{0}'
                  .format(val), callback=acked)
        ret = p.poll(0.5)
        # print(ret)

except KeyboardInterrupt:
    pass

# p.flush(30)
