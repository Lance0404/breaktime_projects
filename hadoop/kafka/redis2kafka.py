'''
# redislabs
https://redislabs.com/lp/python-redis/
https://github.com/andymccurdy/redis-py

conda install redis
redis: 4.0.10-h14c3975_0
# Not correct

pip install redis
Installing collected packages: redis
Successfully installed redis-2.10.6
# Correct


'''

import redis
import json
import multiprocessing as mp
from kafka import KafkaProducer
import sys

# topic = 'lance-topic-v2'
# producer = KafkaProducer(
#     bootstrap_servers=['mn01.breaktime.com.tw:6667',
#                        'mn02.breaktime.com.tw:6667'],
#     # key_serializer=str.encode,
#     # value_serializer=lambda v: json.dumps(v).encode('utf-8'),
#     acks='all',
#     linger_ms=5,
# )


# bootstrap_servers = ['mn01.breaktime.com.tw:6667', 'mn02.breaktime.com.tw:6667']

# bootstrap_servers = ['35.229.128.166:6667']
bootstrap_servers = ['104.199.165.33:6667']


r = redis.Redis(
    # host='10.140.0.3',
    host='35.201.152.206',  # use private IP when deployed on production (GCP)
    port=6248,
    db=1,
    password='73d879c81dc9755f6cf0d121bbfc126a2a44fa1aee21388fb604e3c311a2b08a',
    # decode_responses=True,  # not sure if it's necessary
)

target_keys = [
    'profile_oath',
    'session_stay',
    'ads',
    'profile_id',
    'js_err',
    'openlink',
    'highlighted_text',
    'wifi',
    'footprint'
]

# topic = 'lance-topic-v2'
# topic = 'profile_oath'
# while 1:
#     producer.send(topic,
#                   value=b'redis to kafka'
#                   )

# sys.exit()


def to_kafka(key, datalist, producer):
    # global producer
    topic = key
    # topic = 'lance-topic-v2'
    # print(dir(producer))
    # print(producer.__dict__)
    for each in datalist:
        print(each)
        producer.send(topic,
                      # key=f'{count!s}',
                      value=each
                      )

    # producer.flush()
    # producer.close()


def fetch_redis(key):
    producer = KafkaProducer(
        bootstrap_servers=bootstrap_servers,
        # key_serializer=str.encode,
        # value_serializer=lambda v: json.dumps(v).encode('utf-8'),
        acks='all',
        linger_ms=5,
    )

    total = 0
    start = 0
    batch_size = 50
    while 1:
        end = start + batch_size - 1
        try:
            datalist = r.lrange(key, start, end)
        except:
            print('err when trying r.lrange()')
            raise

        # print(f'key: {key}, len of datalist: {len(datalist)!s}')
        # print(datalist)
        to_kafka(key, datalist, producer)
        # topic = 'lance-topic-v2'
        # for each in datalist:
        #     # print(each)
        #     producer.send(topic,
        #                   # key=f'{count!s}',
        #                   value=each
        #                   )
        # break
        total += len(datalist)
        # delete the element from the source
        if len(datalist) < batch_size:
            # r.delete(key) # dangerous, what if the key continues to grow during this operation
            # r.ltrim(key, len(datalist), -1)
            # print(f'final total processed doc: {total!s}')
            # print(f'total element count: {r.llen(key)!s}')
            break

        try:
            pass
            # r.ltrim(key, batch_size, -1)
        except:
            print('err when trying r.ltrim()')
            raise

        print(f'{key} total processed doc: {total!s}')
        print(f'{key} total element count: {r.llen(key)!s}')


if __name__ == '__main__':
    with mp.Pool(9) as pool:
        res = pool.map(fetch_redis, target_keys)
