'''
failed to consume msg from producer
'''
from confluent_kafka import Consumer, KafkaError

broker = '192.168.21.202:6667'
# broker = 'localhost:9092'
c = Consumer({
    'bootstrap.servers': broker,
    'group.id': 'mygroup',
    'default.topic.config': {
        'auto.offset.reset': 'smallest'
    },
    # 'debug': 'generic,broker,topic,metadata,cgrp,fetch'
    # 'debug': 'all'
})

c.subscribe([b'mytopic'])

while True:
    msg = c.poll(1.0)

    if msg is None:
        # print('msg is None!')
        continue
    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            continue
        else:
            print(msg.error())
            break

    print('Received message: {}'.format(msg.value().decode('utf-8')))

c.close()


# python confluent_kafka_consumer.py > tmp 2>&1
