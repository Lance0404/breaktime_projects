'''
ZK version: 3.4.6
kafka version: 1.0.0
kafka broker version: 1.0.0
kafka-python==1.4.3

'''

from kafka import KafkaConsumer
from kafka import TopicPartition
import sys

import json
consumer = KafkaConsumer(
    # 'lance_topic',
    bootstrap_servers=['mn01.breaktime.com.tw:6667',
                       'mn02.breaktime.com.tw:6667'],
    # auto_offset_reset='latest',  # or 'latest'
    group_id='lance_group',
    # value_deserializer=msgpack.loads,
    # value_deserializer=lambda m: json.loads(m.decode('ascii')),
    key_deserializer=bytes.decode,
    value_deserializer=bytes.decode,
    # api_version=(1, 0, 0)
)

consumer.subscribe(
    topics=['lance-topic-v2'],

)


print(dir(consumer))
print(consumer._use_consumer_group)
print(consumer.__dict__)
# {'config': {'bootstrap_servers': ['mn01.breaktime.com.tw:6667', 'mn02.breaktime.com.tw:6667'], 'client_id': 'kafka-python-1.4.3', 'group_id': 'lance_group', 'key_deserializer': <method 'decode' of 'bytes' objects>, 'value_deserializer': <method 'decode' of 'bytes' objects>, 'fetch_max_wait_ms': 500, 'fetch_min_bytes': 1, 'fetch_max_bytes': 52428800, 'max_partition_fetch_bytes': 1048576, 'request_timeout_ms': 305000, 'retry_backoff_ms': 100, 'reconnect_backoff_ms': 50, 'reconnect_backoff_max_ms': 1000, 'max_in_flight_requests_per_connection': 5, 'auto_offset_reset': 'latest', 'enable_auto_commit': True, 'auto_commit_interval_ms': 5000, 'default_offset_commit_callback': <function KafkaConsumer.<lambda> at 0x7fceca5a4f28>, 'check_crcs': True, 'metadata_max_age_ms': 300000, 'partition_assignment_strategy': (<class 'kafka.coordinator.assignors.range.RangePartitionAssignor'>, <class 'kafka.coordinator.assignors.roundrobin.RoundRobinPartitionAssignor'>), 'max_poll_records': 500, 'max_poll_interval_ms': 300000, 'session_timeout_ms': 10000, 'heartbeat_interval_ms': 3000, 'receive_buffer_bytes': None, 'send_buffer_bytes': None, 'socket_options': [(6, 1, 1)], 'sock_chunk_bytes': 4096, 'sock_chunk_buffer_count': 1000, 'consumer_timeout_ms': inf, 'skip_double_compressed_messages': False, 'security_protocol': 'PLAINTEXT', 'ssl_context': None, 'ssl_check_hostname': True, 'ssl_cafile': None, 'ssl_certfile': None, 'ssl_keyfile': None, 'ssl_crlfile': None, 'ssl_password': None, 'api_version': (1, 0, 0), 'api_version_auto_timeout_ms': 2000, 'connections_max_idle_ms': 540000, 'metric_reporters': [], 'metrics_num_samples': 2, 'metrics_sample_window_ms': 30000, 'metric_group_prefix': 'consumer', 'selector': <class 'selectors.EpollSelector'>, 'exclude_internal_topics': True, 'sasl_mechanism': None, 'sasl_plain_username': None, 'sasl_plain_password': None, 'sasl_kerberos_service_name': 'kafka'}, '_metrics': <kafka.metrics.metrics.Metrics object at 0x7fcecd33c160>, '_client': <kafka.client_async.KafkaClient object at 0x7fceca523c88>, '_subscription': <kafka.consumer.subscription_state.SubscriptionState object at 0x7fceca53cef0>, '_fetcher': <kafka.consumer.fetcher.Fetcher object at 0x7fceca53cd30>, '_coordinator': <kafka.coordinator.consumer.ConsumerCoordinator object at 0x7fceca2cc6a0>, '_closed': False, '_iterator': None, '_consumer_timeout': inf}

# sys.exit()
# print(consumer.topics)
# partition = TopicPartition('lance_topic', 2)
# consumer.assign([partition])

# # Deserialize msgpack-encoded values
# consumer = KafkaConsumer(value_deserializer=msgpack.loads)
# consumer.subscribe(['msgpackfoo'])
# for msg in consumer:
#     assert isinstance(msg.value, dict)


# count = 0
for message in consumer:
    # count += 1
    print(message)
    # print(count)
    # message value and key are raw bytes -- decode if necessary!
    # e.g., for unicode: `message.value.decode('utf-8')`
    print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                         message.offset, message.key,
                                         message.value))


# for msg in consumer:
#     print(msg)
