from kafka import KafkaProducer
import json
import sys

# topic = 'lance-topic-v2'
topic = 'profile_oath'
producer = KafkaProducer(
    bootstrap_servers=['mn01.breaktime.com.tw:6667',
                       'mn02.breaktime.com.tw:6667'],
    key_serializer=str.encode,
    value_serializer=lambda v: json.dumps(v).encode('utf-8'),
    acks='all',
    linger_ms=5,
)

# print(dir(producer))
# for i in range(100):

# ret = producer.partitions_for('lance_topic')
# print(ret)
# sys.exit()

count = 0
while 1:
    count += 1
    producer.send(topic,
                  key=f'{count!s}',
                  value='some_other_message_bytes'
                  )
    # print(str(count))

# producer.flush()
producer.close()

sys.exit()


# # Asynchronous by default
# future = producer.send('lance_topic', key='ping', value='raw_bytes')

# # Block for 'synchronous' sends
# try:
#     record_metadata = future.get(timeout=10)
# except KafkaError:
#     # Decide what to do if produce request failed...
#     log.exception()
#     pass
# # Successful result returns assigned partition and offset
# print(record_metadata.topic)
# print(record_metadata.partition)
# print(record_metadata.offset)


# # produce keyed messages to enable hashed partitioning
# producer.send('my-topic', key=b'foo', value=b'bar')
# # encode objects via msgpack
# producer = KafkaProducer(value_serializer=msgpack.dumps)
# producer.send('msgpack-topic', {'key': 'value'})
# # produce json messages
# producer = KafkaProducer(value_serializer=lambda m: json.dumps(m).encode('ascii'))
# producer.send('json-topic', {'key': 'value'})
# # produce asynchronously
# for _ in range(100):
# producer.send('my-topic', b'msg')
# def on_send_success(record_metadata):
#     print(record_metadata.topic)
#     print(record_metadata.partition)
#     print(record_metadata.offset)
# def on_send_error(excp):
# log.error('I am an errback', exc_info=excp)
# # handle exception
# # produce asynchronously with callbacks
# producer.send('my-topic', b'raw_bytes').add_callback(on_send_success).add_errback(on_
# ˓→send_error)
# # block until all async messages are sent
# producer.flush()
# # configure multiple retries
# producer = KafkaProducer(retries=5)
