'''
HDP version: 2.6
ZK version: 3.4.6
kafka version: 1.0.0
kafka broker version: 1.0.0
kafka-python==1.4.3

'''

from kafka import KafkaConsumer
from kafka import TopicPartition
import sys

import json
consumer = KafkaConsumer(
    # 'lance_topic',
    bootstrap_servers=['mn01.breaktime.com.tw:6667',
                       'mn02.breaktime.com.tw:6667'],
    auto_offset_reset='latest',  # or 'latest'
    group_id='my_group',
    # value_deserializer=msgpack.loads,
    # value_deserializer=lambda m: json.loads(m.decode('ascii')),
    # key_deserializer=bytes.decode,
    # value_deserializer=bytes.decode,
    # api_version=(1, 0, 0)
)

topics = [
    'profile_oath',
    'session_stay',
    'ads',
    'profile_id',
    'js_err',
    'openlink',
    'highlighted_text',
    'wifi',
    'footprint',
    # 'lance-topic-v2',
]

consumer.subscribe(
    topics=topics,
    # topics=['lance-topic-v2'],
)

# print(consumer.__dict__)
# print(dir(consumer))
# print(consumer.topics)
# sys.exit()
for message in consumer:
    # count += 1
    # print(message)
    # print(count)
    # message value and key are raw bytes -- decode if necessary!
    # e.g., for unicode: `message.value.decode('utf-8')`
    print("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                         message.offset,
                                         message.key,
                                         message.value))
