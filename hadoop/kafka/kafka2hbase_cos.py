'''
HDP version: 2.6
ZK version: 3.4.6
kafka version: 1.0.0
kafka broker version: 1.0.0

hbase rowkey design: http://hbase.apache.org/0.94/book/rowkey.design.html
(Long.MAX_VALUE - timestamp)
[key][reverse_timestamp]
'''
from confluent_kafka import Consumer, KafkaError
import sys
import json
from time import sleep
from os.path import abspath
from common import logger
import multiprocessing as mp
import happybase

filepath = abspath(__file__)
logger = logger(filepath)
logger.info('Start!')

# kafka on GCP
bootstrap_servers = '104.155.193.62:9092'
# print(consumer.__dict__)
# sys.exit()

topics = [
    'profile_oath',
    'session_stay',
    'ads',
    'open_id',
    'js_err',
    'openlink',
    'highlighted_text',
    'wifi',
    'footprint',
]


def check_hb_tables(topics):
    conn = happybase.Connection('192.168.21.201', 9090)
    exists = [i.decode() for i in conn.tables()]
    print(exists)
    for i in [f'test_{i}' for i in topics]:
        families = {}
        families = {
            i: dict()
        }
        if i not in exists:
            conn.create_table(i, families)
    exists = [i.decode() for i in conn.tables()]
    print(exists)


def to_hbase(topic, datalist, conn=None):
    prefix = f'test_{topic}'
    table = conn.table(prefix)
    for msg in datalist:
        msg_json = msg.value().decode('utf-8')
        # print(msg_json)
        msg_dict = json.loads(msg_json)
        row = {}
        row_key = "test"  # how should i generate this?
        for key, value in msg_dict.items():
            # print(f'{key} -> {value}')
            if isinstance(value, list) or isinstance(value, tuple) or isinstance(value, dict):
                value = json.dumps(value, ensure_ascii=False).encode('utf8')
            elif isinstance(value, (int, float)):
                value = str(value)
            row[prefix + ":" + key] = value
        # print(row_key, row)
        table.put(row_key, row)


def consume_kafka(topic):
    logger.info(f'start fetching topic: {topic}')
    connection = happybase.Connection('192.168.21.201', 9090)
    # local variable
    topiclist = [topic]  # 1 item list
    c = Consumer({
        'bootstrap.servers': bootstrap_servers,
        'group.id': 'newgroup',
        'default.topic.config': {
            'auto.offset.reset': 'smallest'
        }
    })
    c.subscribe(topiclist)

    count = 0
    datalist = []
    while True:
        count += 1
        msg = c.poll(1.0)

        if msg is None:
            continue
        if msg.error():
            if msg.error().code() == KafkaError._PARTITION_EOF:
                continue
            else:
                print(msg.error())
                break

        datalist.append(msg)
        # batch size = 1000
        if count % 5 == 0:
            to_hbase(topic, datalist, connection)
            datalist = []

        print('Received message: {}'.format(msg.value().decode('utf-8')))
        if count % 10000 == 0:
            logger.info(f'num of doc proccessed for {topic}: {count!s}')
            count = 0

    c.close()


if __name__ == '__main__':
    check_hb_tables(topics)
    # sys.exit()
    with mp.Pool(9) as pool:
        res = pool.map(consume_kafka, topics)

    logger.info('Done!')
