'''
# redislabs
https://redislabs.com/lp/python-redis/
https://github.com/andymccurdy/redis-py

conda install redis
redis: 4.0.10-h14c3975_0
# Not correct

pip install redis
Installing collected packages: redis
Successfully installed redis-2.10.6
# Correct

Using Hiredis can provide up to a 10x speed improvement in parsing responses from the Redis server
pip install hiredis

'''

import redis
import json
# r = redis.Redis(
#     host='hostname',
#     port=port,
#     password='password')


r = redis.Redis(
    # host='10.140.0.3',
    host='35.201.152.206',  # use private IP when deployed on production (GCP)
    port=6248,
    db=1,
    password='73d879c81dc9755f6cf0d121bbfc126a2a44fa1aee21388fb604e3c311a2b08a',
    decode_responses=True,  # not sure if it's necessary
    # url='redis://:73d879c81dc9755f6cf0d121bbfc126a2a44fa1aee21388fb604e3c311a2b08a@10.140.0.3:6248/1',

)

# 9 indexes
target_keys = [
    # 'profile_oath',
    # 'session_stay',
    'ads',
    # 'profile_id',
    # 'js_err',
    # 'openlink',
    # 'highlighted_text',
    # 'wifi',
    # 'footprint'
]

def fetch_redis(key):
    pass

for key in target_keys:

    # # 1.
    # for index in range(r.llen(key)):
    #     print(r.lindex(key, index))
    #     # print(r.lindex(key, index).decode())

    # 2. return a list
    # print(r.lrange(key, 0, -1))
    # datalist = r.lrange(key, 0, -1)
    # datalist = r.lrange(key, 0, 0 + 1)
    # print(datalist)
    # print(f'{len(datalist)!s}')

    # # json str to dict
    # for doc in datalist:
    #     # print(type(doc))
    #     data = json.loads(doc)
    #     print(dir(data))
    #     print(data)
    #     print(data.keys())
    #     # print(data.values())
    #     break

    # get a batch size of docs from redis and delete
    total = 0
    start = 0
    batch_size = 50
    while 1:
        end = start + batch_size - 1
        try:
            datalist = r.lrange(key, start, end)
        except:
            print('err when trying r.lrange()')
            raise

        print('len of list: ' + str(len(datalist)))
        # print(datalist)
        total += len(datalist)
        # delete the element from the source
        if len(datalist) < batch_size:
            # r.delete(key) # dangerous, what if the key continues to grow during this operation
            r.ltrim(key, len(datalist), -1)
            print(f'final total processed doc: {total!s}')
            print(f'total element count: {r.llen(key)!s}')
            break

        try:
            r.ltrim(key, batch_size, -1)
        except:
            print('err when trying r.ltrim()')
            raise

        print(f'total processed doc: {total!s}')
        print(f'total element count: {r.llen(key)!s}')
        # break
        # print(start, end)
        # print(len(datalist))
        # start = end + 1


# print(r)
# print(r.__dict__)
# print(dir(r))
# print(r.keys)
# print(dir(r.keys))
# print(r.llen('ads'))
# print(r.lindex('ads', 3))
# print(r.get('ads'))
