
polarbear > redis db 0
catalyst > redis db 1

# GCP server list
https://console.cloud.google.com/networking/addresses/list?authuser=0&hl=zh-tw&organizationId=842707922564&project=stage-data-alchemy-183102

# connect Sophie's Redis
https://github.com/BreakTimeTaiwan/catalyst/blob/master/catalyst/__init__.py
https://github.com/BreakTimeTaiwan/catalyst/blob/master/catalyst/api/utils.py
https://github.com/BreakTimeTaiwan/catalyst/blob/master/docker_stg/catalyst.env

REDIS_URL=redis://:73d879c81dc9755f6cf0d121bbfc126a2a44fa1aee21388fb604e3c311a2b08a@10.140.0.3:6248/1


# ssh from local
redis-cli -p 6248 -h 35.201.152.206 -a 73d879c81dc9755f6cf0d121bbfc126a2a44fa1aee21388fb604e3c311a2b08a


# useful ssh redis shell cmd

lrange profile_oath 0 -1
lrange footprints 0 -1
key *
# view specific DB
SELECT 1
FLUSHALL

LRANGE ads 0 -1


35.201.152.206:6248[1]> keys *
 1) "profile_oath"
 2) "session_stay"
 3) "ads"
 4) "profile_id"
 5) "RATELIMIT:/session_stay:640e1b221169446ecb20fabad4f0e2c402cbcb0e"
 6) "RATELIMIT:/profile_oath:ddbf41cc549c08ced9ad902bb0714cf532259c16"
 7) "RATELIMIT:/footprint:ddbf41cc549c08ced9ad902bb0714cf532259c16"
 8) "js_err"
 9) "openlink"
10) "highlighted_text"
11) "wifi"
12) "RATELIMIT:/cat_trid:ddbf41cc549c08ced9ad902bb0714cf532259c16"
13) "RATELIMIT:/ads:ddbf41cc549c08ced9ad902bb0714cf532259c16"
14) "footprint"

# todo list:
how to delete records from kafka or expire them?
failed to set consumer_group with pykafka

replace python kafka client with fluentd (e.g. logstesh)
replace redis with kafka?


# design logics:
不同 index(from Redis) 對應不同 topic(to Kafka)

import data from redis to kafka
if succeed, delect from redis

# redis persistence
https://redis.io/topics/persistence

# redis conf
https://raw.githubusercontent.com/antirez/redis/2.8/redis.conf
