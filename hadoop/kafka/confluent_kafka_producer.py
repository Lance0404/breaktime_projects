from confluent_kafka import Producer

broker = '192.168.21.202:6667'
# broker = 'localhost:9092'
p = Producer({'bootstrap.servers': broker,
              # 'debug': 'all'
              })


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print('Message delivered to {} [{}] offset: {}, key: {}, value: {}'.format(
            msg.topic(), msg.partition(), msg.offset(), msg.key(), msg.value()))


some_data_source = [
    'i love rabbit',
    'i love bear',
    'hello kafka'
]


for data in some_data_source:
    print(len(p))
    # Trigger any available delivery report callbacks from previous produce() calls
    p.poll(0)

    # Asynchronously produce a message, the delivery report callback
    # will be triggered from poll() above, or flush() below, when the message has
    # been successfully delivered or failed permanently.
    p.produce('mytopic', data.encode('utf-8'), callback=delivery_report)

print(len(p))
# Wait for any outstanding messages to be delivered and delivery report
# callbacks to be triggered.
p.flush()
print(len(p))
