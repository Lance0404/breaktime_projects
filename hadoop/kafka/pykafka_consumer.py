'''
it works!

Issue:
1. 每一次重啟都會從頭撈，從 mq 的第一筆開始撈
2. consumer_group 設定不成功，無法 commit 新的 offset 回去
'''
import sys
sys.path.insert(0, '/home/lance/Git/breaktime_projects/pykafka/')
from pykafka import KafkaClient
from pykafka.utils import deserialize_utf8
client = KafkaClient(
    hosts="mn01.breaktime.com.tw:6667,mn02.breaktime.com.tw:6667")
import logging
logging.basicConfig(level=logging.NOTSET)


def simple_consume():
    # topic = client.topics['my_topic']
    topic = client.topics[b'lance_topic']
    # print(dir(topic))
    # print(topic.name())

    # consumer_group = b'mygroup'
    # print(consumer_group)
    # sys.exit()
    consumer = topic.get_simple_consumer(
        consumer_group=None,  # how to make use of the consumer_group
        # use_rdkafka=True,
        use_rdkafka=False,
        deserializer=deserialize_utf8,
        # auto_offset_reset='earliest',
        # reset_offset_on_start=True,
    )
    # print(dir(consumer))
    # 'cleanup', 'commit_offsets', 'consume', 'consumer_id', 'fetch', 'fetch_offsets', 'generation_id', 'held_offsets', 'partition_cycle', 'partitions', 'reset_offsets', 'start', 'stop', 'topic']

    # for k, v in consumer.__dict__.items():
    #     print(k, v)

    # print(consumer.getattr())
    # print(consumer.__dict__)
    # consumer._deserializer = deserialize_utf8
    for message in consumer:
        #     # print(dir(message))
        #     # ['VALID_TS_TYPES', '__class__', '__delattr__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__len__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__slots__', '__str__', '__subclasshook__', 'compression_type', 'decode', 'delivery_report_q', 'offset', 'pack_into', 'partition', 'partition_id', 'partition_key', 'produce_attempt', 'protocol_version', 'set_timestamp', 'timestamp', 'timestamp_dt', 'value']
        # break
        if message is not None:
            print(message.offset, message.value, message.partition_key)
            consumer.consume()


def simple_consume2():
    '''
    ERROR:pykafka.cluster:Error discovering offset manager - GroupCoordinatorNotAvailable.
    DEBUG:pykafka.cluster:Retrying offset manager discovery
    '''
    topic = client.topics[b'my_topic']

    consumer = topic.get_simple_consumer(
        consumer_group='mygroup',
        # consumer_group=None,
        # auto_offset_reset='latest',
        auto_offset_reset=195000,
        reset_offset_on_start=True,
        # deserializer=deserialize_utf8
    )
    # for msg in consumer:
    #     if msg is not None:
    #         # attrlist = ['compression_type', 'decode', 'delivery_report_q', 'offset', 'pack_into', 'partition', 'partition_id',
    #         #             'partition_key', 'produce_attempt', 'protocol_version', 'set_timestamp',
    #         #             'timestamp',
    #         #             # 'timestamp_dt',
    #         #             'value'
    #         #             ]
    #         # for k in attrlist:
    #         #     print(f'attr: {k!s}, value: {getattr(msg, k)!s}')
    #         # print(dir(msg))
    #         print(msg.offset, msg.value, msg.partition_key)
    #         break
    consumer.consume()
    consumer.commit_offsets()


def balanced_consume():
    topic = client.topics[b'my_topic']

    balanced_consumer = topic.get_balanced_consumer(
        consumer_group='testgroup',
        managed=True,
        auto_commit_enable=True,
        # zookeeper_connect='192.168.21.202:2181'
        deserializer=deserialize_utf8
    )


# print(dir(balanced_consumer))
# ['__class__', '__del__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__iter__', '__le__', '__lt__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_add_partitions', '_add_self', '_auto_commit_enable', '_auto_commit_interval_ms', '_auto_offset_reset', '_brokers_changed', '_build_default_error_handlers', '_build_watch_callback', '_cluster', '_connection_id', '_consumer', '_consumer_group', '_consumer_id', '_consumer_timeout_ms', '_consumers_changed', '_default_error_handlers', '_deserializer', '_fetch_message_max_bytes', '_fetch_min_bytes', '_fetch_wait_max_ms', '_generation_id', '_get_held_partitions', '_get_internal_consumer', '_get_participants', '_group_coordinator', '_handle_error', '_heartbeat_interval_ms', '_internal_consumer_running', '_is_compacted_topic', '_join_group', '_membership_protocol', '_num_consumer_fetchers', '_offsets_channel_backoff_ms', '_offsets_commit_max_retries', '_partitions', '_path_from_partition', '_path_self', '_post_rebalance_callback', '_queued_max_messages', '_raise_worker_exceptions', '_rebalance', '_rebalance_backoff_ms', '_rebalance_max_retries', '_rebalancing_in_progress', '_rebalancing_lock', '_remove_partitions', '_reset_offset_on_fetch', '_reset_offset_on_start', '_running', '_set_watches', '_setup_heartbeat_worker', '_setup_internal_consumer', '_setup_zookeeper', '_sync_group', '_topic', '_topics_changed', '_update_member_assignment', '_use_rdkafka', '_worker_exception', 'commit_offsets', 'consume', 'held_offsets', 'partitions', 'reset_offsets', 'start', 'stop', 'topic']

    print(balanced_consumer.__dict__)


# {
#    '_cluster':   <pykafka.cluster.Cluster at 0x7f72e5ff7240 (hosts=192.168.21.202:6667   )>,
#    '_consumer_group':b'testgroup',
#    '_topic':<pykafka.topic.Topic at 0x7f72e1bfa940 (name=b'my_topic')>,
#    '_auto_commit_enable':True,
#    '_auto_commit_interval_ms':60000,
#    '_fetch_message_max_bytes':1048576,
#    '_fetch_min_bytes':1,
#    '_num_consumer_fetchers':1,
#    '_queued_max_messages':2000,
#    '_fetch_wait_max_ms':100,
#    '_consumer_timeout_ms':-1,
#    '_offsets_channel_backoff_ms':1000,
#    '_offsets_commit_max_retries':5,
#    '_auto_offset_reset':-2,
#    '_reset_offset_on_start':False,
#    '_rebalance_max_retries':5,
#    '_rebalance_backoff_ms':2000,
#    '_post_rebalance_callback':None,
#    '_is_compacted_topic':True,
#    '_membership_protocol':GroupMembershipProtocol(protocol_type=b'consumer',
#    protocol_name=b'range',
#    metadata=<pykafka.protocol.ConsumerGroupProtocolMetadata object at 0x7f72e1c45400>,
#    decide_partitions=<function decide_partitions_range at 0x7f72e1be0158>),
#    '_heartbeat_interval_ms':3000,
#    '_deserializer':<function deserialize_utf8 at 0x7f72e27f1950>,
#    '_reset_offset_on_fetch':True,
#    '_use_rdkafka':False,
#    '_generation_id':-1,
#    '_rebalancing_lock':<unlocked _thread.lock object at 0x7f72e1bf79e0>,
#    '_rebalancing_in_progress':<threading.Event object at 0x7f72e1bfaf28>,
#    '_internal_consumer_running':<threading.Event object at 0x7f72e1bfa320>,
#    '_connection_id':UUID('f63082a1-69e7-42f7-b3f0-c35cf468c5d5'),
#    '_consumer':None,
#    '_group_coordinator':None,
#    '_consumer_id':b'',
#    '_worker_exception':None,
#    '_default_error_handlers':{
#       15:<function ManagedBalancedConsumer._build_default_error_handlers.<locals>._handle_GroupCoordinatorNotAvailable at 0x7f72e1c00840>,
#       16:<function ManagedBalancedConsumer._build_default_error_handlers.<locals>._handle_NotCoordinatorForGroup at 0x7f72e1c00a60>,
#       14:None,
#       27:None,
#       22:None
#    },
#    '_running':False
# }

    # balanced_consumer._deserializer = deserialize_utf8
    # for message in balanced_consumer:
    #     if message is not None:
    #         print(message.offset, message.value)


if __name__ == '__main__':
    simple_consume()
    # simple_consume2()
    # balanced_consume()
