'''
Full documentation and usage examples
http://pykafka.readthedocs.io/en/latest/

Issue: Unicode support
https://github.com/Parsely/pykafka/issues/405

kafka version: 1.0.0
broker version: 0.9.0

todo:
produce key/val pairs

'''
import sys
# import datetime
import json
# download & use their lastest repo from github:
# https://github.com/Parsely/pykafka
sys.path.insert(0, '/home/lance/Git/breaktime_projects/pykafka/')
from pykafka import KafkaClient, partitioners
from pykafka.utils import serialize_utf8
from queue import Empty
# print(serialize_utf8.__doc__)

# mn01.breaktime.com.tw:6667
# mn02.breaktime.com.tw:6667

client = KafkaClient(
    hosts="mn01.breaktime.com.tw:6667,mn02.breaktime.com.tw:6667")
# print(dir(client))
# print(client.__dict__)
# print(KafkaClient.__doc__)
# sys.exit()

# print(datetime.datetime)

# topic_metadata ={
#     "name":
#     "partitions":
#     "err":
# }


def simple_produce():
    topic = client.topics[b'my_topic']
    with topic.get_sync_producer() as producer:
        # print(dir(producer))
        producer._serializer = serialize_utf8
        for i in range(10):
            producer.produce(
                'test message ' + str(i ** 2),
                timestamp=datetime.datetime
            )


def async_produce():
    topic = client.topics[b'lance_topic']

    print(dir(topic))
    print(topic.__dict__)
    print(topic._partitions)
    sys.exit()
    with topic.get_producer(
        delivery_reports=True,
        serializer=serialize_utf8,
        # partitioner=partitioners.HashingPartitioner,

    ) as producer:
        # producer._serializer = serialize_utf8
        # print(producer.__dict__)
        # print(dir(producer))
        # print(producer._cluster)
        # print(dir(producer._cluster))
        # print(producer._cluster._broker_version) # 0.9.0
        count = 0
        while True:
            if count == 100000:
                break
            count += 1
            data = {
                "count": count,
                "count^2": count ** 2
            }
            data = json.dumps(data)
            partition_key = f'{(count % 4)!s}'
            # partition_key = f'{count % 4}'
            # print(partition_key)
            # partition_key = count % 4
            try:
                producer.produce(
                    data,
                    # partition_key='{}'.format(count),
                    partition_key=partition_key,
                    # timestamp=datetime.datetime # (requires broker_version >= 0.10.0) ours is 0.9.0
                )
            except TypeError as e:
                print(dir(e))
                print(e.__dict__)
                print(e)
                # sys.exit(1)
            # msg = producer.produce(
            # 'test msg', partition_key='{}'.format(count))
            # print(dir(msg))
            # attrlist = ['compression_type', 'decode', 'delivery_report_q', 'offset', 'pack_into', 'partition', 'partition_id',
            #             'partition_key', 'produce_attempt', 'protocol_version', 'set_timestamp',
            #             'timestamp',
            #             # 'timestamp_dt',
            #             'value'
            #             ]
            # for k in attrlist:
            #     print(f'attr: {k!s}, value: {getattr(msg, k)!s}')
            # if count == 3:
            #     break
            # continue
            if count % 10 ** 2 == 0:  # adjust this or bring lots of RAM ;)
                while True:
                    try:
                        msg, exc = producer.get_delivery_report(block=False)
                        if exc is not None:
                            print('Failed to deliver msg {}: {}'.format(
                                msg.partition_key, repr(exc)))
                        else:
                            print('Successfully delivered msg {}'.format(
                                msg.partition_key))
                    except Empty:
                        break
                    # except Exception as e:
                    #     print(f'exception: {e!s}, count = {count!s}')
                    #     break


def produce_multi_topic():

    topics_str = [
        b'profile_oath',
        b'session_stay',
        b'ads',
        b'profile_id',
        b'js_err',
        b'openlink',
        b'highlighted_text',
        b'wifi',
        b'footprint'
    ]

    topics_to_produce_to = [client.topics[i] for i in topics_str]

    # print(topics_to_produce_to)
    # sys.exit()
    topic_producers = {topic.name: topic.get_producer()
                       for topic in topics_to_produce_to}

    print(type(topic_producers))
    print(topic_producers)
    # sys.exit()
    consumed_messages = [
        (b'profile_oath', 'profile_oath'),
        (b'session_stay', 'session_stay'),
        (b'ads', 'ads'),
        (b'profile_id', 'profile_id'),
        (b'js_err', 'js_err'),
    ]

    for destination_topic, message in consumed_messages:
        print(destination_topic, message)
        # topic_producers[destination_topic].produce(message)


if __name__ == '__main__':
    # simple_produce()
    # async_produce()
    produce_multi_topic()
