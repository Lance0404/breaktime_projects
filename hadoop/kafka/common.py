import logging
import os


def logger(filepath, log_dir='/tmp/'):
    # dirpath = os.path.dirname(filepath)
    # log_dir = '/home/changteanhsu/projects/log/'
    filename, ext = os.path.splitext(os.path.basename(filepath))
    outlog = os.path.join(log_dir, filename + '.log')

    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(outlog)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger
