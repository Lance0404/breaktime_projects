"""
# Task: download yahooEC data to actionlink and copy them to breaktime's google team drive every week

# step one: download from ftps to actionlink (size ~200G, took 3~6 hr)
cd /home/lance/projects/database
nohup dump_yahoo_ec.py.py &

# step two: upload from actionlink to google teamdrive
cd /home/lance/projects/google/drive_v3
nohup python upload_v2.py -localpath /home/lance/projects/database/yahooEC  -remotedirid 13M1ItMZjdqIAzHXLNimYRE7nPoq8xjeo -remoteTDid 0ABN1nlDroMTQUk9PVA -remotetopdirname yahooEC_v3 &
# data should be stored under RD > Training DataSet > Yahoo 商品資料


v2, upload files from ubuntu server to google team drive
"""
from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload
import logging
import sys
# import pprint
import time
import os
from os import listdir, stat
from os.path import isfile, join, isdir
sys.path.append('/home/lance/Git/breaktime_projects/tools/')
from common.logger import logger, dirmaster
filepath = os.path.abspath(__file__)
logger = logger(filepath)


# get params from cmd line
params = ['localpath', 'remotedirid', 'remoteTDid', 'remotetopdirname']
argvs = sys.argv
paramDic = {}
err = 0
for param in params:
  if '-' + param in argvs:
    idx = argvs.index('-' + param)
    paramDic[param] = argvs[idx + 1]
  else:
    err = 1
    logger.info('param -' + param + ' is missing!')
if err:
  sys.exit(1)

secret_json = '/home/lance/Git/breaktime_projects/google/drive_v3/client_secret.json'
credentials_json = '/home/lance/Git/breaktime_projects/google/drive_v3/credentials.json'
FOLDERID = paramDic['remotedirid']
TEAMDRIVEID = paramDic['remoteTDid']

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

# sys.exit()
# Setup the Drive v3 API
SCOPES = [
    # 'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive'
]
store = file.Storage(credentials_json)
creds = store.get()
if not creds or creds.invalid:
  flow = client.flow_from_clientsecrets(secret_json, SCOPES)
  creds = tools.run_flow(flow, store)
service = build('drive', 'v3', http=creds.authorize(Http()))


def createfolder(name, folderid):
  file_metadata = {
      'name': name,
      'mimeType': 'application/vnd.google-apps.folder',
      'parents': [folderid]
  }
  file = service.files().create(
      body=file_metadata,
      supportsTeamDrives=True,
      fields='id').execute()

  logger.info('Folder ID: %s' % file.get('id'))
  return file.get('id')


def uploadfile(filepath, folderid, sleep=2):
  tmpl = filepath.split('/')
  filename = tmpl[-1]
  folder_id = folderid
  file_metadata = {
      'name': filename,
      'parents': [folder_id]
  }
  # mime_type = 'application/vnd.google-apps.unknown'
  # 5MB = 1024*1024*5 B
  if stat(filepath).st_size <= 1024 * 1024 * 5:
    # logger.info('file size smaller than 5MB')
    media = MediaFileUpload(filepath,
                            # mimetype=mime_type,
                            resumable=True)
  else:
    # logger.info('file size larger than 5MB')
    media = MediaFileUpload(filepath,
                            # mimetype=mime_type,
                            chunksize=1024 * 1024 * 4,
                            resumable=True)
  try:
    results = service.files().create(body=file_metadata,
                                     media_body=media,
                                     supportsTeamDrives=True,
                                     fields='id').execute()
  except HttpError as err:
    # ref: https://stackoverflow.com/questions/23945784/how-to-manage-google-api-errors-in-python
    if err.resp.status in [500, 501, 502, 503, 504, 403]:
      logger.error('resp status: ' + str(err.resp.status))
      logger.error('sleep ' + str(sleep) + ' sec and retry!')
      time.sleep(sleep)
      sleep = sleep * 2
      return uploadfile(filepath, folderid, sleep)
    else:
      raise

  # logger.info('File ID: %s' % results.get('id'))


def checkfile(name, folderid, typename, teamDriveId=None, sleep=2):
  pass
  pageToken = None
  qlist = []
  qlist.append('\'' + folderid + '\' in parents')
  qlist.append('name = \'' + name + '\'')
  qstr = ' and '.join(qlist)
  try:
    results = service.files().list(
        pageSize=10,
        pageToken=pageToken,
        corpora='teamDrive',
        includeTeamDriveItems=True,
        supportsTeamDrives=True,
        teamDriveId=teamDriveId,
        q=qstr,  # yahoo 商品資料 dir
        fields="files(id)",
        # fields="files(id,teamDriveId,name,mimeType,capabilities/canShare)",
        # fields="*"
    ).execute()
  except HttpError as err:
    # ref: https://stackoverflow.com/questions/23945784/how-to-manage-google-api-errors-in-python
    if err.resp.status in [500, 501, 502, 503, 504, 403]:
      logger.error('resp status: ' + str(err.resp.status))
      logger.error('sleep ' + str(sleep) + ' sec and retry!')
      time.sleep(sleep)
      sleep = sleep * 2
      return checkfile(name, folderid, typename, teamDriveId, sleep)
  # except:
  #     logger.info('failed to get results!')
  #     sys.exit()
  #     return None
  items = results.get('files', [])
  if len(items) == 0:
    logger.info(typename + ' ' + name + ' does not exists. Uploading...')
    return 0
  elif len(items) > 1:
    logger.info('should not have ' + typename + ' with the same name!')
    return 1
  else:
    return items[0]['id']


def executefoldercheck(targetfoldername, folderid):
  # step 1: check if the target folder exists. If not, create one.

  # targetfoldername = 'test2'
  targetdirid = checkfile(name=targetfoldername, folderid=folderid,
                          teamDriveId=TEAMDRIVEID, typename='folder')
  if targetdirid == 0:  # if not exist create one
    pass
    targetdirid = createfolder(
        name=targetfoldername, folderid=folderid)
    # logger.info('folder does not exist, creat new folder with id = ' + targetdirid)
  elif targetdirid == 1:
    logger.info('duplicated folder name: ' + targetfoldername + '. exit!')
    sys.exit()
    pass
  else:
    # dir exists, no need to create another one
    logger.info('dir ' + targetfoldername +
                ' exists, no need to create another one')
    pass
  return targetdirid


def executefileupload(filepath, targetdirid):
  pass
# step 2: check if the target file exists. If yes skip uploading, else upload.

  # filepath = '/home/lance/projects/database/yahooEC/bid/full/2018051506/bid_000000.json.gzip'
  tmpl = filepath.split('/')
  filename = tmpl[-1]

  fileid = checkfile(name=filename, folderid=targetdirid,
                     teamDriveId=TEAMDRIVEID, typename='file')
  if fileid == 0:
    uploadfile(filepath=filepath,
               folderid=targetdirid)
  elif fileid == 1:
    logger.info('[Error] duplicated file name: ' +
                filename + '. exit!')  # 20180703 Lance
    # sys.exit()
    pass
    # logger.info('File already exist id: ' + fileid)
  else:
    logger.info('file exists, no need to upload another one')
    pass
    # file exists, no need to upload another one


if __name__ == '__main__':
  # createfolder(name='test', folderid='1IwSSD5t6nwg9K058UJl7IlN5Vhvdt_Ro')
  # res = executefoldercheck(targetfoldername='test', folderid=FOLDERID)
  # logger.info(res)
  # sys.exit()

  localpath = paramDic['localpath']

  # tmpl = localpath.split('/')
  # dirname = tmpl[-1]
  dirname = paramDic['remotetopdirname']
  # dirname = 'yahooEC_v2'
  topdirid = executefoldercheck(targetfoldername=dirname, folderid=FOLDERID)
  for f in listdir(localpath):
    if isdir(join(localpath, f)):
      lv2dirid = executefoldercheck(
          targetfoldername=f, folderid=topdirid)
      for ff in listdir(join(localpath, f)):
        if isdir(join(localpath, f, ff)):
          lv3dirid = executefoldercheck(
              targetfoldername=ff, folderid=lv2dirid)
          for fff in listdir(join(localpath, f, ff)):
            if isdir(join(localpath, f, ff, fff)):
              lv4dirid = executefoldercheck(
                  targetfoldername=fff, folderid=lv3dirid)
              for ffff in listdir(join(localpath, f, ff, fff)):
                if isfile(join(localpath, f, ff, fff, ffff)):
                  executefileupload(filepath=join(
                      localpath, f, ff, fff, ffff), targetdirid=lv4dirid)
            # elif isfile(join(localpath, f, ff, fff)):
            #     executefileupload(filepath=join(
            #         localpath, f, ff, fff), targetdirid=topdirid)
        # elif isfile(join(localpath, f, ff)):
        #     executefileupload(filepath=join(
        #         localpath, f, ff), targetdirid=topdirid)
    # elif isfile(join(localpath, f)):
    #     executefileupload(filepath=join(localpath, f),
    #                       targetdirid=topdirid)

  #     if isfile(join(localpath, f)):
  #         logger.info(f)
logger.info('Done!')


# onlyfiles = [f for f in listdir(localpath) if isfile(join(localpath, f))]
