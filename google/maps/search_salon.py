'''
ref:
https://developers.google.com/places/web-service/search

Method: places_nearby & places
cons:
1. radius － 定義要傳回地點結果的距離範圍（單位為公尺）。允許的最大半徑是 50,000 公尺。請注意，如果已指定 rankby=distance （參見下方選擇性參數下的說明），就不得包括 radius。
2. 根據預設，「附近地點搜尋」或「文字搜尋」針對每一查詢最多會傳回 20 個 establishment 結果；然而，每個搜尋可傳回多達 60 個紀錄，分列於三頁。如果您的搜尋會傳回超過 20 個結果，則搜尋回應將會包括一個額外的值，也就是 next_page_token 。




'''


import googlemaps
from datetime import datetime
import sys
import os
import time
import pprint
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

logger.info('Start!')
gmaps = googlemaps.Client(key='AIzaSyB2UAp9UgGhnQAHKyCKwtaWhCRw9Jg8P2c')

# print(gmaps.__dict__)
# print(gmaps.__doc__)
# sys.exit()


# ========= hyper params =========
sleep = 3

# ====================================


def todata(adict):
    bdict = {}
    for each in ['id', 'name', 'rating', 'vicinity']:
        bdict[each] = adict.get(each)
    return bdict


def get_place_details(place_id):
    time.sleep(sleep)
    return gmaps.place(place_id)


def ret2dict(adict: 'dict') -> 'list':
    outlist = []
    if 'results' in adict:
        for i in adict['results']:
            if 'formatted_address' in i:
                aid, name, rating, address = i['id'], i['name'], i['rating'], i['formatted_address']
            elif 'vicinity' in i:
                aid, name, rating, address = i['id'], i['name'], i['rating'], i['vicinity']
            atuple = (aid, name, rating, address)
            outlist.append(atuple)
        return outlist
    else:
        return False


if 0:
    placeid_list = []
    # if 1:
    # Nearby Search
    location = (25.033292, 121.554898)
    radius = 50000  # 50,000 maximum
    name = '沙龍'
    place_type = 'hair_care'
    nearby_result = gmaps.places_nearby(
        location=location,
        radius=radius,
        # rankby=prominence,
        name=name,
        type=place_type,
        # rankby=distance,
        language='zh-TW'
    )
    pprint.pprint(nearby_result)
    # pprint.pprint(nearby_result['results'][0])
    # for adict in nearby_result['results']:
    # print(adict)
    # bdict = todata(adict)
    # print(bdict)
    placeid_list = [idict['id'] for idict in nearby_result['results']]

    sys.exit()

    try:
        pagetoken = nearby_result['next_page_token']
        # print(pagetoken)
        while True:
            time.sleep(sleep)
            try:
                # print(pagetoken)
                nearby_result = gmaps.places_nearby(
                    location=location,
                    radius=radius,
                    # rankby=prominence,
                    # rankby=distance,
                    name=name,
                    type=place_type,
                    language='zh-TW',
                    page_token=pagetoken
                )
                # print(nearby_result)
                placeid_list += [idict['id']
                                 for idict in nearby_result['results']]
                print('num of res: ' + str(len(nearby_result['results'])))
            except Exception as e:
                logger.debug(f'gmaps.places_nearby err: {e!s}')
                break
            try:
                pagetoken = nearby_result['next_page_token']
            except Exception as e:
                # logger.debug(e)
                break
    except Exception as e:
        logger.debug(e)

    print(placeid_list)
    print(f'{len(placeid_list)!s}')

    for aid in placeid_list:
        time.sleep(sleep)
        ret = gmaps.place(aid)

        print(ret)

    sys.exit()


outlist = []
# Text Search
location = (25.033292, 121.554898)
# query = '台北市大安區信義路四段306號11樓之2'
query = '沙龍'
radius = 50000  # meter
place_type = 'beauty_salon'
text_result = gmaps.places(
    query,
    location=location,
    radius=radius,
    language='zh-TW'
)
# print(text_result)
# sys.exit()
print('num of res: ' + str(len(text_result['results'])))
outlist = ret2dict(text_result)
# pagetoken = text_result['next_page_token']
# print(pagetoken)

# sys.exit()
try:
    pagetoken = text_result['next_page_token']
    # print(pagetoken)
    while True:
        time.sleep(sleep)
        text_result = gmaps.places(
            query,
            location=location,
            radius=radius,
            language='zh-TW',
            page_token=pagetoken
        )
        # print(text_result)
        print('num of res: ' + str(len(text_result['results'])))
        outlist += ret2dict(text_result)
        try:
            pagetoken = text_result['next_page_token']
        except Exception as e:
            # logger.debug(f'key error: {e!s}')
            break
except Exception as e:
    logger.debug(f'gmaps.places failed: {e!s}')

print(outlist)
print(f'len of outlist: {len(outlist)!s}')

logger.info('Done!')
