import googlemaps
from datetime import datetime
import sys
import pprint
gmaps = googlemaps.Client(key='AIzaSyB2UAp9UgGhnQAHKyCKwtaWhCRw9Jg8P2c')

# print(gmaps.__dict__)
# print(gmaps.__doc__)
# sys.exit()


if 0:
    # Nearby Search
    location = (25.033292, 121.554898)
    radius = 25000
    place_type = 'restaurant'
    nearby_result = gmaps.places_nearby(
        location, radius, type=place_type)
    # print(nearby_result)

    print('num of res: ' + str(len(nearby_result['results'])))

    sys.exit()

# Text Search
location = (25.033292, 121.554898)
# query = '台北市大安區信義路四段306號11樓之2'
query = '火鍋'
# query = '1600 Amphitheatre Parkway, Mountain View, CA'
radius = 25000 # meter
place_type = 'restaurant'
text_result = gmaps.places(
    query, location=location, radius=10000, language='zh-TW')
# print(nearby_result)
print(text_result)
print('num of res: ' + str(len(text_result['results'])))
sys.exit()

# Radar search
# location = (25.017156, 121.506359)
location = (25.033292, 121.554898)
radius = 25000
place_type = 'restaurant'
places_radar_result = gmaps.places_radar(location, radius, type=place_type)
# print(places_radar_result)


def get_place_details(place_id):
    return gmaps.place(place_id)


print('num of returned doc: ' + str(len(places_radar_result['results'])))
sys.exit()
count = 0
for value in places_radar_result['results']:
    place_id = value['place_id']
    place_detail = get_place_details(place_id)
    # print(type(place_detail))
    # for k, v in place_detail.items():
    # print(k)
    # print(type(place_detail['result']))
    # print(place_detail['result'].keys())
    print(place_detail['result']['types'])
    # for k, v in place_detail['result'].items():
    # print(k + ': ' + str(v))
    # print(k)
    count += 1
    if count > 10:
        break

sys.exit()
# Geocoding an address
# geocode_result = gmaps.geocode('1600 Amphitheatre Parkway, Mountain View, CA')
geocode_result = gmaps.geocode('台北市大安區信義路四段306號樓之2')
pprint.pprint(geocode_result)

sys.exit()
# Look up an address with reverse geocoding
reverse_geocode_result = gmaps.reverse_geocode((40.714224, -73.961452))
print(reverse_geocode_result)
# Request directions via public transit
now = datetime.now()
directions_result = gmaps.directions("Sydney Town Hall",
                                     "Parramatta, NSW",
                                     mode="transit",
                                     departure_time=now)
print(directions_result)
