from googleapiclient import sample_tools
from HelloAnalytics import initialize_analyticsreporting
from HelloAnalytics import initialize_analyticsv3


# from HelloAnalytics import get_report
from HelloAnalytics import print_response


def get_report(analytics):
    """Queries the Analytics Reporting API V4.

    Args:
      analytics: An authorized Analytics Reporting API V4 service object.
    Returns:
      The Analytics Reporting API V4 response.
    """
    return analytics.reports().batchGet(
        body={
            'reportRequests': [
                {
                    'viewId': VIEW_ID,
                    'dateRanges': [{'startDate': '2018-06-01', 'endDate': 'today'}],
                    'dimensions': [{'name': 'ga:country'}],
                    'metrics': [{'expression': 'ga:sessions'}]
                }]
        }
    ).execute()


def main():
    analytics = initialize_analyticsreporting()
    response = get_report(analytics)
    print_response(response)

    # response = get_meta(analytics)
    # print(response)


def get_meta(service):
    # ref: https://developers.google.com/analytics/devguides/reporting/metadata/v3/reference/metadata/columns/list
    results = service.metadata().columns().list(reportType='ga').execute()
    return results


if __name__ == '__main__':
    # main()

    analytics = initialize_analyticsv3()
    results = get_meta(analytics)
    print(results)
