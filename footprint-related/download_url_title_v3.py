#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
# import multiprocessing as mp
sys.path.append('./')

# usage:
# ./download_url_title.py -S 20180301 -E 20180401
# nohup ./download_url_title_v3.py -S 20180301 -E 20180304 > tmp_20180301_20180304.log &
# nohup ./download_url_title_v3.py -S 20180304 -E 20180307 > tmp_20180304_20180307.log &
# nohup ./download_url_title_v3.py -S 20180307 -E 20180310 > tmp_20180307_20180310.log &
# nohup ./download_url_title_v3.py -S 20180310 -E 20180313 > tmp_20180310_20180313.log &
# nohup ./download_url_title_v3.py -S 20180313 -E 20180316 > tmp_20180313_20180316.log &
# nohup ./download_url_title_v3.py -S 20180316 -E 20180319 > tmp_20180316_20180319.log &
# nohup ./download_url_title_v3.py -S 20180319 -E 20180321 > tmp_20180319_20180321.log &
# nohup ./download_url_title_v3.py -S 20180321 -E 20180324 > tmp_20180321_20180324.log &
# nohup ./download_url_title_v3.py -S 20180324 -E 20180327 > tmp_20180324_20180327.log &
# nohup ./download_url_title_v3.py -S 20180327 -E 20180401 > tmp_20180327_20180301.log &
# err msg:
# elasticsearch.helpers.ScanError: Scroll request has only succeeded on 3 shards out of 5.
# v3, increase the column of interest ["url", "title", "pageid", "fp", "id", "os_name", "name", "stay", "updated"]
# use hasattr to check for the existence of attribute


'''=================== self-defined functions start ==================='''

def log(msg):
  print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

'''=================== self-defined functions end ==================='''

'''=================== global variables start ==================='''
argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')

startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]

filename = os.path.splitext(sys.argv[0])[0]+'_'+str(startDateString)+'_'+str(endDateString)+'.csv'
# print(filename)

# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)


# initiate the variable 
body = {
  "_source": ["url", "title", "pageid", "fp", "id", "os_name", "name", "stay", "updated"],
  "query": {
    "bool": {
      "must": [
        {"term" : { "hostname" : "zi.media"}
        },
        {
          "range": {
            "updated": {
              "gte": startDateString,
              "lt": endDateString,
              "time_zone": "+08:00",
              "format": "yyyyMMdd"
            }
          }
        }
      ]
    }
  }
}

log(str(body))

res = helpers.scan(
        client = esCluster,
        scroll = '20m',
        size = 10000,
        query = body,
        index = 'footprints_201803',
        doc_type= "footprint",
        request_timeout = 5000
)

# outDic = {}
data = []
count = 0

# ["url", "title", "pageid", "fp", "id", "os_name", "name", "stay", "updated"]
# ["url", "title", "pageid", "fp", "os_name", "name", "stay", "updated"]

with open(filename, 'w') as outfile:
	wr = csv.writer(outfile)
	for doc in res:
		count+=1
		# print(doc)
		footid = doc['_id']
		if hasattr(doc['_source'], 'url'):
			url = doc['_source']['url']
		else:
			url = ''	
		title = doc['_source']['title']
		pageid = doc['_source']['pageid']
		fp = doc['_source']['fp']
		if hasattr(doc['_source'], 'id'):
			idd = doc['_source']['id']
		else:
			idd = ''	
		os_name = doc['_source']['os_name']
		name = doc['_source']['name']
		if hasattr(doc['_source'], 'stay'):
			stay = doc['_source']['stay']
		else:
			stay = ''	 
		updated = doc['_source']['updated']
		#data.append([footid, url, title, pageid, fp])
		# wr.writerow([footid, url, title, pageid, fp, idd, os_name, name, stay, updated])
		wr.writerow([footid, url, title, pageid, fp, idd, os_name, name, stay, updated])		
		if (count % 10000 == 0):
			log('number of doc processed: '+str(count))
			# log('number of key1: '+str(len(outDic)))
		# if (count > 10): # test 
		# 	break

# log('length of list: '+str(len(data)))
log('total processed doc: '+str(count))	


