#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
# import multiprocessing as mp
sys.path.append('./')


'''=================== self-defined functions start ==================='''

def log(msg):
  print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

'''=================== self-defined functions end ==================='''

'''=================== global variables start ==================='''
argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')

startDateString = argvs[indexStart+1]

endDateString = argvs[indexEnd+1]

filename = 'author_titlelv1_uqurl_'+str(startDateString)+'_'+str(endDateString)+'.csv'
# print('filename: '+filename)

# connection to ELK
esCluster = Elasticsearch(
    hosts = ['192.168.21.20'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)


# sys.exit()

'''=================== global variables end ==================='''


# initiate the variable 
body = {
  "_source": ["author_name", "title_lv1", "url"],
  "query": {
    "bool": {
      "must": [
        {"term" : { "hostname" : "zi.media"}
        },
        {
          "range": {
            "updated": {
              "gte": startDateString,
              "lt": endDateString,
              "time_zone": "+08:00",
              "format": "yyyyMMdd"
            }
          }
        }
      ],
      "must_not":{
        "match_phrase": {
          "author_name":{
            "query": ""
          }
        }
      }
    }
  }
  #   ,
  # "aggs": { 
  #   "author": { 
  #     "terms": { 
  #       "field": "author_name",
  #       "size": 2, 
  #       "order": {"_count": "desc"}  
  #     },
  #     "aggs": {
  #         "ab": {
  #           "terms": {
  #             "field": "title_lv1",
  #             "size": 2,
  #             "order": {
  #               "_count": "desc"
  #             }
  #           },
  #           "aggs": {
  #           "ooo": {
  #             "terms": {
  #               "field": "url",
  #               "size": 2,
  #               "order": {"_count": "desc"}
  #             }
  #           }
  #         }
  #         }
  #     }
  #   }
  # }
}

# res = esCluster.search(index="ads", doc_type="ad", scroll="3m", search_type="query_then_fetch", size=scroll_size, body=body, request_timeout=300)
res = helpers.scan(
        client = esCluster,
        scroll = '20m',
        size = 100,
        query = body,
        index = 'nlppages',
        doc_type= "nlppage",
        request_timeout = 5000
)
# log('type returned from helpers.scan: '+str(type(res)))


# sys.exit()

# print(len(res))
outDic = {} # key = author, key2 = title_lv1, val = [url1, url2, ... ,urln]
count = 0
for doc in res:
  count += 1
  # print(doc)
  key1 = doc['_source']['author_name']
  key2 = doc['_source']['title_lv1']
  val = doc['_source']['url']
  # print(key1+','+key2+','+val)
  # log('print key: '+key)
  if key1 in outDic:
    if key2 in outDic[key1]:
      outDic[key1][key2].append(val)
    else:
      outDic[key1][key2] = []
  else: 
    outDic[key1] = {}
  if (count % 10000 ==0):  
    log('number of doc processed: '+str(count))
    log('number of key1: '+str(len(outDic)))
  if (count > 30000): # test 
    break

log('total number of doc processed: '+str(count))
log('total number of keyword before calculation: '+str(len(outDic)))

sys.exit()

# log("Got %d Hits:" % res['hits']['total'])
sortedKeys = sorted(outDic)
outputList = []
# generate interable as csv.writer's input
for k in sortedKeys:
  outputList.append((k, str(outDic[k]['sum']), str(outDic[k]['count'])))
with open(filename, "w") as csvfile:
  csvwriter = csv.writer(csvfile, delimiter=',')
  csvwriter.writerows(outputList)
    # file.write(k + ',' + outDic[k]['sum'] + ',' + outDic[k]['count'] + "\n")

log('write to file done.')
sys.exit()


