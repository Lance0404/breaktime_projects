#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
# import json
# import re
# import hashlib # decided to use pagid instead 
# v1, under construction 
sys.exit()

filename, ext = os.path.splitext(os.path.basename(__file__))


logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''

# argvs = sys.argv[1:]
# indexStart = argvs.index('-S')
# indexEnd = argvs.index('-E')
# startDateString = argvs[indexStart+1]
# endDateString = argvs[indexEnd+1]
# indextb = argvs.index('-tb')
# tablename = argvs[indextb+1]
# indexesidx = argvs.index('-esidx')
# esidx = argvs[indexesidx+1]
# indexestype = argvs.index('-estype')
# estype = argvs[indexestype+1]


# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

# body = {
#   "_source": ["url", "content_p"],
#   "query": {
#     "bool": {
#       "must": [
#         {"term" : { "hostname" : "zi.media"}
#         },
#         {
#           "range": {
#             "updated": {
#               "gte": startDateString,
#               "lte": endDateString,
#               "time_zone": "+08:00",
#               "format": "yyyyMMdd"
#             }
#           }
#         }
#       ]
#     }
#   }
# }
body = {}
body['_source'] = ["pageid", "ads_keyword", "ads"]
body['query'] = {}
body['query']['bool'] = {}
body['query']['bool']['must'] = []
if '-zi' in sys.argv: # params for zi.media only
	body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
body['query']['bool']['must'].append({"range": { "updated": {"gte": startDateString, "lte": endDateString, "time_zone": "+08:00", "format": "yyyyMMdd"}}})


logger.info(body)
# print(body)
res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = esidx,
				doc_type= estype,
				request_timeout = 5000
)

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='192.168.21.101',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)



droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	adsid VARCHAR(20) NOT NULL,
	pageid VARCHAR(40) NOT NULL,
	ads_keyword VARCHAR(100),
	ads_count INT(3) UNSIGNED,
	ads_dedup_count INT(3) UNSIGNED,
	ads_title TEXT,
	ads_descr TEXT,
	KEY `adsid` (`adsid`),
	KEY `pageid` (`pageid`),
	KEY `ads_keyword` (`ads_keyword`)
	) '''

datalist = ['adsid', 'pageid', 'ads_keyword', 'ads_count', 'ads_dedup_count', 'ads_title', 'ads_descr']
insertcolstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))


each10000doc = []
try:
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		logger.info(droptbstr)
		cursor.execute(createtbstr)
		logger.info(createtbstr)
		try:
			count = 0
			# while True: 
			for doc in res:
				count+=1
				# if (count > 10): # test 
				# 	break
				adsid = doc['_id']
				pageid = ckattr(doc['_source'], 'pageid')
				ads_keyword = ckattr(doc['_source'], 'ads_keyword')
				ads = ckattr(doc['_source'], 'ads')
				# ads_json = {}
				dedup = {}
				ads_title_list = []
				ads_desc_list = []
				if ads != '':
					# ads_count = 0 
					for i in ads:
						# ads_count += 1
						if i['title'] in dedup:
							pass
						else:
							dedup[i['title']] = 1
							# if ads_count in ads_json and 'title' in ads_json[ads_count]:
							# 	pass
							# else:
							# 	ads_json[ads_count] = {}
							# 	ads_json[ads_count]['title'] = i['title']
							# 	ads_json[ads_count]['descr'] = i['descr']
							ads_title_list.append(i['title'])
							ads_desc_list.append(i['descr'])
				# url = ckattr(doc['_source'], '')
				ads_count = len(ads)
				ads_dedup_count = len(ads_title_list)

				each_doc = [adsid, pageid, ads_keyword, ads_count, ads_dedup_count, ':;:'.join(ads_title_list), ':;:'.join(ads_desc_list)]
				# each_doc = [adsid, pageid, ads_keyword, json.dumps(ads_json)]
				# if adsid == 'AWHmKYTSPaSFOb6ZTldc':
				# 	print(each_doc)
				# continue
				each10000doc.append(each_doc)
				sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"

				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))

				# if (count > 10): # test 
				# 	break	


		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')