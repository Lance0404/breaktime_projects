#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging

# usage:
# nohup ./insert_footprint.py -S 20180201 -E 20180301 &
# v1, add logging 
# optimize the table schema
# v2, change footid to primary key. Do not filter hostname=zi.media

logger = logging.getLogger('insert_footprint')
logger.setLevel(logging.DEBUG)
filename, ext = os.path.splitext(os.path.basename(__file__))
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('Start!')

'''=================== self-defined functions start ==================='''

# def log(msg):
#   print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')

startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]

# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

body = {
  "_source": ["url", "title", "pageid", "fp", "hostname", "os_name", "name", "stay", "updated"],
  "query": {
    "bool": {
      "must": [
        # {"term" : { "hostname" : "zi.media"}
        # },
        {
          "range": {
            "updated": {
              "gte": startDateString,
              "lt": endDateString,
              "time_zone": "+08:00",
              "format": "yyyyMMdd"
            }
          }
        }
      ]
    }
  }
}

res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = 'footprints_201802',
				doc_type= "footprint",
				request_timeout = 5000
)

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='localhost',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

datalist = ['footid', 'hostname', 'url', 'title', 'pageid', 'fp', 'os_name', 'name', 'stay', 'updated']
insertcolstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))

tablename = 'footprints_201802'
count = 0

# 1ab0f842eee40ffbf6e730da282976ce1522540794
# f90b6ffa4139b64fcb1461123abd288090d8b324
# 1ab0f842eee40ffbf6e730da282976ce
droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(footid VARCHAR(42) NOT NULL PRIMARY KEY,
	hostname VARCHAR(100),
	url TEXT,
	title TEXT,
	pageid VARCHAR(40),
	fp VARCHAR(32),
	os_name VARCHAR(50),
	name TEXT,
	stay INT UNSIGNED,
	updated DATETIME,
	KEY `k_hostname` (`hostname`),
  KEY `k_updated` (`updated`),
  KEY `k_pageid` (`pageid`),
  KEY `k_fp` (`fp`)
	) '''
# id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
# url VARCHAR(2083)	
# ENGINE=InnoDB DEFAULT CHARSET=utf8

each10000doc = []
try:
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		cursor.execute(createtbstr)
		try:
			# while True: 
			for doc in res:
				# doc = res.next()
				# print(len(res))
				# break
				count+=1
				footid = doc['_id']
				hostname = ckattr(doc['_source'], 'hostname')
				url = ckattr(doc['_source'], 'url') 
				title = ckattr(doc['_source'], 'title')
				pageid = ckattr(doc['_source'], 'pageid')
				fp = ckattr(doc['_source'], 'fp')
				os_name = ckattr(doc['_source'], 'os_name')
				name = ckattr(doc['_source'], 'name')
				stay = ckattr(doc['_source'], 'stay')	
				updated = doc['_source']['updated']
				each_doc = [footid, hostname, url, title, pageid, fp, os_name, name, stay, updated]
				each10000doc.append(each_doc)
				sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"

				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))
				# if (count > 50000): # test 
				# 	break

		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')