'''
[cras4202tw 需求]
請協助寫一隻在 OS X 上跑的程式
將某資料夾中裡面的檔案 掃過一輪
不論大小寫與副檔名
有重複的就改為一個即可

舉例：
遇到檔案名稱為
*.jpg.jpg 的改為 *.jpg
*.png.png 的改為 *.png

所有副檔名重複的檔案都會有一個正常的檔案
直接蓋過

CMD:
python cs_dedupfileext.py -dir /home/lance/data/dupdir

## before:
(my_env) root@ubuntu:/home/lance/data/dupdir# ls *
test.jpg.jpg  test.png.png

dupdirlv2:
test.abc.abc  test.yyy.yyy

## after:
(my_env) root@ubuntu:/home/lance/data/dupdir# ls *
test.jpg  test.png

dupdirlv2:
test.abc  test.yyy
'''

import sys
from os import listdir, rename, walk
from os.path import isfile, join, isdir


def parseCMD(argvs: 'list', mustparams: 'list', optionalparams: 'list') -> 'Dict':
    '''
    parse auguments from command line
    '''
    paramDic = {}
    for param in mustparams:
        if '-' + param in argvs:
            idx = argvs.index('-' + param)
            paramDic[param] = argvs[idx + 1]
        else:
            raise KeyError(f'{param!s} must not be empty!')
            # try:
            #     raise KeyError(f'{param!s} must not be empty!')
            # except KeyError as e:
            #     print(f'{e!s}')
            #     sys.exit()
    for param in optionalparams:
        if f'-{param}' in argvs:
            idx = argvs.index('-' + param)
            paramDic[param] = argvs[idx + 1]
        else:
            paramDic[param] = None
    return paramDic


def checkfile(file):
    ret = file
    fileitems = file.split('.')
    if (len(fileitems) > 2) and (fileitems[1] == fileitems[2]):
        ret = '.'.join(fileitems[0:2])
    return ret


def dedupfileext(localpath):
    # for f in listdir(localpath):
    #     file = join(localpath, f)
    #     print(f'checking file: {file!s}')
    #     if isdir(file):
    #         subpath = file
    #         return dedupfileext(subpath)
    #     else:
    #         # check if file has repeated file extension
    #         retfile = checkfile(f)
    #         if retfile != f:
    #             retfile = join(localpath, retfile)
    #             print(f'processing file: {file!s} > {retfile!s}')
    #             rename(file, retfile)

    for root, dirs, files in walk(localpath):
        for file in files:
            filepath = join(root, file)
            print(f'checking file: {filepath!s}')
            retfile = checkfile(file)
            if retfile != file:
                retfilepath = join(root, retfile)
                print(f'processing file: {file!s} > {retfile!s}')
                rename(filepath, retfilepath)


print('Start!')
argvs = sys.argv
mustparams = ['dir']
optionalparams = []

paramDic = parseCMD(argvs, mustparams, optionalparams)

localpath = paramDic['dir']
dedupfileext(localpath)

print('Done!')
