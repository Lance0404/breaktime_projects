if 0:
    raise


class LanceError(ValueError):
    pass


try:
    a = 'atest'
    b = 'btest'
    c = {}
    c[a] = 'dicta'
    print(c[a])
    try:
        print(d)
        # raise LanceError('2nd goes wrong!')
    except:
        # pass
        raise
        # raise ValueError('sth goes wrong!')
        # raise LanceError('2nd goes wrong!')
except ValueError as e:
    print(f'lance defined Exception: {e}')
# except Exception as e:
# except LanceError as e:
#     print(f'lance defined Exception: {e}')
#     raise
#     print(f'lance Exception: {e}')


print('process is not dead')
