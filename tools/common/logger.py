import logging
import os

# filename, ext = os.path.splitext(os.path.basename(__file__))
# filename, ext = os.path.splitext(os.path.basename(__name__))


def logger(filepath):
    # dirpath = os.path.dirname(filepath)
    log_dir = '/home/lance/log/'
    filename, ext = os.path.splitext(os.path.basename(filepath))
    outlog = os.path.join(log_dir, filename + '.log')

    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(outlog)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger


class dirmaster():
    data = '/home/lance/data/'
    info = '/home/lance/info/'

    def __init__(self, data, info):
        self.data = data
        self.info = info
