'''
function or classes that are frequently used by many scripts
'''
import logging
import os
import sys


def logger(filepath):
    # dirpath = os.path.dirname(filepath)
    log_dir = '/home/lance/log/'
    filename, ext = os.path.splitext(os.path.basename(filepath))
    outlog = os.path.join(log_dir, filename + '.log')

    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(outlog)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger


class dirmaster():
    data = '/home/lance/data/'
    info = '/home/lance/info/'

    def __init__(self, data, info):
        self.data = data
        self.info = info


def parseCMD(argvs: 'list', mustparams: 'list', optionalparams: 'list') -> 'Dict':
    '''
    parse auguments from command line
    '''
    paramDic = {}
    for param in mustparams:
        if '-' + param in argvs:
            idx = argvs.index('-' + param)
            paramDic[param] = argvs[idx + 1]
        else:
            raise KeyError(f'{param!s} must not be empty!')
            # try:
            #     raise KeyError(f'{param!s} must not be empty!')
            # except KeyError as e:
            #     print(f'{e!s}')
            #     sys.exit()
    for param in optionalparams:
        if f'-{param}' in argvs:
            idx = argvs.index('-' + param)
            paramDic[param] = argvs[idx + 1]
        else:
            paramDic[param] = None
    return paramDic
