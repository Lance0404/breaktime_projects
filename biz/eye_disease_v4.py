'''
import hashlib # decided to use pagid instead
v1, query webpages related to eye disease
v2, since there are 21% of webpages loss in index webpages, so keywords query should also be done on footprints_2018*. what a pain in the ass
use update instead of union!
elk returned title does not contain the queried keyword, double check with python
v3, add checkagain function, make sure that the title contains the keyword
support top50 output for *_perpage and *_wordperpage
v4, support EACH: OR: query text format, not yet supporting AND:
v4.1, support (a b) as "*a* AND *b*"
'''
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
import multiprocessing as mp
# import json
import re
# import pandas as pd
# import numpy as np
import pymysql.cursors
# import logging
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

params = ['qfile', 'proc', 'S', 'E', 'out']
argvs = sys.argv
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass
    # print(e)

startDateString = paramDic['S']
endDateString = paramDic['E']
if 'proc' in paramDic:
    numprocess = int(paramDic['proc'])
esidx = 'footprints_2018*'
estype = 'footprint'
outputlabel = paramDic['out']


logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

queryDict = {}
wordlist = []
with open(paramDic['qfile'], 'r') as f:
    for x in [line.strip('\n') for line in f]:
        # print(x)
        itype, istr = (i for i in x.split(':', 1))
        # print(itype, istr)
        # list comprehension
        itp = tuple([i for i in istr.split('/') if i != ''])
        # print(itype, itp)
        if len(itp) > 0:
            queryDict[itype] = itp

# sys.exit()
wordlist = []
for each in queryDict['EACH']:  # EACH must exist and only one of OR/AND must exist
    if 'OR' in queryDict:
        tmp = {}
        tmp['OR'] = queryDict['OR']
        wordlist.append((each, tmp))
    elif 'AND' in queryDict:
        tmp = {}
        tmp['AND'] = queryDict['AND']
        wordlist.append((each, tmp))


print(wordlist)
check = input('Make sure there is no special character in your qfile[Y]: ')
if check in ['', 'y', 'Y', 'yes', 'Yes']:
    print('Proceed!')
else:
    print('Quit and check again!')
    sys.exit()
# print(queryDict)
# q_each
# q_or
# q_and

# sys.exit()

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        # print(po[attr])
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))


def getGAlabel(gender, age):
    label = 'NA'
    if gender == 'M':
        if age >= 15 and age <= 24:
            label = 'M1'
        if age >= 25 and age <= 34:
            label = 'M2'
        if age >= 35 and age <= 44:
            label = 'M3'
        if age >= 45 and age <= 54:
            label = 'M4'
        if age >= 55 and age <= 64:
            label = 'M5'
        if age >= 65:
            label = 'M6'
    if gender == 'F':
        if age >= 15 and age <= 24:
            label = 'F1'
        if age >= 25 and age <= 34:
            label = 'F2'
        if age >= 35 and age <= 44:
            label = 'F3'
        if age >= 45 and age <= 54:
            label = 'F4'
        if age >= 55 and age <= 64:
            label = 'F5'
        if age >= 65:
            label = 'F6'
    return label


'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
step = delta // numprocess
worker_start = start
worker_end = end


def checkagain(keyword, pageidset):

    def ismatch(keyword, title):
        '''
        might prone  to bugs!!!!!
        '''
        # keyword is a tuple
        if title == '':
            return False
        each, adict = keyword
        itype = [i for i in adict.keys()][0]  # should be either OR or AND
        if re.search(each, title, re.I):
            if itype == 'OR':
                # i may be a pharse or a (pharse1 pharse2)
                for i in adict[itype]:
                    mObj = re.match(r'^\((.+)\)$', i)
                    if mObj:
                        tmplist = mObj.group(1).split()
                        for ii in tmplist:
                            if re.search(ii, title, re.I):
                                continue
                            else:
                                return False
                        return True
                    else:
                        if re.search(i, title, re.I):
                            return True  # if any were matched break
            elif itype == 'AND':
                for i in adict[itype]:
                    if not re.search(i, title, re.I):
                        return False  # if any were not matched break
            else:
                raise Exception('unexpected itype!')
        else:
            return False

    esCluster = Elasticsearch(
        # hosts = ['192.168.21.20'],
        hosts=['192.168.21.11'],
        # http_auth=('elastic', 'breaktime168'),
        port=9200,
        timeout=36000
    )

    # kw_list = []
    # for each in keyword.split():
    #     kw_list.append(each)

    newpageidset = set()
    for eachpageid in pageidset:
        body = {}
        body['_source'] = ["title"]
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must'].append(
            {"term": {"pageid": eachpageid}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # logger.info(body)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=500
        )
        count = 0
        matchflag = 0
        for doc in res:
            title = doc['_source']['title']
            if ismatch(keyword, title):
                matchflag = 1
            else:
                matchflag = 0
                break
            count += 1
            if count == 10:  # check 10 doc at most
                break
        if matchflag == 1:
            newpageidset.add(eachpageid)
    logger.info('check pageid for keyword ' + str(keyword) + ':' +
                str(len(pageidset)) + ' > ' + str(len(newpageidset)))
    return newpageidset


def worker_one(stepnum):
    # split workers with wordlist, not by time range
    global proc
    if len(wordlist) % proc > proc // 2:
        step = len(wordlist) // proc + 1
    else:
        step = len(wordlist) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = wordlist[s:]
    else:
        todo_worklist = wordlist[s:s + step]

    workeroutlist = []
    # logger.info(todo_worklist)
    for word in todo_worklist:  # word is a tuple
        # ('曼秀雷敦', {'OR': ('痠痛貼布', '酸痛貼布', '運動痠痛', '運動酸痛', '痠痛藥布', '酸痛藥布', '痠痛軟膏', '酸痛軟膏', '酸痛滾珠', '痠痛滾珠', '放鬆肌肉', '肌肉放鬆', '痠痛噴霧', '酸痛噴霧', '上班痠痛', '上班酸痛', '痠痛按摩', '酸痛按摩', '酸痛精油', '痠痛精油', '按摩', '(酸痛 貼布)', '(酸痛 噴霧)', '(酸痛 按摩)', '(酸痛 精油)', '(上班 痠痛)', '(家事 痠痛)', '(家事 酸痛)', '(運動 痠痛)')})
        body = {}
        body['_source'] = ["pageid", "title"]

        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must_not'] = []

        q_kw_str = ''
        aeach, adict = word
        aeachstr = '(*' + aeach + '*)'
        # print(adict)
        # continue
        ilist = []
        for itype, ituple in adict.items():  # should have only one key
            for item in ituple:
                itemstr = ''
                mObj = re.match(r'^\((.+)\)$', item)
                if mObj:
                    tmplist = ['*' + i + '*' for i in mObj.group(1).split()]
                    tmpstr = ' AND '.join(tmplist)
                    itemstr = '(' + tmpstr + ')'
                    # logger.info('match!')
                else:
                    itemstr = '*' + item + '*'
                    # logger.info('not match!')
                ilist.append(itemstr)

            itypestr = ' ' + itype + ' '
            iliststr = itypestr.join(ilist)
            q_kw_str = aeachstr + ' AND (' + iliststr + ')'
            break  # do once then break

        body['query']['bool']['must'].append(
            {"query_string": {"query": q_kw_str, "fields": ["title"]}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})

        logger.info(body)
        # continue
        esCluster = Elasticsearch(
            # hosts = ['192.168.21.20'],
            hosts=['192.168.21.11'],
            # http_auth=('elastic', 'breaktime168'),
            port=9200,
            timeout=36000
        )
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )

        pageidSet = set()
        count = 0
        wordDic = {}
        for doc in res:  # each is a pageid
            pageid = ckattr(doc['_source'], 'pageid')
            title = ckattr(doc['_source'], 'title')
            matchflag = 0
            if title == '':
                continue
            # if re.match(r'and', paramDic['operator'], re.I):
            #     for i in kw_list:
            #         # print(i, title)
            #         if re.search(i, title, re.I):
            #             matchflag = 1
            #         else:
            #             matchflag = 0
            # elif re.match(r'or', paramDic['operator'], re.I):
            #     for i in kw_list:
            #         # print(i, title)
            #         if re.search(i, title, re.I):
            #             matchflag = 1
            #             break
            # if matchflag == 0:
            #     continue
            if pageid in pageidSet:
                continue
            pageidSet.add(pageid)
            count += 1
        if len(pageidSet) != 0:
            pageidSet = checkagain(word, pageidSet)
        wordDic[aeach] = pageidSet
        print(aeach + ': ' + str(len(pageidSet)))
        logger.info('num of pageid for ' + aeach + ': ' + str(len(pageidSet)))
        workeroutlist.append(wordDic)

    return workeroutlist


mapping = {
    "M1": "male_15-24",
    "M2": "male_25-34",
    "M3": "male_35-44",
    "M4": "male_45-54",
    "M5": "male_55-64",
    "M6": "male_65+",
    "F1": "female_15-24",
    "F2": "female_25-34",
    "F3": "female_35-44",
    "F4": "female_45-54",
    "F5": "female_55-64",
    "F6": "female_65+"
}
# use pageid to query footprint
maleLabel = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
femaleLabel = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6']


# for i in ['NA']+maleLabel+femaleLabel:
#     print(i)

# get footprint Info for each unique pageid
def worker_two(stepnum):
    global todopageidList
    global proc
    if len(todopageidList) % proc > proc // 2:
        step = len(todopageidList) // proc + 1
    else:
        step = len(todopageidList) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = todopageidList[s:]
    else:
        todo_worklist = todopageidList[s:s + step]

    esCluster = Elasticsearch(
        # hosts = ['192.168.21.20'],
        hosts=['192.168.21.11'],
        # http_auth=('elastic', 'breaktime168'),
        port=9200,
        timeout=36000
    )

    pageidcount = 0
    logger.info('len of todo_worklist: ' + str(len(todo_worklist)))
    pageidDict = {}
    for eachpageid in todo_worklist:

        # prepare returned dict
        if eachpageid not in pageidDict:
            pageidDict[eachpageid] = {}
            pageidDict[eachpageid]['pv'] = 0
            pageidDict[eachpageid]['fp'] = set()
            pageidDict[eachpageid]['url'] = ''
            pageidDict[eachpageid]['title'] = ''
            pageidDict[eachpageid]['meta'] = ''
            pageidDict[eachpageid]['author'] = ''
            pageidDict[eachpageid]['host'] = ''
            pageidDict[eachpageid]['path'] = ''
            tmp = {}
            for i in ['NA', 'M', 'F']:
                tmp[i] = 0
            pageidDict[eachpageid]['gender'] = tmp
            tmp = {}
            for i in maleLabel + femaleLabel:
                tmp[i] = 0
            pageidDict[eachpageid]['gender_age'] = tmp

        # first: query for index footprint_2018*
        body = {}
        body['_source'] = ["url", "hostname", "title", "meta_description",
                           "fp", "userage", "usergender", "path"]
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must'].append(
            {"term": {"pageid": eachpageid}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # logger.info(body)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # count = 0
        fpset = set()

        for doc in res:
            if pageidDict[eachpageid]['url'] == '':
                pageidDict[eachpageid]['url'] = ckattr(doc['_source'], 'url')
                pageidDict[eachpageid]['host'] = ckattr(
                    doc['_source'], 'hostname')
                pageidDict[eachpageid]['title'] = ckattr(
                    doc['_source'], 'title')
                pageidDict[eachpageid]['meta'] = ckattr(
                    doc['_source'], 'meta_description')
                pageidDict[eachpageid]['path'] = ckattr(doc['_source'], 'path')
                # pageidDict[eachpageid]['path'] = ckattr(doc['_source'], 'path')
                # author = ckattr(doc['_source'], 'author')
                if pageidDict[eachpageid]['path'] != '':
                    m = re.match(r'^/(@\w+?)/.*$',
                                 pageidDict[eachpageid]['path'])
                    if m:
                        author = m.group(1)
                    else:
                        author = ''
                pageidDict[eachpageid]['author'] = author
            pageidDict[eachpageid]['pv'] += 1
            fp = ckattr(doc['_source'], 'fp')
            gender = ckattr(doc['_source'], 'usergender')
            if gender in ['M', 'F']:
                pageidDict[eachpageid]['gender'][gender] += 1
            else:
                pageidDict[eachpageid]['gender']['NA'] += 1
            age = ckattr(doc['_source'], 'userage')
            label = getGAlabel(gender, age)
            if label != 'NA':
                pageidDict[eachpageid]['gender_age'][label] += 1
            fpset.add(fp)
            # count += 1
        pageidDict[eachpageid]['fp'] = fpset
        pageidcount += 1
    logger.info('num of processed pageid: ' + str(pageidcount))
    logger.info('worker done!')
    return pageidDict


if __name__ == '__main__':
    proc = len(wordlist) if numprocess > len(wordlist) else numprocess

    # print('proc: ' + str(proc))
    with mp.Pool() as pool:
        res = pool.map(worker_one, range(proc))

    # word2pageidList = []
    word2pageidList = [b for a in res for b in a]
    # each element is a dict for each keyword
    # print(word2pageidList)

    word2pageidDic = {}
    s = set()
    s = {x for adic in word2pageidList for ilist in adic.values()
         for x in ilist}
    logger.info('total unique pageid: ' + str(len(s)))
    # sys.exit()
    todopageidList = list(s)
    del res

    proc = len(todopageidList) if numprocess > len(
        todopageidList) else numprocess
    with mp.Pool() as pool:
        res = pool.map(worker_two, range(proc))

    z = {}
    for adic in res:
        z.update(adic)

    # print(z)
    # print(str(len(z.keys())))
    del res

    # output 1: perpage
    outfile = 'data/' + outputlabel + '_perpage.csv'
    outfiletop50 = 'data/' + outputlabel + '_perpage_top50.csv'
    header = ['pageid', 'url', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f, open(outfiletop50, 'w') as ft:
        writer = csv.writer(f)
        writer.writerow(header)
        writer_ft = csv.writer(ft)
        writer_ft.writerow(header)
        tmp = {}
        for pageid in z:
            tmp[pageid] = z[pageid]['pv']
        sortedk = sorted(tmp, key=tmp.get, reverse=True)
        count = 0
        for pageid in sortedk:
            count += 1
            outline = []
            outline.append(pageid)
            for i in ['url', 'host', 'title', 'meta', 'author', 'pv']:
                outline.append(z[pageid][i])
            outline.append(str(len(z[pageid]['fp'])))
            for i in ['NA', 'M', 'F']:
                outline.append(z[pageid]['gender'][i])
            for i in maleLabel + femaleLabel:
                outline.append(z[pageid]['gender_age'][i])
            writer.writerow(outline)
            if count <= 50:
                writer_ft.writerow(outline)

    # output 2: per word per page
    outfile = 'data/' + outputlabel + '_wordperpage.csv'
    outfiletop50 = 'data/' + outputlabel + '_wordperpage_top50.csv'
    header = ['word', 'pageid', 'url', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f, open(outfiletop50, 'w') as ft:
        writer = csv.writer(f)
        writer.writerow(header)
        writer_ft = csv.writer(ft)
        writer_ft.writerow(header)
        for adic in word2pageidList:
            for word, pageids in adic.items():
                tmp = {}
                for pageid in pageids:
                    tmp[pageid] = z[pageid]['pv']
                sortedk = sorted(tmp, key=tmp.get, reverse=True)
                count = 0
                for pageid in sortedk:
                    if pageid == '':
                        continue
                    count += 1
                    outline = []
                    outline.append(word)
                    outline.append(pageid)
                    for i in ['url', 'host', 'title', 'meta', 'author', 'pv']:
                        outline.append(z[pageid][i])
                    outline.append(str(len(z[pageid]['fp'])))
                    for i in ['NA', 'M', 'F']:
                        outline.append(z[pageid]['gender'][i])
                    for i in maleLabel + femaleLabel:
                        outline.append(z[pageid]['gender_age'][i])
                    writer.writerow(outline)
                    if count <= 50:
                        writer_ft.writerow(outline)

    # output 3: per word
    outfile = 'data/' + outputlabel + '_perword.csv'
    header = ['word', 'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                                     for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for adic in word2pageidList:
            for word, pageids in adic.items():
                outline = []
                outline.append(word)
                pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
                fpset = set()
                for pageid in pageids:
                    pv += z[pageid]['pv']
                    fpset.update(z[pageid]['fp'])
                    na += z[pageid]['gender']['NA']
                    m += z[pageid]['gender']['M']
                    f += z[pageid]['gender']['F']
                    m1 += z[pageid]['gender_age']['M1']
                    m2 += z[pageid]['gender_age']['M2']
                    m3 += z[pageid]['gender_age']['M3']
                    m4 += z[pageid]['gender_age']['M4']
                    m5 += z[pageid]['gender_age']['M5']
                    m6 += z[pageid]['gender_age']['M6']
                    f1 += z[pageid]['gender_age']['F1']
                    f2 += z[pageid]['gender_age']['F2']
                    f3 += z[pageid]['gender_age']['F3']
                    f4 += z[pageid]['gender_age']['F4']
                    f5 += z[pageid]['gender_age']['F5']
                    f6 += z[pageid]['gender_age']['F6']
                fp = str(len(fpset))
                for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
                    outline.append(i)
                writer.writerow(outline)

    # output 4: summary
    outfile = 'data/' + outputlabel + '_sum.csv'
    header = ['pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                             for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)

        # for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
        #     i = 0
        pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
        fpset = set()
        for pageid in z:
            pv += z[pageid]['pv']
            fpset.update(z[pageid]['fp'])
            na += z[pageid]['gender']['NA']
            m += z[pageid]['gender']['M']
            f += z[pageid]['gender']['F']
            m1 += z[pageid]['gender_age']['M1']
            m2 += z[pageid]['gender_age']['M2']
            m3 += z[pageid]['gender_age']['M3']
            m4 += z[pageid]['gender_age']['M4']
            m5 += z[pageid]['gender_age']['M5']
            m6 += z[pageid]['gender_age']['M6']
            f1 += z[pageid]['gender_age']['F1']
            f2 += z[pageid]['gender_age']['F2']
            f3 += z[pageid]['gender_age']['F3']
            f4 += z[pageid]['gender_age']['F4']
            f5 += z[pageid]['gender_age']['F5']
            f6 += z[pageid]['gender_age']['F6']
        outline = []
        fp = str(len(fpset))
        for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
            outline.append(i)
        writer.writerow(outline)

    logger.info('Done!')
