# query domain with keyword
{
  "query": {
    "bool": {
      "must": {
        "query_string": {
          "query": "*isunfar*",
          "fields": [
            "hostname"
          ]
        }
      }
    }
  }
}
