#!/root/anaconda3/envs/my_env/bin/python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
import multiprocessing as mp
# import json
import re
# import hashlib # decided to use pagid instead
# v1, query webpages related to eye disease
# v2, since there are 21% of webpages loss in index webpages, so keywords query should also be done on footprints_2018*. what a pain in the ass
# use update instead of union!
# elk returned title does not contain the queried keyword, double check with python
params = ['qfile', 'proc', 'S', 'E', 'out', 'operator']
argvs = sys.argv
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass
    # print(e)

startDateString = paramDic['S']
endDateString = paramDic['E']
if 'proc' in paramDic:
    numprocess = int(paramDic['proc'])
esidx = 'footprints_2018*'
estype = 'footprint'
outputlabel = paramDic['out']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)
logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

wordlist = []
with open(paramDic['qfile'], 'r') as f:
    wordlist = [line.strip('\n') for line in f]


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        # print(po[attr])
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))


# def initwordDic():
#     # total 8 elements
#     # [a,b,c,d,e,f,g,h]
#     # wordlist = ['日立', '冰箱', '洗衣機', '烘烤微波爐', '吸塵器', '投影機', '家電', '冷氣']
#     # wordlist was defined above
#     wordDic = {}
#     for eachword in wordlist:
#         wordDic[eachword] = 0
#     return wordlist, wordDic

# def toSQL(cursor, tablename, each10000list):
#     # [pageid, hostname, title, titleDic, meta_desc, metaDic]
#     datalist = ['pageid', 'url', 'hostname', 'author', 'title', 'title_stat',
#                 'meta_desc', 'meta_stat', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
#     insertcolstr = addacute(datalist)
#     sqlparam = ','.join(['%s'] * len(datalist))

#     sql = "INSERT INTO " + tablename + \
#         " (" + insertcolstr + ") VALUES (" + sqlparam + ")"
#     cursor.executemany(sql, each10000list)


def getGAlabel(gender, age):
    label = 'NA'
    if gender == 'M':
        if age >= 15 and age <= 24:
            label = 'M1'
        if age >= 25 and age <= 34:
            label = 'M2'
        if age >= 35 and age <= 44:
            label = 'M3'
        if age >= 45 and age <= 54:
            label = 'M4'
        if age >= 55 and age <= 64:
            label = 'M5'
        if age >= 65:
            label = 'M6'
    if gender == 'F':
        if age >= 15 and age <= 24:
            label = 'F1'
        if age >= 25 and age <= 34:
            label = 'F2'
        if age >= 35 and age <= 44:
            label = 'F3'
        if age >= 45 and age <= 54:
            label = 'F4'
        if age >= 55 and age <= 64:
            label = 'F5'
        if age >= 65:
            label = 'F6'
    return label


'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
step = delta // numprocess
worker_start = start
worker_end = end


def worker_one(stepnum):
    # split workers with wordlist, not by time range
    global proc
    if len(wordlist) % proc > proc // 2:
        step = len(wordlist) // proc + 1
    else:
        step = len(wordlist) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = wordlist[s:]
    else:
        todo_worklist = wordlist[s:s + step]

    workeroutlist = []
    logger.info(todo_worklist)
    for word in todo_worklist:
        # logger.info(word)

        body = {}
        body['_source'] = ["pageid", "title"]

        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must_not'] = []

        q_kw_list = []
        kw_list = []
        for each in word.split():
            q_kw_list.append('*' + each + '*')
            kw_list.append(each)

        # q_kw_str = ' AND '.join(q_kw_list)
        opstr = ' ' + paramDic['operator'] + ' '
        q_kw_str = opstr.join(q_kw_list)
        body['query']['bool']['must'].append(
            {"query_string": {"query": q_kw_str, "fields": ["title"]}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})

        logger.info(body)

        esCluster = Elasticsearch(
            # hosts = ['192.168.21.20'],
            hosts=['192.168.21.11'],
            # http_auth=('elastic', 'breaktime168'),
            port=9200,
            timeout=36000
        )
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )

        pageidSet = set()
        count = 0
        wordDic = {}
        for doc in res:  # each is a pageid
            pageid = ckattr(doc['_source'], 'pageid')
            title = ckattr(doc['_source'], 'title')
            matchflag = 0
            if title == '':
                continue
            if re.match(r'and', paramDic['operator'], re.I):
                for i in kw_list:
                    # print(i, title)
                    if re.search(i, title):
                        matchflag = 1
                    else:
                        matchflag = 0
            elif re.match(r'or', paramDic['operator'], re.I):
                for i in kw_list:
                    # print(i, title)
                    if re.search(i, title):
                        matchflag = 1
                        break
            if matchflag == 0:
                continue
            if pageid in pageidSet:
                continue
            pageidSet.add(pageid)
            count += 1
        wordDic[word] = pageidSet
        print(word + ': ' + str(len(pageidSet)))
        logger.info('num of pageid for ' + word + ': ' + str(len(pageidSet)))
        workeroutlist.append(wordDic)

    return workeroutlist


mapping = {
    "M1": "male_15-24",
    "M2": "male_25-34",
    "M3": "male_35-44",
    "M4": "male_45-54",
    "M5": "male_55-64",
    "M6": "male_65+",
    "F1": "female_15-24",
    "F2": "female_25-34",
    "F3": "female_35-44",
    "F4": "female_45-54",
    "F5": "female_55-64",
    "F6": "female_65+"
}
# use pageid to query footprint
maleLabel = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
femaleLabel = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6']


# for i in ['NA']+maleLabel+femaleLabel:
#     print(i)

# get footprint Info for each unique pageid
def worker_two(stepnum):
    global todopageidList
    global proc
    if len(todopageidList) % proc > proc // 2:
        step = len(todopageidList) // proc + 1
    else:
        step = len(todopageidList) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = todopageidList[s:]
    else:
        todo_worklist = todopageidList[s:s + step]

    esCluster = Elasticsearch(
        # hosts = ['192.168.21.20'],
        hosts=['192.168.21.18'],
        http_auth=('elastic', 'breaktime168'),
        port=9200,
        timeout=36000
    )

    pageidcount = 0
    logger.info('len of todo_worklist: ' + str(len(todo_worklist)))
    pageidDict = {}
    for eachpageid in todo_worklist:

        # prepare returned dict
        if eachpageid not in pageidDict:
            pageidDict[eachpageid] = {}
            pageidDict[eachpageid]['pv'] = 0
            pageidDict[eachpageid]['fp'] = set()
            pageidDict[eachpageid]['url'] = ''
            pageidDict[eachpageid]['title'] = ''
            pageidDict[eachpageid]['meta'] = ''
            pageidDict[eachpageid]['author'] = ''
            pageidDict[eachpageid]['host'] = ''
            pageidDict[eachpageid]['path'] = ''
            tmp = {}
            for i in ['NA', 'M', 'F']:
                tmp[i] = 0
            pageidDict[eachpageid]['gender'] = tmp
            tmp = {}
            for i in maleLabel + femaleLabel:
                tmp[i] = 0
            pageidDict[eachpageid]['gender_age'] = tmp

        # first: query for index footprint_2018*
        body = {}
        body['_source'] = ["url", "hostname", "title", "meta_description",
                           "fp", "userage", "usergender", "path"]
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must'].append(
            {"term": {"pageid": eachpageid}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # logger.info(body)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # count = 0
        fpset = set()

        for doc in res:
            if pageidDict[eachpageid]['url'] == '':
                pageidDict[eachpageid]['url'] = ckattr(doc['_source'], 'url')
                pageidDict[eachpageid]['host'] = ckattr(
                    doc['_source'], 'hostname')
                pageidDict[eachpageid]['title'] = ckattr(
                    doc['_source'], 'title')
                pageidDict[eachpageid]['meta'] = ckattr(
                    doc['_source'], 'meta_description')
                pageidDict[eachpageid]['path'] = ckattr(doc['_source'], 'path')
                # pageidDict[eachpageid]['path'] = ckattr(doc['_source'], 'path')
                # author = ckattr(doc['_source'], 'author')
                if pageidDict[eachpageid]['path'] != '':
                    m = re.match(r'^/(@\w+?)/.*$',
                                 pageidDict[eachpageid]['path'])
                    if m:
                        author = m.group(1)
                    else:
                        author = ''
                pageidDict[eachpageid]['author'] = author
            pageidDict[eachpageid]['pv'] += 1
            fp = ckattr(doc['_source'], 'fp')
            gender = ckattr(doc['_source'], 'usergender')
            if gender in ['M', 'F']:
                pageidDict[eachpageid]['gender'][gender] += 1
            else:
                pageidDict[eachpageid]['gender']['NA'] += 1
            age = ckattr(doc['_source'], 'userage')
            label = getGAlabel(gender, age)
            if label != 'NA':
                pageidDict[eachpageid]['gender_age'][label] += 1
            fpset.add(fp)
            # count += 1
        pageidDict[eachpageid]['fp'] = fpset
        pageidcount += 1
    logger.info('num of processed pageid: ' + str(pageidcount))
    logger.info('worker done!')
    return pageidDict


if __name__ == '__main__':
    proc = len(wordlist) if numprocess > len(wordlist) else numprocess

    # print('proc: ' + str(proc))
    with mp.Pool() as pool:
        res = pool.map(worker_one, range(proc))

    # word2pageidList = []
    word2pageidList = [b for a in res for b in a]
    # each element is a dict for each keyword
    # print(word2pageidList)

    word2pageidDic = {}
    s = set()
    s = {x for adic in word2pageidList for ilist in adic.values()
         for x in ilist}
    logger.info('total unique pageid: ' + str(len(s)))
    # sys.exit()
    todopageidList = list(s)
    del res

    proc = len(todopageidList) if numprocess > len(
        todopageidList) else numprocess
    with mp.Pool() as pool:
        res = pool.map(worker_two, range(proc))

    z = {}
    for adic in res:
        z.update(adic)

    # print(z)
    # print(str(len(z.keys())))
    del res

    # output 1: overall stats
    outfile = 'data/' + outputlabel + '_perpage.csv'
    header = ['pageid', 'url', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        tmp = {}
        for pageid in z:
            tmp[pageid] = z[pageid]['pv']
        sortedk = sorted(tmp, key=tmp.get, reverse=True)
        for pageid in sortedk:
            outline = []
            outline.append(pageid)
            for i in ['url', 'host', 'title', 'meta', 'author', 'pv']:
                outline.append(z[pageid][i])
            outline.append(str(len(z[pageid]['fp'])))
            for i in ['NA', 'M', 'F']:
                outline.append(z[pageid]['gender'][i])
            for i in maleLabel + femaleLabel:
                outline.append(z[pageid]['gender_age'][i])
            writer.writerow(outline)

    # output 2: word stats, show every page for every word
    outfile = 'data/' + outputlabel + '_wordperpage.csv'
    header = ['word', 'pageid', 'url', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for adic in word2pageidList:
            for word, pageids in adic.items():
                tmp = {}
                for pageid in pageids:
                    tmp[pageid] = z[pageid]['pv']
                sortedk = sorted(tmp, key=tmp.get, reverse=True)
                for pageid in sortedk:
                    if pageid == '':
                        continue
                    outline = []
                    outline.append(word)
                    outline.append(pageid)
                    for i in ['url', 'host', 'title', 'meta', 'author', 'pv']:
                        outline.append(z[pageid][i])
                    outline.append(str(len(z[pageid]['fp'])))
                    for i in ['NA', 'M', 'F']:
                        outline.append(z[pageid]['gender'][i])
                    for i in maleLabel + femaleLabel:
                        outline.append(z[pageid]['gender_age'][i])
                    writer.writerow(outline)

    # output 3: word stats, show status for each word
    outfile = 'data/' + outputlabel + '_perword.csv'
    header = ['word', 'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                                     for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        for adic in word2pageidList:
            for word, pageids in adic.items():
                outline = []
                outline.append(word)
                pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
                fpset = set()
                for pageid in pageids:
                    pv += z[pageid]['pv']
                    fpset.update(z[pageid]['fp'])
                    na += z[pageid]['gender']['NA']
                    m += z[pageid]['gender']['M']
                    f += z[pageid]['gender']['F']
                    m1 += z[pageid]['gender_age']['M1']
                    m2 += z[pageid]['gender_age']['M2']
                    m3 += z[pageid]['gender_age']['M3']
                    m4 += z[pageid]['gender_age']['M4']
                    m5 += z[pageid]['gender_age']['M5']
                    m6 += z[pageid]['gender_age']['M6']
                    f1 += z[pageid]['gender_age']['F1']
                    f2 += z[pageid]['gender_age']['F2']
                    f3 += z[pageid]['gender_age']['F3']
                    f4 += z[pageid]['gender_age']['F4']
                    f5 += z[pageid]['gender_age']['F5']
                    f6 += z[pageid]['gender_age']['F6']
                fp = str(len(fpset))
                for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
                    outline.append(i)
                writer.writerow(outline)

    # output 4: whole stats
    outfile = 'data/' + outputlabel + '_sum.csv'
    header = ['pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                             for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)

        # for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
        #     i = 0
        pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
        fpset = set()
        for pageid in z:
            pv += z[pageid]['pv']
            fpset.update(z[pageid]['fp'])
            na += z[pageid]['gender']['NA']
            m += z[pageid]['gender']['M']
            f += z[pageid]['gender']['F']
            m1 += z[pageid]['gender_age']['M1']
            m2 += z[pageid]['gender_age']['M2']
            m3 += z[pageid]['gender_age']['M3']
            m4 += z[pageid]['gender_age']['M4']
            m5 += z[pageid]['gender_age']['M5']
            m6 += z[pageid]['gender_age']['M6']
            f1 += z[pageid]['gender_age']['F1']
            f2 += z[pageid]['gender_age']['F2']
            f3 += z[pageid]['gender_age']['F3']
            f4 += z[pageid]['gender_age']['F4']
            f5 += z[pageid]['gender_age']['F5']
            f6 += z[pageid]['gender_age']['F6']
        outline = []
        fp = str(len(fpset))
        for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
            outline.append(i)
        writer.writerow(outline)

    logger.info('Done!')
