'''
fetch fp within 5/27~6/3 (input from cmd)
observe the user behavior within 5/14~6/3 (hard coded)
stat the daily wifi pv among a time range
v1, aim at calculating the nearest fp footprint. postponed!
v2, not the nearest but all within the time range
v2.1, change secdiff to mindiff. add title
v3, reformat the output to fit biz user's requirement
'''

from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
import logging
import multiprocessing as mp
import re
from operator import itemgetter
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

params = ['S', 'E', 'out', 'debug']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if param == 'debug':
        if f'--{param}' in argvs:
            debug = True
        else:
            debug = None
        continue
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# if debug:
#     print('debug: ' + str(debug))
# else:
#     print('debug: None')
# sys.exit()

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
# outfile = paramDic['out']


logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    '''update'''
    return po.get(attr, '')


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def dtdelta2min(td):
    s = timedelta.total_seconds(td)
    m = s / 60
    return f'{m:.2f}'  # rounded to second decimal place
    # td = f'{int(td)}'


def getGAlabel(gender, age):
    Glabel = 'NA'
    Alabel = 'NA'
    if gender == 'M':
        Glabel = gender
        if age >= 15 and age <= 24:
            Alabel = 'M1'
        if age >= 25 and age <= 34:
            Alabel = 'M2'
        if age >= 35 and age <= 44:
            Alabel = 'M3'
        if age >= 45 and age <= 54:
            Alabel = 'M4'
        if age >= 55 and age <= 64:
            Alabel = 'M5'
        if age >= 65:
            Alabel = 'M6'
    elif gender == 'F':
        Glabel = gender
        if age >= 15 and age <= 24:
            Alabel = 'F1'
        if age >= 25 and age <= 34:
            Alabel = 'F2'
        if age >= 35 and age <= 44:
            Alabel = 'F3'
        if age >= 45 and age <= 54:
            Alabel = 'F4'
        if age >= 55 and age <= 64:
            Alabel = 'F5'
        if age >= 65:
            Alabel = 'F6'
    return Glabel, Alabel


'''=================== self-defined functions end ==================='''

pathlist = ['/event/hcnicecloud/', '/event/hc-nice-hytqbest/',
            '/event/allianz', '/event/jec/']
# t_pathlist = ['hcnicecloud', 'hc-nice-hytqbest', ]
q_list = pathlist
start = datetime.strptime(startDateString, '%Y%m%d').strftime('%Y-%m-%d')
end = datetime.strptime(endDateString, '%Y%m%d').strftime('%Y-%m-%d')

# print(start, end)
# sys.exit()
proc = 8
stepsize = 1

# connection to ELK
esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker(stepnum):
    '''
    prepare fingerprint candidata
    '''
    # print(start, end)
    # sys.exit()
    global q_list
    global start  # get params from global
    global end  # get params from global
    # print(start, end)
    # sys.exit()
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    if debug:
        start = "2018-05-30"
        end = "2018-05-31"
    retDict = {}
    for each in todoworklist:
        # retDict[each] = []
        retDict.setdefault(each, [])

        body = {}
        body['_source'] = ["geoip.ip", "geoip.region_name", "fp",
                           "updated", "url", "userage", "usergender", "openlink"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['filter'].append(
            {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        body['query']['bool']['must'].append(
            {"term": {"path": each}})
        logger.info(body)

        # res = esCluster.search(
        #     index="footprints_2018*",
        #     doc_type="footprint",
        #     scroll="3m",
        #     search_type="query_then_fetch",
        #     size=0,
        #     body=body, request_timeout=300)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # total = res['hits']['total']
        # retDict[each][wifi] = total
        # print(each, wifi, total)
        # sys.exit()
        count = 0
        # outlist = []
        for doc in res:
            # print(doc)
            fp = doc['_source']['fp']
            # path = doc['_source']['path']
            ip = doc['_source']['geoip']['ip']
            ts = doc['_source']['updated']
            sex = ckattr(doc['_source'], 'usergender')
            age = ckattr(doc['_source'], 'userage')
            region = doc['_source']['geoip'].get('region_name', '')
            oplk = ckattr(doc['_source'], 'openlink')  # list
            cand_id_tp = (fp, ip, ts)  # 3 items
            cand_info_tp = (sex, age, region)  # 3 items
            # oplk_tuple = ()  # external, href, text
            oplk_list = []
            if oplk != '':
                ol_len = len(oplk)
                for tmpdict in oplk:
                    external = ckattr(tmpdict, 'external')
                    href = ckattr(tmpdict, 'href')
                    text = ckattr(tmpdict, 'text')
                    oplk_tuple = (external, href, text)  # 3 items
                    oplk_list.append(oplk_tuple)
            # [(tuple, tuple, list), (), ...]
            retDict[each].append((cand_id_tp, cand_info_tp, oplk_list))
            count += 1
            # if count == 5:
            #     break
        logger.info('num of processed doc: ' + str(count))

    return retDict


def getExtendedFpInfo(stepnum):
    '''
    get the previous and the next footprint of the candidate fingerprint in footprints
    '''

    # global q_list
    # list of tuple (fp, datetime), e.g. (720c22f395a9fea66aaa3ee55b22d4a3,2018-05-28T15:17:07)
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # retList = []
    start = "2018-05-14"
    end = "2018-06-03"
    if debug:
        start = "2018-05-30"
        end = "2018-05-31"
    rangedtformat = "yyyy-MM-dd"  # by day

    retDict = {}
    for eachtuple in todoworklist:
        each_id_tp, each_info_tp, eachol_list = eachtuple
        # ol_len, external, href, text = eachol_tp
        eachfp, eachip, eachts = each_id_tp
        keytuple = each_id_tp
        tObj = datetime.strptime(eachts, '%Y-%m-%dT%H:%M:%S')
        # start = tObj - timedelta(minutes=30)
        # end = tObj + timedelta(minutes=30)
        # logger.info('The candidate')
        # logger.info(keytuple)
        # rangedtformat = "yyyy-MM-dd HH:mm:ss"

        body = {}
        body['_source'] = ["path", "url", "geoip.ip",
                           "pageid", "fp", "updated", "title"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        # body['query']['bool']['filter'].append(
        #     {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": rangedtformat}}})
        # "time_zone": "+08:00",
        body['query']['bool']['filter'].append(
            {"term": {"fp": eachfp}})
        body['query']['bool']['filter'].append(
            {"term": {"geoip.ip": eachip}})

        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        count = 0

        eachDict = {}
        retDict[keytuple] = {}
        eachDict = retDict[keytuple]
        # eachDict['candidate'] = keytuple # tuple
        eachDict['sex'], eachDict['age'], eachDict['region'] = each_info_tp
        eachDict['openlink'] = eachol_list  # list
        eachDict.setdefault('prev', [])
        eachDict.setdefault('next', [])
        # eachDict['prev'] = []  # list
        # eachDict['next'] = []  # list
        # eachDict['bself'] = 0
        # eachDict['aself'] = 0
        # pdist = timedelta()
        # ndist = timedelta()

        for doc in res:
            fp = ip = ts = pageid = url = td = ''
            fp = doc['_source']['fp']
            ip = doc['_source']['geoip']['ip']
            path = doc['_source']['path']
            ts = doc['_source']['updated']
            pageid = doc['_source']['pageid']
            url = doc['_source']['url']
            ti = ckattr(doc['_source'], 'title')
            # ti = doc['_source'].get('title', '')
            # aimtuple = (fp, ip, ts, path, pageid, url)
            # aimtuple = (fp, ip, ts,)
            # logger.info(aimtuple)
            # continue
            if ts == eachts:  # same as candidate, skip!
                continue
            doctsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
            # logger.info(f'{tObj} vs {doctsObj}')
            if tObj > doctsObj:  # before section
                # if path == path_param:
                #     eachDict['bself'] += 1
                # td = dtdelta2min(tObj - doctsObj)  # the fourth in tuple
                aimtuple = (ts, pageid, url, ti)
                eachDict['prev'].append(aimtuple)
                # logger.info(f'before')
            elif tObj < doctsObj:  # after section
                # if path == path_param:
                #     eachDict['aself'] += 1
                # td = dtdelta2min(doctsObj - tObj)  # the fourth in tuple
                aimtuple = (ts, pageid, url, ti)
                eachDict['next'].append(aimtuple)
                # logger.info(f'after')
            count += 1
            # if count == 1:
            #     break
        # print(str(tObj), eachDict)
        eachDict['prev'] = sorted(
            eachDict['prev'], key=itemgetter(0), reverse=False)
        eachDict['next'] = sorted(eachDict['next'], key=itemgetter(0))
        # logger.info('num of processed doc for ' +
        #             str(keytuple) + ': ' + str(count))
        # logger.info('num of prev doc: ' + str(len(eachDict['prev'])))
        # logger.info('num of next doc: ' + str(len(eachDict['next'])))
        # retList.append(eachDict)
    # return retList
    return retDict


def multiproc():
    pass
    global q_list
    # global start
    # global end
    # try:
    procc = proc if proc <= len(q_list) else len(q_list)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))
    # except:
        # print('debug 002')
    # sys.exit()
    # merge dicts
    joinDict = {}
    for eachdict in res:
        # print(eachdict)
        # joinDict.update(eachdict)
        # try it!
        joinDict = dict(joinDict, **eachdict)

    # print(joinDict)
    # sys.exit()
    # simple stat
    # for k, il in joinDict.items():
    #     print(k, str(len(il)))

    outDict = {}
    for k, il in joinDict.items():
        procc = proc if proc <= len(il) else len(il)
        q_list = il
        global path_param
        path_param = k
        logger.info(f'num of candidate for {k!s}: {len(q_list)!s}')
        with mp.Pool() as pool:
            res = pool.map(getExtendedFpInfo, range(procc))
            bigDict = {}
            count = 0
            for eachDict in res:
                # count += 1
                # print(str(count))
                # bigDict = dict(bigDict, **eachDict)
                bigDict.update(eachDict)
            # for ik, iv in bigDict.items():
                # print(k, ik, len(iv['prev']), len(iv['next']))  # should be tuple
            outDict[k] = bigDict
    # for k, v in outDict.items():
    #     print(k, v)
    #     break
    # sys.exit()
    url_prefix = 'https://zi.media'
    # outfile1 = 'data/' + label + '_ui.csv'  # ui stands for user info
    # outfile2 = 'data/' + label + '_uo.csv'  # uo stands for user behavior
    # outfile3 = 'data/' + label + '_ub.csv'  # ub stands for user openlink
    # header1 = ['date', 'path', 'ol_count', 'gender',
    #            'age', 'region', 'pv']
    # header2 = ['date', 'path', 'ol_external', 'ol_href', 'ol_text']
    # header3 = ['date', 'path', 'gender', 'age',
    # 'type', 'datetime', 't_path', 't_pageid', 't_url', 't_title']
    # with open(outfile1, 'w') as out1, open(outfile2, 'w') as out2, open(outfile3, 'w') as out3:
    #     writer1 = csv.writer(out1)
    #     writer1.writerow(header1)
    #     writer2 = csv.writer(out2)
    #     writer2.writerow(header2)
    #     writer3 = csv.writer(out3)
    #     writer3.writerow(header3)

    out_label_dict = {}
    count = 1
    for i in range(2 + len(pathlist)):
        if i == 0:
            out_label_dict[i] = 'ui'
        elif i == 1:
            out_label_dict[i] = 'uo'
        else:
            out_label_dict[i] = f'ub_{count:02}'
            count += 1

    Amapping = {
        "M1": "male_15-24",
        "M2": "male_25-34",
        "M3": "male_35-44",
        "M4": "male_45-54",
        "M5": "male_55-64",
        "M6": "male_65+",
        "F1": "female_15-24",
        "F2": "female_25-34",
        "F3": "female_35-44",
        "F4": "female_45-54",
        "F5": "female_55-64",
        "F6": "female_65+"
    }
    # use pageid to query footprint
    maleLabel = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
    femaleLabel = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6']

    out_data_dict = {}
    for i in range(2 + len(pathlist)):
        out_data_dict[i] = []

    out_header_dict = {}
    for i in range(2 + len(pathlist)):
        if i == 0:
            tmplist = [Amapping[i] for i in maleLabel + femaleLabel]
            out_header_dict[i] = ['c_url', 'olcount', 'pv', 'M', 'F'] + tmplist
        elif i == 1:
            out_header_dict[i] = ['candidate', 'href', 'text', 'eventcount']
        else:
            out_header_dict[i] = ['c_url', 'prev7d_url', 'pv',
                                  'prev30m_url', 'pv', 'prev15m', 'pv', 'post15m_url', 'pv', 'post30m_url', 'pv', 'post7d_url', 'pv']

    # prepare the list of tuple first, print out later
    outlist = []
    uiDict = {}
    uoDict = {}
    ubDict = {}
    for cpath, eachDict in outDict.items():
        curl = url_prefix + cpath  # e.g. https://zi.media/event/hcnicecloud/
        mObj = re.match(r'^/event/(\S+?)/?$', cpath)
        if mObj:
            print(f'{cpath!s} matched!')
            cpath = mObj.group(1)
        uoDict.setdefault(cpath, {})
        uiDict.setdefault(curl, {})
        ubDict.setdefault(cpath, {})
        ubDict[cpath].setdefault('prev15m', {})
        ubDict[cpath].setdefault('prev30m', {})
        ubDict[cpath].setdefault('prev7d', {})
        ubDict[cpath].setdefault('post15m', {})
        ubDict[cpath].setdefault('post30m', {})
        ubDict[cpath].setdefault('post7d', {})
        for i in ['olcount', 'pv', 'M', 'F', 'M1', 'M2', 'M3', 'M4', 'M5', 'M6', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6']:
            uiDict[curl].setdefault(i, 0)
        for ktuple, kdict in eachDict.items():
            fp, ip, dt = ktuple  # candidate
            dtObj = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S')
            sex, age, region, ol_list, prev_list, next_list = kdict['sex'], kdict[
                'age'], kdict['region'], kdict['openlink'], kdict['prev'], kdict['next']
            pv = len(kdict['prev']) + len(kdict['next']) + 1
            uiDict[curl]['pv'] += pv
            day = dt.split('T')[0]  # the date part
            olcount = len(ol_list)
            uiDict[curl]['olcount'] += olcount
            Glabel, Alabel = getGAlabel(sex, age)
            if Glabel != 'NA':
                uiDict[curl][Glabel] += 1
            if Alabel != 'NA':
                uiDict[curl][Alabel] += 1
            # outlist = []
            # outlist = tuple([i for i in [day, curl, str(
            # len(ol_list)), sex, age, pv]])
            # out_data_dict[0].append(outlist)
            for external, href, text in ol_list:
                clean_href = href.split('?')[0]
                if re.search(r'http://', clean_href):
                    logger.info(f'mysterious openlink href: {clean_href!s}')
                mObj = re.match(r'^https?://(\S+)$', clean_href)
                if mObj:
                    clean_href = 'https://' + mObj.group(1)
                uoDict[cpath].setdefault(clean_href, {})
                uoDict[cpath][clean_href].setdefault('text', {})
                # uoDict[cpath][clean_href].setdefault('eventcount', 0)
                uoDict[cpath][clean_href]['text'].setdefault(text, 0)

                # uoDict[cpath][clean_href]['text'].append(text)
                uoDict[cpath][clean_href]['text'][text] += 1
                # uoDict[cpath][clean_href]['eventcount'] += 1
                # outlist = []
                # outlist = tuple([i for i in [day, curl, external, href, text]])
                # out_data_dict[1].append(outlist)
            # before
            for ts, pageid, url, ti in prev_list:  # should be sorted already
                url = url.split('?')[0]
                ubDict[cpath]['prev15m'].setdefault(url, 0)
                ubDict[cpath]['prev30m'].setdefault(url, 0)
                ubDict[cpath]['prev7d'].setdefault(url, 0)
                tsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
                # outlist = []  # actually it's a tuple
                # outlist = tuple([i for i in [day, curl, ts, pageid, url, ti]])
                s = timedelta.total_seconds(dtObj - tsObj)
                if s < 15 * 60:
                    ubDict[cpath]['prev15m'][url] += 1
                    # out_data_dict[2].append(outlist)
                if s < 30 * 60:
                    ubDict[cpath]['prev30m'][url] += 1
                    # out_data_dict[3].append(outlist)
                if s < 60 * 60 * 24 * 7:
                    ubDict[cpath]['prev7d'][url] += 1
                    # out_data_dict[4].append(outlist)
            # after
            for ts, pageid, url, ti in next_list:  # should be sorted already
                url = url.split('?')[0]
                ubDict[cpath]['post15m'].setdefault(url, 0)
                ubDict[cpath]['post30m'].setdefault(url, 0)
                ubDict[cpath]['post7d'].setdefault(url, 0)
                tsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
                outlist = []  # actually it's a tuple
                outlist = tuple([i for i in [day, curl, ts, pageid, url, ti]])
                s = timedelta.total_seconds(tsObj - dtObj)
                if s < 15 * 60:
                    ubDict[cpath]['post15m'][url] += 1
                    # out_data_dict[5].append(outlist)
                if s < 30 * 60:
                    ubDict[cpath]['post30m'][url] += 1
                    # out_data_dict[6].append(outlist)
                if s < 60 * 60 * 24 * 7:
                    ubDict[cpath]['post7d'][url] += 1
                    # out_data_dict[7].append(outlist)

    for k, v in uiDict.items():  # v is a dict
        outlist = []
        outlist = [k] + [v[i] for i in ['olcount', 'pv', 'M', 'F', 'M1',
                                        'M2', 'M3', 'M4', 'M5', 'M6', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6']]
        # not yet store int as str
        outlist = tuple(outlist)
        out_data_dict[0].append(outlist)

    for k, v in uoDict.items():  # v is a dict
        for ik, iv in v.items():
            for text, evcount in iv['text'].items():
                outlist = []
                outlist = [k, ik, text, evcount]
                # not yet store int as str
                outlist = tuple(outlist)
                out_data_dict[1].append(outlist)

    top = 20  # top 20 url with the most pv
    # keycount = 2
    orderlist = ['prev7d', 'prev30m', 'prev15m',
                 'post15m', 'post30m', 'post7d']
    for keycount, (cpath, v) in enumerate(ubDict.items(), 2):
        adict = {}
        for count, idict in enumerate([v[i] for i in orderlist], 1):
            sortedKeys = sorted(idict, key=idict.get, reverse=True)
            # list of tuples (url, pv) of len 20
            tmplist = [(url, idict[url]) for url in sortedKeys[:top]]
            adict[count] = tmplist
        for i in range(top):
            outlist = []
            outlist.append(cpath)
            outlist = outlist + \
                [tp for ii in range(1, 7) for tp in adict[ii][i]]
            # for url, pv in [\
            # url, pv for count in range(1, 7) for url, pv in adict[count][i]]:
            #     outlist.append(url)
            #     outlist.append(pv)
            out_data_dict[keycount].append(outlist)
        # keycount += 1
        # for count, idict in adict.items():

            # aimtuple = (ts, pageid, url, ti)

            # sort all the list as desired before print out
            # sort accordingly by type
    logger.info('Sorting...')
    for k, v in out_data_dict.items():
        if k == 0:
            v = sorted(v, key=itemgetter(1), reverse=True)
            out_data_dict[k] = sorted(v, key=itemgetter(2), reverse=True)
        elif k == 1:
            v = sorted(v, key=itemgetter(3), reverse=True)
            out_data_dict[k] = sorted(v, key=itemgetter(0), reverse=False)
        else:
            continue
            # v = sorted(v, key=itemgetter(2), reverse=False)
            # out_data_dict[k] = sorted(v, key=itemgetter(0), reverse=False)

    for no, tag in out_label_dict.items():
        # try f string
        output = f'data/{label!s}_{tag!s}.csv'
        with open(output, 'w') as out:
            writer = csv.writer(out)
            writer.writerow(out_header_dict[no])
            writer.writerows(out_data_dict[no])


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
