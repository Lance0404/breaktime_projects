'''
fetch fp within 5/27~6/3 (input from cmd)
observe the user behavior within 5/14~6/3 (hard coded)
stat the daily wifi pv among a time range
v1, aim at calculating the nearest fp footprint. postponed!
v2, not the nearest but all within the time range
v2.1, change secdiff to mindiff. add title
v3, reformat the output to fit biz user's requirement
'''

from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
import logging
import multiprocessing as mp
from operator import itemgetter
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

params = ['S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
# outfile = paramDic['out']


logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    '''update'''
    return po.get(attr, '')


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def dtdelta2min(td):
    s = timedelta.total_seconds(td)
    m = s / 60
    return f'{m:.2f}'  # rounded to second decimal place
    # td = f'{int(td)}'


'''=================== self-defined functions end ==================='''

pathlist = ['/event/hcnicecloud/', '/event/hc-nice-hytqbest/',
            '/event/allianz', '/event/jec/']
q_list = pathlist
start = datetime.strptime(startDateString, '%Y%m%d').strftime('%Y-%m-%d')
end = datetime.strptime(endDateString, '%Y%m%d').strftime('%Y-%m-%d')

proc = 8
stepsize = 1

# connection to ELK
esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker(stepnum):
    '''
    prepare fingerprint candidata
    '''

    global q_list
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    retDict = {}
    for each in todoworklist:
        # retDict[each] = []
        retDict.setdefault(each, [])

        body = {}
        body['_source'] = ["geoip.ip", "geoip.region_name", "fp",
                           "updated", "url", "userage", "usergender", "openlink"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['filter'].append(
            {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        body['query']['bool']['must'].append(
            {"term": {"path": each}})
        logger.info(body)

        # res = esCluster.search(
        #     index="footprints_2018*",
        #     doc_type="footprint",
        #     scroll="3m",
        #     search_type="query_then_fetch",
        #     size=0,
        #     body=body, request_timeout=300)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # total = res['hits']['total']
        # retDict[each][wifi] = total
        # print(each, wifi, total)
        # sys.exit()
        count = 0
        # outlist = []
        for doc in res:
            # print(doc)
            fp = doc['_source']['fp']
            # path = doc['_source']['path']
            ip = doc['_source']['geoip']['ip']
            ts = doc['_source']['updated']
            sex = ckattr(doc['_source'], 'usergender')
            age = ckattr(doc['_source'], 'userage')
            region = doc['_source']['geoip'].get('region_name', '')
            oplk = ckattr(doc['_source'], 'openlink')  # list
            cand_id_tp = (fp, ip, ts)  # 3 items
            cand_info_tp = (sex, age, region)  # 3 items
            # oplk_tuple = ()  # external, href, text
            oplk_list = []
            if oplk != '':
                ol_len = len(oplk)
                for tmpdict in oplk:
                    external = ckattr(tmpdict, 'external')
                    href = ckattr(tmpdict, 'href')
                    text = ckattr(tmpdict, 'text')
                    oplk_tuple = (external, href, text)  # 3 items
                    oplk_list.append(oplk_tuple)
            # [(tuple, tuple, list), (), ...]
            retDict[each].append((cand_id_tp, cand_info_tp, oplk_list))
            count += 1
            # if count == 5:
            #     break
        logger.info('num of processed doc: ' + str(count))

    return retDict


def getExtendedFpInfo(stepnum):
    '''
    get the previous and the next footprint of the candidate fingerprint in footprints
    '''

    # global q_list
    # list of tuple (fp, datetime), e.g. (720c22f395a9fea66aaa3ee55b22d4a3,2018-05-28T15:17:07)
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # retList = []
    retDict = {}
    for eachtuple in todoworklist:
        each_id_tp, each_info_tp, eachol_list = eachtuple
        # ol_len, external, href, text = eachol_tp
        eachfp, eachip, eachts = each_id_tp
        keytuple = each_id_tp
        tObj = datetime.strptime(eachts, '%Y-%m-%dT%H:%M:%S')
        # start = tObj - timedelta(minutes=30)
        # end = tObj + timedelta(minutes=30)
        # logger.info('The candidate')
        # logger.info(keytuple)
        # rangedtformat = "yyyy-MM-dd HH:mm:ss"
        start = "2018-05-14"
        end = "2018-06-03"
        rangedtformat = "yyyy-MM-dd"  # by day
        body = {}
        body['_source'] = ["path", "url", "geoip.ip",
                           "pageid", "fp", "updated", "title"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        # body['query']['bool']['filter'].append(
        #     {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": rangedtformat}}})
        # "time_zone": "+08:00",
        body['query']['bool']['filter'].append(
            {"term": {"fp": eachfp}})
        body['query']['bool']['filter'].append(
            {"term": {"geoip.ip": eachip}})

        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        count = 0

        eachDict = {}
        retDict[keytuple] = {}
        eachDict = retDict[keytuple]
        # eachDict['candidate'] = keytuple # tuple
        eachDict['sex'], eachDict['age'], eachDict['region'] = each_info_tp
        eachDict['openlink'] = eachol_list  # list
        eachDict.setdefault('prev', [])
        eachDict.setdefault('next', [])
        # eachDict['prev'] = []  # list
        # eachDict['next'] = []  # list
        # eachDict['bself'] = 0
        # eachDict['aself'] = 0
        # pdist = timedelta()
        # ndist = timedelta()

        for doc in res:
            fp = ip = ts = pageid = url = td = ''
            fp = doc['_source']['fp']
            ip = doc['_source']['geoip']['ip']
            path = doc['_source']['path']
            ts = doc['_source']['updated']
            pageid = doc['_source']['pageid']
            url = doc['_source']['url']
            ti = ckattr(doc['_source'], 'title')
            # ti = doc['_source'].get('title', '')
            # aimtuple = (fp, ip, ts, path, pageid, url)
            # aimtuple = (fp, ip, ts,)
            # logger.info(aimtuple)
            # continue
            if ts == eachts:  # same as candidate, skip!
                continue
            doctsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
            # logger.info(f'{tObj} vs {doctsObj}')
            if tObj > doctsObj:  # before section
                # if path == path_param:
                #     eachDict['bself'] += 1
                # td = dtdelta2min(tObj - doctsObj)  # the fourth in tuple
                aimtuple = (ts, pageid, url, ti)
                eachDict['prev'].append(aimtuple)
                # logger.info(f'before')
            elif tObj < doctsObj:  # after section
                # if path == path_param:
                #     eachDict['aself'] += 1
                # td = dtdelta2min(doctsObj - tObj)  # the fourth in tuple
                aimtuple = (ts, pageid, url, ti)
                eachDict['next'].append(aimtuple)
                # logger.info(f'after')
            count += 1
            # if count == 1:
            #     break
        # print(str(tObj), eachDict)
        eachDict['prev'] = sorted(
            eachDict['prev'], key=itemgetter(0), reverse=False)
        eachDict['next'] = sorted(eachDict['next'], key=itemgetter(0))
        # logger.info('num of processed doc for ' +
        #             str(keytuple) + ': ' + str(count))
        # logger.info('num of prev doc: ' + str(len(eachDict['prev'])))
        # logger.info('num of next doc: ' + str(len(eachDict['next'])))
        # retList.append(eachDict)
    # return retList
    return retDict


def multiproc():
    pass
    global q_list

    procc = proc if proc <= len(q_list) else len(q_list)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))

    # merge dicts
    joinDict = {}
    for eachdict in res:
        # print(eachdict)
        # joinDict.update(eachdict)
        # try it!
        joinDict = dict(joinDict, **eachdict)

    # print(joinDict)
    # sys.exit()
    # simple stat
    # for k, il in joinDict.items():
    #     print(k, str(len(il)))

    outDict = {}
    for k, il in joinDict.items():
        procc = proc if proc <= len(il) else len(il)
        q_list = il
        global path_param
        path_param = k
        logger.info(f'num of candidate for {k!s}: {len(q_list)!s}')
        with mp.Pool() as pool:
            res = pool.map(getExtendedFpInfo, range(procc))
            bigDict = {}
            count = 0
            for eachDict in res:
                # count += 1
                # print(str(count))
                # bigDict = dict(bigDict, **eachDict)
                bigDict.update(eachDict)
            # for ik, iv in bigDict.items():
                # print(k, ik, len(iv['prev']), len(iv['next']))  # should be tuple
            outDict[k] = bigDict
    # for k, v in outDict.items():
    #     print(k, v)
    #     break
    # sys.exit()
    url_prefix = 'https://zi.media'
    # outfile1 = 'data/' + label + '_ui.csv'  # ui stands for user info
    # outfile2 = 'data/' + label + '_uo.csv'  # uo stands for user behavior
    # outfile3 = 'data/' + label + '_ub.csv'  # ub stands for user openlink
    # header1 = ['date', 'path', 'ol_count', 'gender',
    #            'age', 'region', 'pv']
    # header2 = ['date', 'path', 'ol_external', 'ol_href', 'ol_text']
    # header3 = ['date', 'path', 'gender', 'age',
    # 'type', 'datetime', 't_path', 't_pageid', 't_url', 't_title']
    # with open(outfile1, 'w') as out1, open(outfile2, 'w') as out2, open(outfile3, 'w') as out3:
    #     writer1 = csv.writer(out1)
    #     writer1.writerow(header1)
    #     writer2 = csv.writer(out2)
    #     writer2.writerow(header2)
    #     writer3 = csv.writer(out3)
    #     writer3.writerow(header3)

    out_label_dict = {
        0: "ui",
        1: "uo",
        2: "ub_b15m",
        3: "ub_b30m",
        4: "ub_b7d",
        5: "ub_a15m",
        6: "ub_a30m",
        7: "ub_a7d"
    }

    out_data_dict = {}
    for i in range(8):
        out_data_dict[i] = []

    out_header_dict = {}
    for i in range(8):
        if i == 0:
            out_header_dict[i] = ['date', 'c_url',
                                  'olcount', 'gender', 'age', 'pv']
        elif i == 1:
            out_header_dict[i] = ['date', 'c_url', 'external', 'href', 'text']
        else:
            out_header_dict[i] = ['date', 'c_url',
                                  'datetime', 'pageid', 'url', 'title']

    # prepare the list of tuple first, print out later
    outlist = []
    for cpath, eachDict in outDict.items():
        curl = url_prefix + cpath  # e.g. https://zi.media/event/hcnicecloud/
        for ktuple, kdict in eachDict.items():
            fp, ip, dt = ktuple
            dtObj = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S')
            sex, age, region, ol_list, prev_list, next_list = kdict['sex'], kdict[
                'age'], kdict['region'], kdict['openlink'], kdict['prev'], kdict['next']
            pv = len(kdict['prev']) + len(kdict['next']) + 1
            day = dt.split('T')[0]  # the date part
            outlist = []
            outlist = tuple([i for i in [day, curl, str(
                len(ol_list)), sex, age, pv]])
            out_data_dict[0].append(outlist)
            for external, href, text in ol_list:
                outlist = []
                outlist = tuple([i for i in [day, curl, external, href, text]])
                out_data_dict[1].append(outlist)
            # before
            for ts, pageid, url, ti in prev_list:  # should be sorted already
                tsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
                outlist = []  # actually it's a tuple
                outlist = tuple([i for i in [day, curl, ts, pageid, url, ti]])
                s = timedelta.total_seconds(dtObj - tsObj)
                if s < 15 * 60:
                    out_data_dict[2].append(outlist)
                if s < 30 * 60:
                    out_data_dict[3].append(outlist)
                if s < 60 * 60 * 24 * 7:
                    out_data_dict[4].append(outlist)
            # after
            for ts, pageid, url, ti in next_list:  # should be sorted already
                tsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
                outlist = []  # actually it's a tuple
                outlist = tuple([i for i in [day, curl, ts, pageid, url, ti]])
                s = timedelta.total_seconds(tsObj - dtObj)
                if s < 15 * 60:
                    out_data_dict[5].append(outlist)
                if s < 30 * 60:
                    out_data_dict[6].append(outlist)
                if s < 60 * 60 * 24 * 7:
                    out_data_dict[7].append(outlist)

# aimtuple = (ts, pageid, url, ti)

    # sort all the list as desired before print out
    # sort accordingly by type
    logger.info('Sorting!')
    for k, v in out_data_dict.items():
        if k == 0:
            v = sorted(v, key=itemgetter(5), reverse=True)
            out_data_dict[k] = sorted(v, key=itemgetter(0), reverse=False)
        elif k == 1:
            v = sorted(v, key=itemgetter(1), reverse=True)
            out_data_dict[k] = sorted(v, key=itemgetter(0), reverse=False)
        else:
            v = sorted(v, key=itemgetter(2), reverse=False)
            out_data_dict[k] = sorted(v, key=itemgetter(0), reverse=False)

    for no, tag in out_label_dict.items():
        # try f string
        output = f'data/{label!s}_{tag!s}.csv'
        with open(output, 'w') as out:
            writer = csv.writer(out)
            writer.writerow(out_header_dict[no])
            writer.writerows(out_data_dict[no])


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
