import re
import sys
sys.path.append('../tools/')
from common.config import logger, dirmaster, parseCMD


# Lance test 3q
# ========== hyper param ==========
mustparams = ['in']
optionalparams = []
# ========== ========== ==========

paramDic = parseCMD(sys.argv, mustparams, optionalparams)


def word2elkqstr(word: 'str') -> 'str':
    '''
    only support (A B)(A C)... for now
    can't support (A B (A C)), ((A B)(A C))
    '''
    if re.match(r'\((.+)\)', word):
        items = [i for i in re.findall(r'\(.+?\)', word)]
    else:    # w/o '()'
        items = word.split()
    print(items)
    # items = [i.strip(' ') for i in re.split(r'OR', word, flags=re.I)]
    # support (A B) OR (A C) OR ...
    # use 'OR' as or operator
    if len(items) > 1:
        tmplist = [word2elkqstr(item) for item in items]
        # print(tmplist)
        itemstr = ' OR '.join(tmplist)
    elif len(items) == 1:
        word = items[0]
        mObj = re.match(r'^\((.+)\)$', word)
        if mObj:
            tmplist = ['*' + i + '*' for i in mObj.group(1).split()]
            itemstr = ' AND '.join(tmplist)
            itemstr = f'({itemstr!s})'
        else:
            tmplist = ['*' + i + '*' for i in word.split()]
            itemstr = ' OR '.join(tmplist)
            itemstr = f'({itemstr!s})'
    return itemstr

def ismatch(keyword, title):
    if title == '': return False
    if re.match(r'\((.+)\)', keyword):
        items = [i for i in re.findall(r'\(.+?\)', keyword)]
    else:    # w/o '()'
        items = [i.strip(' ') for i in keyword.split() if i != '']
    check = lambda x, y: True if re.search(x, y, re.I) != None else False
    if len(items) == 1:
        m = re.match(r'^\((.+)\)$', keyword)
        if m: # and
            tmplist = m.group(1).split()
            retlist = [check(i, title) for i in tmplist]
            return False if False in retlist else True
        else: # singleton w/o ()
            if re.search(items[0], title): return True
    elif len(items) > 1: # or, either 'a b' or '(a b)(a c)'
        retlist = [ismatch(i, title) for i in items]
        return True if True in retlist else False
    else:
        return False


wordlist = []
with open(paramDic['in'], 'r') as f:
    wordlist = [line.strip('\n ') for line in f if re.match(r'^\s+?$', line) == None]

print(wordlist)
sys.exit()
# title = '曼秀雷敦 護唇'

a = '(曼秀雷敦 護唇) (曼秀雷敦 潤唇)'
b = '(曼秀雷敦 護唇)'

alist = [
'((曼秀雷敦 護唇) (曼秀雷敦 潤唇))',
'(曼秀雷敦 護唇)(曼秀雷敦 潤唇(曼秀雷敦 潤唇))',
# '(曼秀雷敦 護唇)',
# '曼秀雷敦 護唇'
]

titlelist = [
'曼秀雷敦的護唇好吃',
'曼秀雷敦的潤唇好吃',
'曼秀雷敦的這品牌很爛',
'潤唇膏'

]


tmplist = [ismatch(a, i) for i in titlelist]
print(f'result: {tmplist}')

# tmplist = [word2elkqstr(i) for i in alist]
# print(tmplist)
# check = word2elkqstr(a)
# check = word2elkqstr(b)
# print(check)
