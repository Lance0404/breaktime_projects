#!/root/anaconda3/envs/my_env/bin/python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
import multiprocessing as mp

# import json
import re
# import hashlib # decided to use pagid instead
# v1, query webpages related to eye disease

params = ['qfile', 'proc', 'S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param ' + param + ' is missing!')
try:
    if err:
        sys.exit()
except Exception as e:
    pass
    # print(e)

startDateString = paramDic['S']
endDateString = paramDic['E']
if 'proc' in paramDic:
    numprocess = int(paramDic['proc'])
esidx = 'webpages'
estype = 'webpage'
outputlabel = paramDic['out']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

wordlist = []
with open(paramDic['qfile'], 'r') as f:
    wordlist = [line.strip('\n') for line in f]
    # wordlist.append(x)

# x = [len(i.split()) for i in wordlist]
# # print(x)
# for x,y in zip(wordlist, x):
#     print(x,y)
# print(wordlist)
# print(str(len(wordlist)))
# sys.exit()
'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        # print(po[attr])
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))


def initwordDic():
    # total 8 elements
    # [a,b,c,d,e,f,g,h]
    # wordlist = ['日立', '冰箱', '洗衣機', '烘烤微波爐', '吸塵器', '投影機', '家電', '冷氣']
    # wordlist was defined above
    wordDic = {}
    for eachword in wordlist:
        wordDic[eachword] = 0
    return wordlist, wordDic


def toSQL(cursor, tablename, each10000list):
    # [pageid, hostname, title, titleDic, meta_desc, metaDic]
    datalist = ['pageid', 'url', 'hostname', 'author', 'title', 'title_stat',
                'meta_desc', 'meta_stat', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    insertcolstr = addacute(datalist)
    sqlparam = ','.join(['%s'] * len(datalist))

    sql = "INSERT INTO " + tablename + \
        " (" + insertcolstr + ") VALUES (" + sqlparam + ")"
    cursor.executemany(sql, each10000list)


def getGAlabel(gender, age):
    label = 'NA'
    if gender == 'M':
        if age >= 15 and age <= 24:
            label = 'M1'
        if age >= 25 and age <= 34:
            label = 'M2'
        if age >= 35 and age <= 44:
            label = 'M3'
        if age >= 45 and age <= 54:
            label = 'M4'
        if age >= 55 and age <= 64:
            label = 'M5'
        if age >= 65:
            label = 'M6'
    if gender == 'F':
        if age >= 15 and age <= 24:
            label = 'F1'
        if age >= 25 and age <= 34:
            label = 'F2'
        if age >= 35 and age <= 44:
            label = 'F3'
        if age >= 45 and age <= 54:
            label = 'F4'
        if age >= 55 and age <= 64:
            label = 'F5'
        if age >= 65:
            label = 'F6'
    return label


'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
step = delta // numprocess
worker_start = start
worker_end = end

if 0:
    conn = pymysql.connect(
        host='localhost',
        user='lance',
        password='',
        db='breaktime',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )

    # tablename = 'hitachi_not_refrig'
    droptbstr = '''DROP TABLE IF EXISTS ''' + tablename
    createtbstr = '''CREATE TABLE IF NOT EXISTS ''' + tablename + '''
        (id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
        pageid VARCHAR(40) NOT NULL,
        url TEXT,
        hostname VARCHAR(100),
        author TEXT,
        title TEXT,
        title_stat TEXT,
        meta_desc TEXT,
        meta_stat TEXT,
        a INT UNSIGNED DEFAULT 0,
        b INT UNSIGNED DEFAULT 0,
        c INT UNSIGNED DEFAULT 0,
        d INT UNSIGNED DEFAULT 0,
        e INT UNSIGNED DEFAULT 0,
        f INT UNSIGNED DEFAULT 0,
        g INT UNSIGNED DEFAULT 0,
        h INT UNSIGNED DEFAULT 0,
        KEY `pageid` (`pageid`),
        KEY `hostname` (`hostname`),
        KEY `a` (`a`),
        KEY `b` (`b`),
        KEY `c` (`c`),
        KEY `d` (`d`),
        KEY `e` (`e`),
        KEY `f` (`f`),
        KEY `g` (`g`),
        KEY `h` (`h`)
        ) '''

    try:
        with conn.cursor() as cursor:
            cursor.execute(droptbstr)
            cursor.execute(createtbstr)
    except Exception as e:
        logger.error(e)


def worker_one(stepnum):
    # split workers with wordlist, not by time range
    global proc
    if len(wordlist) % proc > proc // 2:
        step = len(wordlist) // proc + 1
    else:
        step = len(wordlist) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = wordlist[s:]
    else:
        todo_worklist = wordlist[s:s + step]

    # worker_start = start + step * stepnum
    # if stepnum == numprocess - 1:
    #     worker_end = end
    # else:
    #     worker_end = worker_start + step - timedelta(days=1)
    # print(str(len(todo_worklist)))
    # print(todo_worklist)
    workeroutlist = []
    workerwordPageidInfoList = []
    logger.info(todo_worklist)
    for word in todo_worklist:
        logger.info(word)

        body = {}
        body['_source'] = ["url", "hostname",
                           "title", "meta_description", "author"]

        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must_not'] = []

        for each in word.split():
            body['query']['bool']['filter'].append(
                {"match_phrase": {"title": each}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # this has no effect
        # body['query']['bool']['filter'].append(
        #     {"regexp": {"author_name": ".*"}})

        logger.info(body)

        esCluster = Elasticsearch(
            # hosts = ['192.168.21.20'],
            hosts=['192.168.21.18'],
            http_auth=('elastic', 'breaktime168'),
            port=9200,
            timeout=36000
        )
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index=esidx,
            doc_type=estype,
            request_timeout=5000
        )

        count = 0
        outlist = []
        wordPageidInfoList = []
        wordPageidInfo = {}  # store pageidInfo Dict
        eachcount = 0
        wordDic = {}
        pageidInfo = {}
        for doc in res:  # each is a pageid
            pageid = doc['_id']
            # prepare pageid dic

            pageidInfo[pageid] = {}
            pageidInfo[pageid]['url'] = ''
            pageidInfo[pageid]['author'] = ''
            pageidInfo[pageid]['host'] = ''
            pageidInfo[pageid]['title'] = ''
            pageidInfo[pageid]['meta_description'] = ''

            outlist.append(pageid)
            url = ckattr(doc['_source'], 'url')
            author = ckattr(doc['_source'], 'author')
            host = ckattr(doc['_source'], 'hostname')
            title = ckattr(doc['_source'], 'title')
            meta = ckattr(doc['_source'], 'meta_description')
            if author != '':
                m = re.match(r'^<.+?>\s+?(\w+.*?\w+)\s+?<.+?>$', author)
                author = m.group(1)
                # print(author)
            pageidInfo[pageid]['url'] = url
            pageidInfo[pageid]['author'] = author
            pageidInfo[pageid]['host'] = host
            pageidInfo[pageid]['title'] = title
            pageidInfo[pageid]['meta'] = meta
            # wordPageidInfoList.append(pageidInfo)
            count += 1
        wordPageidInfo[word] = pageidInfo
        wordDic[word] = outlist
        # print(wordPageidInfo)
        print(word + ': ' + str(count))
        logger.info(word + ': ' + str(count))
        workeroutlist.append(wordDic)
        workerwordPageidInfoList.append(wordPageidInfo)
    return workeroutlist, workerwordPageidInfoList


mapping = {
    "M1": "male_15-24",
    "M2": "male_25-34",
    "M3": "male_35-44",
    "M4": "male_45-54",
    "M5": "male_55-64",
    "M6": "male_65+",
    "F1": "female_15-24",
    "F2": "female_25-34",
    "F3": "female_35-44",
    "F4": "female_45-54",
    "F5": "female_55-64",
    "F6": "female_65+"
}
# use pageid to query footprint
maleLabel = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
femaleLabel = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6']


# for i in ['NA']+maleLabel+femaleLabel:
#     print(i)


def worker_two(stepnum):
    global todopageidList
    global proc
    if len(todopageidList) % proc > proc // 2:
        step = len(todopageidList) // proc + 1
    else:
        step = len(todopageidList) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = todopageidList[s:]
    else:
        todo_worklist = todopageidList[s:s + step]

    esCluster = Elasticsearch(
        # hosts = ['192.168.21.20'],
        hosts=['192.168.21.18'],
        http_auth=('elastic', 'breaktime168'),
        port=9200,
        timeout=36000
    )

    pageidcount = 0
    logger.info('len of todo_worklist: ' + str(len(todo_worklist)))
    pageidDict = {}
    for eachpageid in todo_worklist:

        # prepare returned dict
        if eachpageid not in pageidDict:
            pageidDict[eachpageid] = {}
            pageidDict[eachpageid]['pv'] = 0
            pageidDict[eachpageid]['fp'] = []
            tmp = {}
            for i in ['NA'] + maleLabel + femaleLabel:
                tmp[i] = 0
            pageidDict[eachpageid]['gender_age'] = tmp
            # pageidDict[eachpageid]['age'] = {}
            # pageidDict[eachpageid]['gender'] = {}
            # to be continue
            # for i in allinterestedKeys:
            #     pageidDict[eachpageid][i] = {}

        # first: query for index footprint_2018*
        body = {}
        body['_source'] = ["fp", "userage", "usergender", "path"]
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must'].append(
            {"term": {"pageid": eachpageid}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # logger.info(body)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        count = 0
        fpset = set()

        for doc in res:
            pageidDict[eachpageid]['pv'] += 1
            fp = ckattr(doc['_source'], 'fp')
            gender = ckattr(doc['_source'], 'usergender')
            age = ckattr(doc['_source'], 'userage')
            label = getGAlabel(gender, age)
            pageidDict[eachpageid]['gender_age'][label] += 1
            fpset.add(fp)
            # pageidDict[eachpageid]['fp'] = len(fpset)
            count += 1
            # if not (count % 10000):
            #     logger.info('num of processed doc: ' + str(count))
        # logger.info('num of processed doc: ' + str(count))
        pageidDict[eachpageid]['fp'] = list(fpset)
        pageidcount += 1
        # if not (pageidcount % 100):
        #     logger.info('num of processed pageid: ' + str(pageidcount))
    logger.info('num of processed pageid: ' + str(pageidcount))
    logger.info('worker done!')
    return pageidDict


if __name__ == '__main__':
    if numprocess > len(wordlist):
        proc = len(wordlist)
    else:
        proc = numprocess

    # print('proc: ' + str(proc))
    with mp.Pool() as pool:
        res = pool.map(worker_one, range(proc))

    word2pageidList = []
    word2pageidInfoList = []
    for a, b in res:
        word2pageidList.append(a)
        word2pageidInfoList.append(b)
    # sys.exit()
    # print(word2pageidList)
    # print(str(len(word2pageidInfoList)))

    word2pageidDic = {}
    s = set()
    s = {x for ilist in word2pageidList for dic in ilist for iilist in dic.values()
         for x in iilist}
    logger.info('total unique pageid: ' + str(len(s)))

    pageidRef = {}  # generate a dict: key=pageid, val = pageid info
    dedup = set()
    for eachdic in [eachdic for ilist in word2pageidInfoList for eachdic in ilist]:
        for valdic in eachdic.values():
            for k in valdic:
                if k not in dedup:
                    dedup.add(k)
                    pageidRef[k] = valdic[k]
                    # print(k)
    # print(pageidRef.keys())
    del word2pageidInfoList
    # sys.exit()

    print('total unique pageid: ' + str(len(s)))
    for a, b in [(pageid, pageidlist) for ilist in word2pageidList for dic in ilist for pageid, pageidlist in dic.items()]:
        word2pageidDic[a] = b
    # print(word2pageidDic.values())
    # sys.exit()
    # todopageidList = list(s)
    word2footDic = {}
    for word, plist in word2pageidDic.items():
        logger.info('join foot info to word: ' + word)
        todopageidList = plist
        if len(todopageidList) == 0:
            word2footDic[word] = 0  # not sure yet
            continue
        if numprocess > len(todopageidList):
            proc = len(todopageidList)
        else:
            proc = numprocess
        with mp.Pool() as pool:
            res = pool.map(worker_two, range(proc))
        z = {}
        for dic in res:
            z.update(dic)
            # z = {**z. **dic}
        # print(z)
        # print(str(len(z)))
        word2footDic[word] = z
    # print(word2footDic.keys())

    # outfile1
    outfile = 'data/' + outputlabel + '_pageid.csv'
    # outfile = 'data/tmp.csv'
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['word', 'pageid', 'url', 'host', 'author',
                         'title', 'meta', 'pv', 'fp', 'NA'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel])
        for word in word2footDic:
            if word2footDic[word] == 0:
                continue
            tmp = {}
            for pageid in word2footDic[word]:
                tmp[pageid] = word2footDic[word][pageid]['pv']
            sortedk = sorted(tmp, key=tmp.get, reverse=True)
            for pageid in sortedk:
                eachline = []
                eachline.append(word)
                eachline.append(pageid)
                url = pageidRef[pageid]['url']
                host = pageidRef[pageid]['host']
                author = pageidRef[pageid]['author']
                title = pageidRef[pageid]['title']
                meta = pageidRef[pageid]['meta']
                for x in [url, host, author, title, meta]:
                    eachline.append(x)
                eachline.append(word2footDic[word][pageid]['pv'])
                eachline.append(str(len(word2footDic[word][pageid]['fp'])))
                for label in ['NA'] + maleLabel + femaleLabel:
                    eachline.append(
                        str(word2footDic[word][pageid]['gender_age'][label]))
                writer.writerow(eachline)

    sys.exit()
    # prepare overall dic
    overallDic = {}
    for word, eachdic in word2footDic.items():
        for pageid, subdic in eachdic.items():
            pass
            # outfile2
    outfile = 'data/' + outputlabel + '_overall.csv'
    # outfile = 'data/tmp.csv'
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['pv', 'fp', 'NA'] + maleLabel + femaleLabel)
        for word in word2footDic:
            pass
            # outfile3 per host
    logger.info('Done!')
