'''
import hashlib # decided to use pagid instead
v1, query webpages related to eye disease
v2, since there are 21% of webpages loss in index webpages, so keywords query should also be done on footprints_2018*. what a pain in the ass
use update instead of union!
elk returned title does not contain the queried keyword, double check with python
v3, add checkagain function, make sure that the title contains the keyword
support top50 output for *_perpage and *_wordperpage
v3.1, remove operator support "aa bb" concat with OR, "(aa cc)" concat with AND.
v3_1, forked from eye_disease_v3.py, query from sql instead of from elasticsearch footprint, should be faster since each webpage is unqiue, unlike footprint having so much redundant information

'''
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
import multiprocessing as mp
# import json
import re
# import pandas as pd
# import numpy as np
import pymysql.cursors
# import logging
sys.path.append('../tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

params = ['qfile', 'proc', 'S', 'E', 'out']
argvs = sys.argv
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass
    # print(e)

startDateString = paramDic['S']
endDateString = paramDic['E']
if 'proc' in paramDic:
    numprocess = int(paramDic['proc'])

outputlabel = paramDic['out']

# ========== hyper param ==========
cols = ['url']
colstr = ','.join(cols)
tb = 'ziExport'
data_dir = '/home/lance/data/'
# ========== ========== ==========


logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

wordlist = []
with open(paramDic['qfile'], 'r') as f:
    wordlist = [line.strip('\n') for line in f]

# # this will cause "nohup cmd &" to fail!
# print(wordlist)
# check = input('Make sure there is no special character in your qfile[Y]: ')
# try:
#     if check in ['', 'y', 'Y', 'yes', 'Yes']:
#         print('Proceed!')
#     else:
#         raise Exception('Quit and check again!!')
# except Exception as e:
#     print(e)

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    return po.get(attr, '')
    # if attr in po.keys():
    #     # print(po[attr])
    #     return po[attr]
    # else:
    #     if attr == 'stay':
    #         return 0
    #     else:
    #         return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))


def getGAlabel(gender, age):
    label = 'NA'
    if gender == 'M':
        if age >= 15 and age <= 24:
            label = 'M1'
        if age >= 25 and age <= 34:
            label = 'M2'
        if age >= 35 and age <= 44:
            label = 'M3'
        if age >= 45 and age <= 54:
            label = 'M4'
        if age >= 55 and age <= 64:
            label = 'M5'
        if age >= 65:
            label = 'M6'
    if gender == 'F':
        if age >= 15 and age <= 24:
            label = 'F1'
        if age >= 25 and age <= 34:
            label = 'F2'
        if age >= 35 and age <= 44:
            label = 'F3'
        if age >= 45 and age <= 54:
            label = 'F4'
        if age >= 55 and age <= 64:
            label = 'F5'
        if age >= 65:
            label = 'F6'
    return label


'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
# step = delta // numprocess
worker_start = start
worker_end = end

# sys.exit()

esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker_one(stepnum):
    # split workers with wordlist, not by time range
    global proc
    if len(wordlist) % proc > proc // 2:
        step = len(wordlist) // proc + 1
    else:
        step = len(wordlist) // proc
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        todo_worklist = wordlist[s:]
    else:
        todo_worklist = wordlist[s:s + step]

    workeroutlist = []
    logger.info(todo_worklist)
    word2url_dict = {}
    for word in todo_worklist:
        word2url_dict.setdefault(word, set())  # key = word, val = {set}
        # first check if the word were wrapped with ()
        tmplist = []
        mObj = re.match(r'^\((.+)\)$', word)
        if mObj:
            tmplist = [
                f'(title LIKE \'%{i}%\' OR content LIKE \'%{i}%\')' for i in mObj.group(1).split()]
            itemstr = ' AND '.join(tmplist)
        else:
            tmplist = [
                f'(title LIKE \'%{i}%\' OR content LIKE \'%{i}%\')' for i in word.split()]
            itemstr = ' OR '.join(tmplist)

        # print(itemstr)
        whereclause = f'WHERE {itemstr}'
        # print(whereclause)
        sql = f'SELECT {colstr} FROM {tb} {whereclause}'
        logger.info(sql)

        try:
            # !!! might generate a connect for each worker
            con = pymysql.connect(host='localhost',
                                  user='lance',
                                  password='',
                                  db='breaktime',
                                  charset='utf8mb4',
                                  cursorclass=pymysql.cursors.DictCursor)
            with con.cursor() as cursor:
                try:
                    cursor.execute(sql)
                except Exception as e:
                    logger.error(f'debug {e!s}')

                count = 0
                try:
                    while True:
                        rows = cursor.fetchmany(10000)
                        if not rows:
                            break
                        for row in rows:
                            url = row['url']
                            word2url_dict[word].add(url)  # add into set
                            count += 1
                            if (count % 10000 == 0):
                                logger.info(
                                    f'number of doc processed: {count!s}')
                        # if (count >= 100):
                        #     break
                except Exception as e:
                    logger.error(e)
                finally:
                    logger.info(f'number of doc processed: {count!s}')
                    logger.info(
                        f'number of unique url for {word}: {len(word2url_dict[word])!s}')
        except Exception as e:
            logger.error(e)
        finally:
            con.close()

    return word2url_dict


mapping = {
    "M1": "male_15-24",
    "M2": "male_25-34",
    "M3": "male_35-44",
    "M4": "male_45-54",
    "M5": "male_55-64",
    "M6": "male_65+",
    "F1": "female_15-24",
    "F2": "female_25-34",
    "F3": "female_35-44",
    "F4": "female_45-54",
    "F5": "female_55-64",
    "F6": "female_65+"
}
# use pageid to query footprint
maleLabel = ['M1', 'M2', 'M3', 'M4', 'M5', 'M6']
femaleLabel = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6']


# for i in ['NA']+maleLabel+femaleLabel:
#     print(i)

# get footprint Info for each unique pageid
def worker_two(stepnum):
    # global q_list
    # global proc
    if len(q_list) % proc > proc // 2:
        step = len(q_list) // proc + 1
    else:
        step = len(q_list) // proc

    if step < 1:  # should no happen
        step = 1
    s = 0 + step * stepnum
    if stepnum == proc - 1:
        # print('lastone')
        todo_worklist = q_list[s:]
    else:
        todo_worklist = q_list[s:s + step]

    count = 0
    logger.info('len of todo_worklist: ' + str(len(todo_worklist)))
    retDict = {}
    for eachitem in todo_worklist:
        # print(eachitem)
        # continue
        # prepare returned dict
        if eachitem not in retDict:
            # retDict.setdefault(eachitem, {})
            retDict[eachitem] = {}
            retDict[eachitem]['pv'] = 0
            retDict[eachitem]['fp'] = set()
            retDict[eachitem]['url'] = ''
            retDict[eachitem]['title'] = ''
            retDict[eachitem]['meta'] = ''
            retDict[eachitem]['author'] = ''
            retDict[eachitem]['host'] = ''
            retDict[eachitem]['path'] = ''
            tmp = {}
            for i in ['NA', 'M', 'F']:
                tmp[i] = 0
            retDict[eachitem]['gender'] = tmp
            tmp = {}
            for i in maleLabel + femaleLabel:
                tmp[i] = 0
            retDict[eachitem]['gender_age'] = tmp

        # first: query for index footprint_2018*
        body = {}
        body['_source'] = ["url", "hostname", "title", "meta_description",
                           "fp", "userage", "usergender", "path"]
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['must'].append(
            {"term": {"url": eachitem}})
        body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(
            worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # logger.info(body)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # count = 0
        fpset = set()

        for doc in res:
            if retDict[eachitem]['url'] == '':
                retDict[eachitem]['url'] = ckattr(doc['_source'], 'url')
                retDict[eachitem]['host'] = ckattr(
                    doc['_source'], 'hostname')
                retDict[eachitem]['title'] = ckattr(
                    doc['_source'], 'title')
                retDict[eachitem]['meta'] = ckattr(
                    doc['_source'], 'meta_description')
                retDict[eachitem]['path'] = ckattr(doc['_source'], 'path')
                # retDict[eachitem]['path'] = ckattr(doc['_source'], 'path')
                # author = ckattr(doc['_source'], 'author')
                if retDict[eachitem]['path'] != '':
                    m = re.match(r'^/(@\w+?)/.*$',
                                 retDict[eachitem]['path'])
                    if m:
                        author = m.group(1)
                    else:
                        author = ''
                retDict[eachitem]['author'] = author
            retDict[eachitem]['pv'] += 1
            fp = ckattr(doc['_source'], 'fp')
            gender = ckattr(doc['_source'], 'usergender')
            if gender in ['M', 'F']:
                retDict[eachitem]['gender'][gender] += 1
            else:
                retDict[eachitem]['gender']['NA'] += 1
            age = ckattr(doc['_source'], 'userage')
            label = getGAlabel(gender, age)
            if label != 'NA':
                retDict[eachitem]['gender_age'][label] += 1
            fpset.add(fp)
            # count += 1
        retDict[eachitem]['fp'] = fpset
        count += 1
    logger.info(f'num of processed item: {count!s}')
    logger.info('worker done!')
    return retDict


if __name__ == '__main__':
    proc = len(wordlist) if numprocess > len(wordlist) else numprocess

    with mp.Pool() as pool:
        res = pool.map(worker_one, range(proc))

    # res: list of dicts
    word2url_dict = {}
    urls = set()
    for idict in res:
        word2url_dict.update(idict)
        urls = urls.union({i for vlist in idict.values() for i in vlist})

    logger.info(f'num of total unique url: {len(urls)!s}')

    # word2pageidDic = {}
    # s = set()
    # s = {x for adic in word2pageidList for ilist in adic.values()
    #      for x in ilist}
    # logger.info('total unique pageid: ' + str(len(s)))
    # todopageidList = list(s)
    # del res
    q_list = list(urls)
    proc = len(q_list) if numprocess > len(
        q_list) else numprocess
    with mp.Pool() as pool:
        res = pool.map(worker_two, range(proc))

    z = {}
    for adic in res:
        z.update(adic)

    # print(z)
    # print(str(len(z.keys())))
    del res
    # sys.exit()
    # output 1: perpage
    outfile = data_dir + outputlabel + '_perpage.csv'
    outfiletop50 = data_dir + outputlabel + '_perpage_top50.csv'
    header = ['url', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f, open(outfiletop50, 'w') as ft:
        writer = csv.writer(f)
        writer.writerow(header)
        writer_ft = csv.writer(ft)
        writer_ft.writerow(header)
        tmp = {}
        for pageid in z:
            tmp[pageid] = z[pageid]['pv']
        sortedk = sorted(tmp, key=tmp.get, reverse=True)
        count = 0
        for pageid in sortedk:
            if z[pageid]['pv'] == 0:
                continue
            count += 1
            outline = []
            outline.append(pageid)
            for i in ['host', 'title', 'meta', 'author', 'pv']:
                outline.append(z[pageid][i])
            outline.append(str(len(z[pageid]['fp'])))
            for i in ['NA', 'M', 'F']:
                outline.append(z[pageid]['gender'][i])
            for i in maleLabel + femaleLabel:
                outline.append(z[pageid]['gender_age'][i])
            writer.writerow(outline)
            if count <= 50:
                writer_ft.writerow(outline)

    # output 2: per word per page
    outfile = data_dir + outputlabel + '_wordperpage.csv'
    outfiletop50 = data_dir + outputlabel + '_wordperpage_top50.csv'
    header = ['word', 'pageid', 'host', 'title', 'meta', 'author',
              'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x] for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f, open(outfiletop50, 'w') as ft:
        writer = csv.writer(f)
        writer.writerow(header)
        writer_ft = csv.writer(ft)
        writer_ft.writerow(header)
        # for adic in word2pageidList:
        for word, pageids in word2url_dict.items():
            tmp = {}
            for pageid in pageids:
                tmp[pageid] = z[pageid]['pv']
            sortedk = sorted(tmp, key=tmp.get, reverse=True)
            count = 0
            for pageid in sortedk:
                if pageid == '' or z[pageid]['pv'] == 0:
                    continue
                count += 1
                outline = []
                outline.append(word)
                outline.append(pageid)
                for i in ['host', 'title', 'meta', 'author', 'pv']:
                    outline.append(z[pageid][i])
                outline.append(str(len(z[pageid]['fp'])))
                for i in ['NA', 'M', 'F']:
                    outline.append(z[pageid]['gender'][i])
                for i in maleLabel + femaleLabel:
                    outline.append(z[pageid]['gender_age'][i])
                writer.writerow(outline)
                if count <= 50:
                    writer_ft.writerow(outline)

    # output 3: per word
    outfile = data_dir + outputlabel + '_perword.csv'
    header = ['word', 'pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                                     for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(header)
        # for adic in word2pageidList:
        for word, pageids in word2url_dict.items():
            outline = []
            outline.append(word)
            pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
            fpset = set()
            for pageid in pageids:
                pv += z[pageid]['pv']
                fpset.update(z[pageid]['fp'])
                na += z[pageid]['gender']['NA']
                m += z[pageid]['gender']['M']
                f += z[pageid]['gender']['F']
                m1 += z[pageid]['gender_age']['M1']
                m2 += z[pageid]['gender_age']['M2']
                m3 += z[pageid]['gender_age']['M3']
                m4 += z[pageid]['gender_age']['M4']
                m5 += z[pageid]['gender_age']['M5']
                m6 += z[pageid]['gender_age']['M6']
                f1 += z[pageid]['gender_age']['F1']
                f2 += z[pageid]['gender_age']['F2']
                f3 += z[pageid]['gender_age']['F3']
                f4 += z[pageid]['gender_age']['F4']
                f5 += z[pageid]['gender_age']['F5']
                f6 += z[pageid]['gender_age']['F6']
            fp = str(len(fpset))
            for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
                outline.append(i)
            writer.writerow(outline)

    # output 4: summary
    outfile = data_dir + outputlabel + '_sum.csv'
    header = ['pv', 'fp', 'NA', 'M', 'F'] + [mapping[x]
                                             for x in maleLabel] + [mapping[x] for x in femaleLabel]
    with open(outfile, 'w') as file:
        writer = csv.writer(file)
        writer.writerow(header)

        # for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
        #     i = 0
        pv = fp = na = m = f = m1 = m2 = m3 = m4 = m5 = m6 = f1 = f2 = f3 = f4 = f5 = f6 = 0
        fpset = set()
        for pageid in z:
            pv += z[pageid]['pv']
            fpset.update(z[pageid]['fp'])
            na += z[pageid]['gender']['NA']
            m += z[pageid]['gender']['M']
            f += z[pageid]['gender']['F']
            m1 += z[pageid]['gender_age']['M1']
            m2 += z[pageid]['gender_age']['M2']
            m3 += z[pageid]['gender_age']['M3']
            m4 += z[pageid]['gender_age']['M4']
            m5 += z[pageid]['gender_age']['M5']
            m6 += z[pageid]['gender_age']['M6']
            f1 += z[pageid]['gender_age']['F1']
            f2 += z[pageid]['gender_age']['F2']
            f3 += z[pageid]['gender_age']['F3']
            f4 += z[pageid]['gender_age']['F4']
            f5 += z[pageid]['gender_age']['F5']
            f6 += z[pageid]['gender_age']['F6']
        outline = []
        fp = str(len(fpset))
        for i in [pv, fp, na, m, f, m1, m2, m3, m4, m5, m6, f1, f2, f3, f4, f5, f6]:
            outline.append(i)
        writer.writerow(outline)

    logger.info('Done!')
