from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
import multiprocessing as mp
# import json
import re
# import pandas as pd
# import numpy as np
import pymysql.cursors
# import logging
sys.path.append('../tools/')
from common.config import logger, dirmaster, parseCMD
filepath = os.path.abspath(__file__)
logger = logger(filepath)

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

# ========== hyper param ==========
mustparams = ['qfile', 'proc', 'S', 'E', 'out']
optionalparams = ['bhost']
argvs = sys.argv
data_dir = dirmaster.data

paramDic = parseCMD(argvs, mustparams, optionalparams)

print(paramDic)
