import sys
import json
import os
import re
# import gzip
import zipfile
import csv

# check out column explaination
# https://tw.search.ec.yahoo.com/api/static/swagger-ui/#%21/Items/search_items_get

cmdstr = ' '.join(sys.argv)
print(cmdstr)

params = []
argvs = sys.argv
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit()
except Exception as e:
    pass

targetpath = {}
for x in ['buy', 'mall', 'bid']:
    if x == 'bid':
        targetpath[x] = 'yahooEC/' + x + '/full/2018050806/'
    else:
        targetpath[x] = 'yahooEC/' + x + '/full/2018050803/'

# local_dir = 'yahooEC/buy/full/2018050803/'
type2file = {}
for x, y in targetpath.items():
    # print(y)
    local_dir = y
    flist = []
    for f in os.listdir(local_dir):
        if re.match(r'\S+\.gzip$', f):
            flist.append(f)
            # print(f)
    flist.sort()
    type2file[x] = flist

# print(type2file)
# sys.exit()
# print(flist)
# for f in flist[:1]:
outputdir = 'csv/'
if not os.path.exists(outputdir):
    os.makedirs(outputdir)
for qtype, flist in type2file.items():
    print('start processing type: ' + qtype)
    local_dir = targetpath[qtype]
    # print(qtype, type(flist))
    qlist = [flist[0], flist[-1]]
    # print(qlist)
    # break

    for f in qlist:
        filesplits = f.split('.')
        fileprefix = filesplits[0]
        csvoutfile = outputdir + fileprefix + '.csv'
        # break
        check_cat = 0
        if re.match(r'^category', f):
            check_cat = 1
            csvoutfile = outputdir + qtype + '_' + fileprefix + '.csv'
        filename = local_dir + f
        # print(filename)
        # sys.exit()
        tmpfL = []
        with zipfile.ZipFile(filename, 'r') as fin:
            # print(fin.infolist())
            # print(fin.namelist())
            finL = fin.namelist()
            tmp_dir = '/tmp/'
            fin.extractall(tmp_dir)
            tmpfL = [tmp_dir + i for i in finL]
            # should be only one item in this case
        # print(tmpfL[0])

        with open(tmpfL[0], encoding='utf8') as jf:
            json_data = json.load(jf)
            # buy_000000.json(check_cat = 0) & category.json(check_cat = 1) have different structure
            if check_cat == 0:
                # list of dict
                with open(csvoutfile, 'w') as f:
                    writer = csv.writer(f)
                    obv_header = [x for x in json_data[0].keys()]
                    print(obv_header)
                    header = []
                    if qtype == 'bid':
                        header = ['status', 'description', 'title', 'fastShipping', 'price', 'additionalImageUrls', 'payType',
                                  'seller', 'shipFee', 'createDate', 'bidPrice', 'url', 'categoryPath', 'property', 'imageUrl']
                    else:
                        header = ['status', 'promo', 'description', 'title', 'url', 'price', 'payType', 'seller',
                                  'shipFee', 'createDate', 'categoryPath', 'property', 'fastShipping', 'imageUrl']
                    # print(header)
                    writer.writerow(header)
                    errcount = 0
                    countdict = {}
                    for i in range(len(json_data)):
                        if str(len(json_data[i].keys())) in countdict:
                            countdict[str(len(json_data[i].keys()))] += 1
                        else:
                            countdict[str(len(json_data[i].keys()))] = 1
                        outline = []
                        err = 0
                        for k in header:
                            try:
                                val = json_data[i][k]
                                if type(val) is dict:
                                    val = json.dumps(val, ensure_ascii=False)
                                outline.append(val)
                                # print(k, val)
                            except KeyError as e:
                                err = 1
                                errcount += 1
                                val = ''  # note: '' is stored instead of empty dict
                                outline.append(val)
                                # print(k, val)
                        if err == 1:
                            pass
                            # print(json_data[i])
                        writer.writerow(outline)
                    print(countdict)
                    print('errcount: ' + str(errcount))
            elif check_cat == 1:
                # big dict
                # print(json_data)
                with open(csvoutfile, 'w') as f:
                    writer = csv.writer(f)
                    header = ['cat_level', 'cat_id', 'name',
                              'parent_cat_id', 'children_cat_id']
                    writer.writerow(header)
                    for k, v in json_data.items():
                        # for a, b in v['1'].items():
                        #     print(b.keys())
                        #     print(a, b)
                        # break
                        # first layer's key is response_dats
                        # discover cat_level 6 at most for buy, start from 1
                        # but only cat_level 4 has data
                        klist = [k for k in v]
                        klist.sort()
                        # print(klist)
                        # break
                        for i in klist:
                            # print(v[i])
                            catidlist = []
                            catidlist = [x for x in v[i].keys()]
                            catidlist.sort()
                            # print(catidlist)
                            # break
                            # for i in v[i]:
                            #     tmp[i] = word2footDic[word][pageid]['pv']
                            # sortedk = sorted(tmp, key=tmp.get, reverse=True)
                            for catid in catidlist:
                                outline = []
                                for h in header:
                                    outline.append(v[i][catid][h])
                                writer.writerow(outline)

        # print(dcObj)
        # data = json.loads(fin.read().decode('utf-8'))
        # json_data = json.load(dcObj)
        # print(data)
        # with gzip.open(filename, 'r') as jf:
        #     print(jf.__dir__)
        #     content = jf.read()
        #     print(content)
        # jf_data = json.load(jf)
        # for k, v in json_data[0].items():
        # print(k, v)


sys.exit()

# with open(paramDic['in'], encoding='utf8') as json_file:
#     json_data = json.load(json_file)
#     # print(json_data)
#     # print(str(len(json_data)))
#     # print(json_data[0])
#     for i in range(2):
#         print(i)
#         for k, v in json_data[i].items():
#             print(k, v)

# there are 10000 items in each

# status 1
# promo {'title': '5/14-5/20開架大賞 專櫃保養/彩妝、香水香氛 滿1200現折120', 'url': 'https://buy.yahoo.com.tw/?sub=8', 'imageUrl': '', 'startTime': 1526263200, 'shortDesc': '', 'endTime': 1526867999, 'price': 850}
# description 結帳享9折優惠　　　抗油、抗汗、抗淚液，持久不暈染　　　長時間維持鮮明清晰線條　　　超柔軟筆刷，觸感舒適易操控
# title Kanebo佳麗寶 COFFRET D’OR深黑持久眼線液WP
# url https://tw.buy.yahoo.com/gdsale/Kanebo佳麗寶-COFFRET-D-OR深黑持-5753454.html
# price 850.0
# payType 1,2,4,5,6
# seller {'url': 'https://tw.buy.yahoo.com', 'title': '購物中心'}
# shipFee
# createDate 1425446280
# categoryPath 6,8,879,95766
# property buy
# fastShipping 0
# imageUrl https://s.yimg.com/wb/images/EC25F9C65C0C5A39E710A5FAA0C208C82A470448
# for i in json_data
