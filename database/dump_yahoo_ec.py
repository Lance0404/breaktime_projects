'''
purpose download json file from yahoo ec
ref: https://pythonprogramming.net/ftp-transfers-python-ftplib/
run every week
'''
from ftplib import FTP_TLS
import os
import sys
sys.path.append('/home/lance/Git/breaktime_projects/tools/')
from common.logger import logger, dirmaster
filepath = os.path.abspath(__file__)
logger = logger(filepath)

logger.info('Start!')

if not os.path.exists(dirmaster.data):
    os.makedirs(dirmaster.data)
# print(dirmaster.data)
# sys.exit()

with FTP_TLS(host='asia-dropbox.yahoo.com') as ftps:

    def grabFile(filename):
        # filename = 'example.txt'
        localfile = open(dirmaster.data + 'yahooEC' + filename, 'wb')
        ftps.retrbinary('RETR ' + filename, localfile.write, 1024)
        # ftps.quit()
        localfile.close()

    ftps.auth()
    ftps.prot_p()
    ftps.login('ecsearch', 'qJ42lx3c')
    # ftps.retrlines('LIST')

    target_dir = []
    q_dir = ['/buy/full/', '/bid/full/', '/mall/full/']
    for d in q_dir:
        flist = ftps.nlst(d)
        # logger.info(flist)
        for fd in flist:
            fdlist = ftps.nlst(fd)
            if len(fdlist) > 0:
                target_dir.append(fd)
                logger.info(fd + ': ' + str(len(fdlist)))

    # sys.exit()
    for d in target_dir:
        remotedir = d
        localdir = dirmaster.data + 'yahooEC' + remotedir
        if not os.path.exists(localdir):
            os.makedirs(localdir)
            logger.info('mkdir ' + localdir)
            flist = ftps.nlst(remotedir)
            # logger.info(flist)
            for i in flist:
                grabFile(i)
        logger.info('download ' + d + ' finished!')

logger.info('Done!')
# ftps.cwd('/buy/full/2018051503')
# ftps.retrlines('LIST')
# ftps.cwd('full')
# ftps.retrlines('LIST')
