import urllib.request
import urllib.parse
import sys
import json

params = ['q']
paramDic = {}
for param in params:
    if '-' + param in sys.argv:
        idx = sys.argv.index('-' + param)
        paramDic[param] = sys.argv[idx + 1]

if 0:
    url = 'https://tw.search.ec.yahoo.com:443/api/affiliate/v1/recommendedItemsByItem/b102434078'
    values = {
        'property': 'bid',
        'model': 'vv'
    }
    data = urllib.parse.urlencode(values)
    # data = data.decode('ascii')  # data should be bytes
    print(data)
    # sys.exit()
    req = urllib.request.Request(url, data)
    with urllib.request.urlopen(req) as response:
        # print(type(response))
        the_page = response.read()
        print(the_page)


if True:
    # use GET method
    url = 'https://tw.search.ec.yahoo.com:443/api/affiliate/v1/search/items'
    # tmp = urllib.parse.urlencode('nike+-adidas')
    # print(tmp)
    if 'q' in paramDic:
        keyword_q = paramDic['q']
    else:
        print('-q not set')
        sys.exit()
    values = {'q': keyword_q,
              'property': 'bid',
              'sort': 'rel',
              'limit': 50,
              # 'offset': 50
              }
    data = urllib.parse.urlencode(values)
    fullurl = url + '?' + data
    print(fullurl)
    req = urllib.request.Request(fullurl)
    with urllib.request.urlopen(req) as response:
        the_page = response.read().decode()
        # print(type(the_page))
        obj = json.loads(the_page)
        # print(type(obj))
        # print(obj.keys())
        # print(str(len(obj['items'])))
        # print(obj['nextOffset'])
        print('total num: ' + str(obj['resultsTotal']))

# https://tw.search.ec.yahoo.com:443/api/affiliate/v1/search/items?q=nike%20-adidas&property=bid&sort=rel
