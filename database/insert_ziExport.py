'''
purpose: insert ziExport.json into mariadb
'''
import bz2
import json
import pymysql.cursors
import sys
import os
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

# file = 'ziExport.json.bz2'
# with bz2.open(file, 'rb') as bjf:
#     pass
#     print(bjf.__dict__)
#     json_data = json.load(bjf)
#     for _ in json_data:
#         print(_)
#         break

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

'''=================== self-defined functions start ==================='''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    # outstr = outlist.join(',')
    outstr = ','.join(outlist)
    # print(outstr)
    return outstr


'''=================== self-defined functions end ==================='''

# ============= hyper params =============
file = 'ziExport.json'
url_prefix = 'https://zi.media'

connection = pymysql.connect(
    host='localhost',
    user='lance',
    password='',
    db='breaktime',
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor
)

# ============= ============= =============

# with open(file, encoding='utf8') as jf:
#     json_data = json.load(jf)
#     print(type(json_data))

tablename = 'ziExport'
droptbstr = '''DROP TABLE IF EXISTS ''' + tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS ''' + tablename + '''
    (id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    url TEXT,
    title TEXT,
    content TEXT,
    PRIMARY KEY `id` (`id`)
    ) '''

insertcol = ['url', 'title', 'content']
insertcolstr = addacute(insertcol)
sqlparam = ','.join(['%s'] * len(insertcol))
sql = "INSERT INTO " + tablename + \
    " (" + insertcolstr + ") VALUES (" + sqlparam + ")"
logger.info(sql)


try:
    with connection.cursor() as cursor:
        cursor.execute(droptbstr)
        cursor.execute(createtbstr)
        with open(file, 'rb') as f:
            count = 0
            each10000doc = []
            try:
                for line in f:
                    json_data = json.loads(line)
                    #  5 keys for each ['_id', 'BloggerId', 'Title', 'Content', 'Id']
                    _id, bloggerid, title, content, Id = json_data['_id'], json_data[
                        'BloggerId'], json_data['Title'], json_data['Content'], json_data['Id']
                    url = f'{url_prefix}/@{bloggerid}/post/{Id}'
                    # catstr = ''.join(content.split('\n'))
                    each_doc = [url, title, content]
                    each10000doc.append(each_doc)
                    count += 1

                    if (count % 10000 == 0):
                        try:
                            cursor.executemany(sql, each10000doc)
                            # cursor.execute(sql, (each_doc))
                            connection.commit()
                            each10000doc = []  # init
                        except Exception as e:
                            logger.error(
                                'error while executemany(): ' + str(e))
                        logger.info('num of doc processed: ' + str(count))
                        continue
                    # if count == 10:
                    #     break
            finally:
                logger.info('length of last each10000doc: ' +
                            str(len(each10000doc)))
                try:
                    cursor.executemany(sql, each10000doc)
                    connection.commit()
                    each10000doc = []
                except Exception as err:
                    logger.error('error while executemany(): ' + str(err))
                logger.info('num of doc processed: ' + str(count))

except Exception as e:
    logger.error(e)

finally:
    connection.close()

logger.info('Done!')
