'''
stat the daily wifi pv among a time range
'''

#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
import logging
import multiprocessing as mp
# import pandas as pd

params = ['S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
# outfile = paramDic['out']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    # print(indate)
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:8]))
    # return datetime(int(indate[0:4]), int(indate[4:6]), int(indate[6:8]), hour=int(indate[8:10]), minute=int(indate[10:12]), second=int(indate[12:14]))
    # return date[0:4]+'-'+date[4:6]+'-'+date[6:]


'''=================== self-defined functions end ==================='''

wifilist = ['/wifi', '/adwifi', '/tymetroWiFI', '/mrt_adwifi']
start = toDateObj(startDateString)
end = toDateObj(endDateString)
q_date = []

while (start <= end):
    # print(start)
    q_date.append(str(start))
    start = start + timedelta(days=1)

# print(q_date)
# print(start)
# sys.exit()
proc = 8
stepsize = 1
# stepsize = len(
#     qrange) // proc if len(qrange) % proc < proc // 2 else len(qrange) // proc + 1  # 4
# print(stepsize)
# sys.exit()

# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000


)


def worker(stepnum):
    pass
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_date[0 + stepsize * stepnum:]
    else:
        todoworklist = q_date[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # returnlist = []
    retDict = {}
    for eachday in todoworklist:
        retDict[eachday] = {}
        for wifi in wifilist:
            body = {}
            # body['_source'] = ["path"]
            # body['size'] = 0
            body['query'] = {}
            body['query']['bool'] = {}
            body['query']['bool']['must'] = []
            body['query']['bool']['must_not'] = []
            body['query']['bool']['filter'] = []
            body['query']['bool']['filter'].append(
                {"term": {"hostname": "zi.media"}})
            body['query']['bool']['filter'].append({"range": {"updated": {
                                                   "gte": eachday, "lte": eachday, "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
            body['query']['bool']['must'].append(
                {"prefix": {"path": wifi}})
            logger.info(body)

            res = esCluster.search(
                index="footprints_2018*",
                doc_type="footprint",
                scroll="3m",
                search_type="query_then_fetch",
                size=0,
                body=body, request_timeout=300)
            # res = helpers.scan(
            #     client=esCluster,
            #     scroll='20m',
            #     size=10000,
            #     query=body,
            #     index='footprints_2018*',
            #     doc_type='footprint',
            #     request_timeout=5000
            # )
            total = res['hits']['total']
            retDict[eachday][wifi] = total
            # print(eachday, wifi, total)
            # sys.exit()

    return retDict


def multiproc():
    pass
    procc = proc if proc <= len(q_date) else len(q_date)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))

    # merge dicts
    joinDict = {}
    for eachdict in res:
        # print(eachdict)
        joinDict.update(eachdict)

    # print(joinDict)
    outfile = 'data/' + label + '.csv'
    with open(outfile, 'w') as csvf:
        writer = csv.writer(csvf)
        writer.writerow(['date'] + wifilist)
        for eachdate in q_date:
            outlist = []
            outlist.append(eachdate)
            for wifi in wifilist:
                outlist.append(joinDict[eachdate][wifi])
            writer.writerow(outlist)


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
