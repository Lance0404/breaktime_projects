
# coding: utf-8

# In[1]:


import pandas as pd


# In[47]:


df0312 = pd.read_csv('ads_dataloss_0312_0313.csv')
df0319 = pd.read_csv('ads_dataloss_0319_0320.csv')
df0326 = pd.read_csv('ads_dataloss_0326_0327.csv')
df0402 = pd.read_csv('ads_dataloss_0402_0403.csv')


# In[29]:


len(df0312)
df0312.tail()


# In[28]:


len(df0319)
df0319.tail()


# In[27]:


len(df0326)
df0326.tail()


# In[26]:


len(df0402)
df0402.tail()


# In[20]:


df0312.dtypes


# In[58]:



df0312.columns=['time', 'count', 'mavg']
df0319.columns=['time', 'count', 'mavg']
df0326.columns=['time', 'count', 'mavg']
df0402.columns=['time', 'count', 'mavg']


# In[59]:


df0319.dtypes


# In[60]:


# get row 1:215
df0312s = df0312.iloc[1:216, 1:]
df0319s = df0319.iloc[1:216, 1:]
df0326s = df0326.iloc[1:216, 1:]
df0402s = df0402.iloc[1:216, 1:]


# In[61]:


len(df0312s)
len(df0319s)
len(df0326s)
len(df0402s)
df0312s


# In[64]:


df0312s['mavg'] = df0312s['mavg'].astype(float)


# In[65]:


df0312s.dtypes

