#!/root/anaconda3/envs/my_env/bin/python
import pymysql.cursors
import sys
import logging
import os.path
# import pandas as pd
# import numpy as np
import csv
# import re

# v1, not yet importing footprint
# v2, optimization. use pageid to combine the pv count

q_list = []
argvs = sys.argv
if '-top' in argvs:
	indextop = argvs.index('-top')
	topnum = int(argvs[indextop+1])
else:
	topnum = 20

if '-title_lv1' in argvs:		
	idxtitelv1 = argvs.index('-title_lv1')
	titlelv1_q = argvs[idxtitelv1+1]
	q_list.append(titlelv1_q)
else:
	titlelv1_q = ''

if '-title_lv2' in argvs:		
	idxtitelv2 = argvs.index('-title_lv2')
	titlelv2_q = argvs[idxtitelv2+1]
	q_list.append(titlelv2_q)
else:
	titlelv2_q = ''


filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('Start!')

if len(q_list) > 0:
	qliststr = '_'.join(q_list)	
	outcsv = filename+'_'+qliststr+'.csv'
else:
	outcsv = filename+'.csv'



cols = ['nlpid', 'hostname', 'title_lv1', 'title_lv2', 'title_lv3', 'title_k', 'con_h1_k', 'con_h2_k', 'con_p_k', 'con_k']
colstr = ','.join(cols)
tb = 'nlp_0131_0401'

bigDict = {}
for idx, val in enumerate(cols):
	if idx > 1:
		# print(val)
		bigDict[val] = {} # {} nested under a big {}

def toDict(inrow):
	global bigDict
	footcount = 0
	try:
		con = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
		with con.cursor() as cursor:
			whereclause = " where pageid ='"+inrow['nlpid']+"'"
			sql = "select footcount from pageid2foot_201803" +whereclause
			# logger.info(sql)		
			
			try:
				cursor.execute(sql)
			except Exception as e:
				logger.error('debug: '+str(e))

			try:	
				row = cursor.fetchone()
				if row is not None:
					footcount = row['footcount']
				else:
					footcount = 0
				# while row is not None:
				# 	footcount = cursor.fetchone()
				# if row:
				# 	footcount = row[0]
				# logger.info(type(row))	
				# logger.info('footcount: '+str(footcount))
			except TypeError as e:
				# logger.error(e)
				# logger.info(type(row))
				footcount = 0
	# except:
	# 	pass
		# footcount = 0
		# logger.info(inrow['nlpid']+' does not exist in table pageid2foot_201803')
	# except Exception as err:        
	# 	logger.error(err)
	finally:
	    con.close()


	for key in ['title_lv1', 'title_lv2', 'title_lv3', 'title_k', 'con_h1_k', 'con_h2_k', 'con_p_k', 'con_k']:
		for i in inrow[key].split():
			if i in bigDict[key]:
				bigDict[key][i] += footcount
			else:
				bigDict[key][i] = footcount	
# sys.exit()
try:
	# mysql breaktime -h localhost -u root -p
	# Connect to the database
	conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	count = 0
# first extract data from webpages
	with conn.cursor() as cursor:
		wherelist = []
		wherelist.append("updated >= '20180301' + INTERVAL 8 HOUR")
		wherelist.append("updated <= '20180331' + INTERVAL 8 HOUR")
		if titlelv1_q != '':
			wherelist.append("title_lv1 = '"+titlelv1_q+"'")
		if titlelv2_q != '':
			wherelist.append("title_lv2 = '"+titlelv2_q+"'")			
		whereclause = " where "+' AND '.join(wherelist)
		sql = "select "+colstr+" from "+tb+whereclause
		# sql = "select "+colstr+" from "+tb+" limit 1000"
		logger.info(sql)		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			if not rows:
				break
			for row in rows:
				count+=1
				pass
				toDict(row)
			# count+= 10000	
			if (count % 10000 == 0):
				logger.info('number of doc processed: '+str(count))
			# if count > 20000:
			# 	break
				
except Exception as err:        
	logger.error('debug_2: '+str(err))
finally:
	logger.info('number of doc processed: '+str(count))
	conn.close()
    # pageidDict.clear()
# print(bigDict)
# sys.exit()
# top 10 keywords for each key
with open(outcsv, 'w') as csv_file:
	writer = csv.writer(csv_file)
	for key1, val1 in bigDict.items():
		sortedKeys = sorted(val1, key=val1.get, reverse=True)
		count = 0
		for key2 in sortedKeys:
			if count >= topnum: # present only top 20 or more
				break
			writer.writerow([key1, key2, val1[key2]])
			count+=1	
		# for key2, val2 in val1.items():
			

# sys.exit()

logger.info('Done!')
