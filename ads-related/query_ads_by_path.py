#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
import pandas as pd
# import numpy as np
# import pymysql.cursors
import logging
# import multiprocessing as mp

# purpose:
# calculate the top 10 keywords with the most ads exposed for queried path


# read in params 

params = ['esidx', 'estype', 'S', 'E', 'path', 'out']
# q_list = []
argvs = sys.argv
print(argvs)
paramDic = {}
for param in params:
	if '-'+param in argvs:
		idx = argvs.index('-'+param)
		paramDic[param] =  argvs[idx+1] # check the data type
	else:
		paramDic[param] = ''

esidx = paramDic['esidx']
estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
qpath = paramDic['path']
outfile = paramDic['out']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('cmdline: '+' '.join(sys.argv))
logger.info('Start!')
# sys.exit()




'''=================== self-defined functions start ==================='''

def ckattr(po, attr):
	if attr in po.keys():
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	outstr = ','.join(outlist)	
	return outstr

def toDateObj(indate):
	# from 20180301 to 2018-03-01
	return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))
	# return date[0:4]+'-'+date[4:6]+'-'+date[6:]

'''=================== self-defined functions end ==================='''

start = toDateObj(startDateString)
end = toDateObj(endDateString)
# delta = end - start
# numprocess = 4 # must < delta.days
# step = delta//numprocess


# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)


body = {}
body['_source'] = ["path", "ads_keyword", "ads"]
# body['size'] = 0
body['query'] = {}
body['query']['bool'] = {}
body['query']['bool']['must'] = []
body['query']['bool']['filter'] = []
# body['query']['bool']['must_not'] = []
# body['query']['bool']['should'] = []	
body['query']['bool']['must'].append({"match_phrase": {"path": {"query": qpath}}})
body['query']['bool']['filter'].append({"term": {"hostname": "zi.media"}})
body['query']['bool']['filter'].append({"range": { "updated": {"gte": str(start), "lte": str(end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})

logger.info(body)
# sys.exit()

res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = esidx,
				doc_type= estype,
				request_timeout = 5000
)


Dict = {}
try:
	count = 0
	for doc in res:
		# print(doc)
		# ckattr(doc['_source'], '')
		path = doc['_source']['path']
		kw = ckattr(doc['_source'], 'ads_keyword')
		adlist = ckattr(doc['_source'], 'ads')
		adcount = len(adlist)
		# eachlist = [path, kw, adcount]
		# print(eachlist)
		if kw in Dict:
			Dict[kw] += adcount
		else:
			Dict[kw] = 0	
		count +=1 
		# if count > 3: break
		if count % 1000 == 0: # not much in this case
			logger.info('number of doc processed: '+str(count))

except Exception as e:
	logger.error(e)

sortedKeys = sorted(Dict, key=Dict.get, reverse=True)
with open(outfile, 'w') as f:
	writer = csv.writer(f)
	count = 0
	for i in sortedKeys:
		writer.writerow([i, Dict[i]])
		if count < 10:
			print(i+','+str(Dict[i]))
		count+=1 

sys.exit()	





