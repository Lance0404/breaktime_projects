# copy and paste
nohup python download_ads_scanv2.py -S 20180219 -E 20180226 > tmp_20180219_20180226.log & 
nohup python download_ads_scanv2.py -S 20180226 -E 20180305 > tmp_20180226_20180305.log &
nohup python download_ads_scanv2.py -S 20180305 -E 20180312 > tmp_20180305_20180312.log & 
nohup python download_ads_scanv2.py -S 20180312 -E 20180320 > tmp_20180312_20180320.log &  

# small scale test
nohup python download_ads_scanv2.py -S 20180319 -E 20180320 & 

vi tmp.txt
k2CountAndSum_20180219_20180226.csv
k2CountAndSum_20180226_20180305.csv
k2CountAndSum_20180305_20180312.csv
k2CountAndSum_20180312_20180320.csv

# merge the result and calculate the avg and total count
# python cal_avg.py

Extract row with avg >= 2
# awk -F, '{if ($4 >= 2)print;}' key2AvgAndCount.csv > key2AvgAndCount_abo2.csv

vi tmp_log.txt
tmp_20180219_20180226.log
tmp_20180226_20180305.log
tmp_20180305_20180312.log
tmp_20180312_20180320.log

cat tmp_log.txt | awk '{print "tail "$0}' | sh | grep 'total number of doc processed' | awk -F': ' 'BEGIN{SUM=0}{SUM+=$2}END{print SUM}'
26555350

