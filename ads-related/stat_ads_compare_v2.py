import sys
import logging
import glob
import os
import re
import pandas as pd
import csv

# v1, calculate keyword btw set a and set b
# v2, get the intersect of all the *_sum.csv


filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)
logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


labels = ['range0407_08', 'range0414_15', 'range0421_22', 'range0519_20']

types = ['adscount', 'zerocount', 'kwcount', 'avg']
typemapping = {
    0: 'adscount',
    1: 'zerocount',
    2: 'kwcount',
    3: 'avg'
}
# kwset = set()
xdict = {}
ydict = {}
for label in labels:
    xdict[label] = set()
    ydict[label] = {}
    with open('data/' + label + '_sum.csv') as csvf:
        readcsv = csv.reader(csvf, delimiter=',')
        count = 0
        for row in readcsv:
            if row[0] == 'keyword':
                continue
            kw, adscount, zerocount, kwcount, avg = row
            ydict[label][kw] = (adscount, zerocount, kwcount, avg)
            count += 1
            xdict[label].add(row[0])
        print('total num of row: ' + str(count))

# print(ydict)

kwset = set()
count = 0
for label, vset in xdict.items():
    if count == 0:
        kwset = vset
    else:
        kwset = set.intersection(kwset, vset)
    count += 1

print('num of intersected keyword: ' + str(len(kwset)))

for n, itype in typemapping.items():
    outfile = 'info/stat_' + itype + '_diff.csv'
    with open(outfile, 'w') as csvf:
        writer = csv.writer(csvf)
        writer.writerow(['keyword']+labels)
        for kw in kwset:
            outlist = [kw]
            outlist = outlist + [ydict[label][kw][n] for label in labels]
            writer.writerow(outlist)

sys.exit()
