#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
# from elasticsearch_dsl import Search, Q
import datetime
import csv
import sys
# import multiprocessing as mp
sys.path.append('./')

# usage: 
# nohup python download_ads_scanv3.py -S 20180219 -E 20180226 > tmp_20180219_20180226.log & 
# nohup python download_ads_scanv3.py -S 20180226 -E 20180305 > tmp_20180226_20180305.log &
# nohup python download_ads_scanv3.py -S 20180305 -E 20180312 > tmp_20180305_20180312.log & 
# nohup python download_ads_scanv3.py -S 20180312 -E 20180320 > tmp_20180312_20180320.log & 
# v1, make use of script_fields, reduce the work load of python
# v2, dissect the jobs with date, output keyword, count, sum for later calculation
# v3, specific for path = '/adwifi'

# "keyword": {
#   "count": "123",
#   "sum": "8888"
# }

'''=================== self-defined functions start ==================='''

# optimize the script with function
def collectData(responseDict, outputDict, scroll_size):
  for i in range(scroll_size):
    sourceDict = responseDict['hits']['hits'][i]['_source']
    key = sourceDict['ads_keyword']     
    val = len(sourceDict['ads'])
    if key in outputDict:
      # if it's true, append
      outputDict[key].append(val)
    else: 
      # else define as empty list
      outputDict[key] = []

def calKey2Avg(inDic, outDic):
  for k in inDic.keys():
    # the number of elements in a list
    n = len(inDic[k])
    # sum up
    s = sum(inDic[k])
    if (n >= 1):
      avg = s/n
      if (avg >= 2):
        outDic[k] = avg

def log(msg):
  print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

'''=================== self-defined functions end ==================='''

'''=================== global variables start ==================='''
argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')

startDateString = argvs[indexStart+1]
# startYear  = int(startDateString[0:4])
# startMonth = int(startDateString[4:6])
# startDay   = int(startDateString[6:8])
# startDate  = date(startYear, startMonth, startDay)
# logging.info(startDate.isoformat())

endDateString = argvs[indexEnd+1]
# endYear  = int(endDateString[0:4])
# endMonth = int(endDateString[4:6])
# endDay   = int(endDateString[6:8])
# endDate  = date(endYear, endMonth, endDay)
# logging.info(endDate.isoformat())
filename = 'k2CountAndSum_'+str(startDateString)+'_'+str(endDateString)+'.csv'
# print(filename)

# sys.exit()
# print('startDate: '+str(startDate))
# print('endDate: '+str(endDate))
# print(type(endDate))
# print(str(tempDate))
# sys.exit()

# connection to ELK
esCluster = Elasticsearch(
    hosts = ['192.168.21.20'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)


# sys.exit()
# max_slice = 5 
'''=================== global variables end ==================='''


# initiate the variable 
body = {
          "_source" : ["ads_keyword"],
          "query": {
            "bool":{
              "must": [
                {
                  "match_all": {}
                },
                {
                  "match_phrase": {
                    "path": {
                      "query": "/adwifi"
                    }
                  }
                },
                {
                  "range": {
                    "updated": {
                      "gte": startDateString,
                      "lt": endDateString,
                      "time_zone": "+08:00",
                      "format": "yyyyMMdd"
                    }
                  }
                }
              ]            
            }
          },
          "script_fields" : {
            "ads_count" : {
              "script" : "params['_source']['ads'].length"
            }
          }          
}

# res = esCluster.search(index="ads", doc_type="ad", scroll="3m", search_type="query_then_fetch", size=scroll_size, body=body, request_timeout=300)
res = helpers.scan(
        client = esCluster,
        scroll = '20m',
        size = 10000,
        query = body,
        index = 'ads',
        doc_type= "ad",
        request_timeout = 5000
)
log('type returned from helpers.scan: '+str(type(res)))

# count = 0
# for doc in res:
#   count+=1
#   # print(type(doc))
#   # print(doc.keys())
#   key = doc['_source']['ads_keyword']
#   val = doc['fields']['ads_count'][0]
#   print(key+','+str(val))
#   if (count > 10):
#     sys.exit()

# sys.exit()

# print(len(res))
outDic = {}
count = 0
for doc in res:
  count += 1
  key = doc['_source']['ads_keyword']
  val = doc['fields']['ads_count'][0]
  # log('print key: '+key)
  if key in outDic:
    outDic[key]['count'] += 1
    outDic[key]['sum'] += val
  else: 
    outDic[key] = {}
    outDic[key]['count'] = 1
    outDic[key]['sum'] = 0 
  if (count % 10000 ==0):  
    log('number of doc processed: '+str(count))
    log('number of keyword: '+str(len(outDic)))
  # if (count > 1000): # test 
  #   break

log('total number of doc processed: '+str(count))
log('total number of keyword before calculation: '+str(len(outDic)))

# log("Got %d Hits:" % res['hits']['total'])
sortedKeys = sorted(outDic)
outputList = []
# generate interable as csv.writer's input
for k in sortedKeys:
  outputList.append((k, str(outDic[k]['sum']), str(outDic[k]['count'])))
with open(filename, "w") as csvfile:
  csvwriter = csv.writer(csvfile, delimiter=',')
  csvwriter.writerows(outputList)
    # file.write(k + ',' + outDic[k]['sum'] + ',' + outDic[k]['count'] + "\n")

log('write to file done.')
sys.exit()



# # calculate the avg
# key2avg = {}
# calKey2Avg(outDic, key2avg)

# sortedKeys = sorted(key2avg, key=key2avg.get, reverse=True)
# with open(filename, "w") as file:
#   for i in sortedKeys:
#     file.write(i + ",%.2f" % key2avg[i] + "\n")

# log('total number of keyword after calculation: '+str(len(key2avg)))
