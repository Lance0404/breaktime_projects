#!/root/anaconda3/envs/my_env/bin/python
import datetime
import csv
import sys

inputfile = 'tmp.txt'
# files = []
outputfile = 'key2AvgAndCount.csv'

# i=0
# while True:
# 	with open (inputfile, 'r') as f:
# 		print(f.readline(i+1))


# print(lines)
# print(type(lines))

filelist = []
file = open(inputfile, 'r')
# print(type(file))
for aline in file:
	# aline.strip()
	# print(aline.strip())
	filelist.append(aline.strip())

file.close()

outDic = {}

for afile in filelist:
	with open(afile, 'r') as fp:
		reader = csv.reader(fp, delimiter=',')
		for row in reader: 
			key = row[0]
			tmpsum = int(row[1])
			tmpcount = int(row[2])
			if key in outDic:
				outDic[key]['sum']+= tmpsum
				outDic[key]['count'] += tmpcount
			else:
				outDic[key] = {}
				outDic[key]['sum'] = tmpsum
				outDic[key]['count'] = tmpcount

avgDic = {}
for key in outDic.keys():
	outDic[key]['avg'] = ''
	if int(outDic[key]['sum']) > 0:
		outDic[key]['avg'] = (int(outDic[key]['sum']) / int(outDic[key]['count']))
		avgDic[key] = outDic[key]['avg']
	else:
		outDic[key]['avg'] = 0
		avgDic[key] = 0



outputList = []
sortedKeys = sorted(avgDic, key=avgDic.get, reverse=True)

for k in sortedKeys:
  outputList.append([k, str(outDic[k]['sum']), str(outDic[k]['count']), str(outDic[k]['avg'])])


with open(outputfile, "w") as csvfile:
  csvwriter = csv.writer(csvfile, delimiter=',')
  csvwriter.writerow(["keyword", "sum", "count", "avg"])  
  csvwriter.writerows(outputList)




