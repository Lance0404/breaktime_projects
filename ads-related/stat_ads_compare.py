import sys
import logging
import glob
import os
import re
import pandas as pd

# v1, calculate keyword btw set a and set b
params = ['a', 'b']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

a = paramDic['a']
b = paramDic['b']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)
logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


# print(a)
# afilelist = glob.glob(a + '*.csv')
afilelist = glob.glob('data/' + a + '*.csv')
# print(afilelist)
# print(str(len(afilelist)))

tags = []
for i in afilelist:
    mObj = re.match('data/' + a + '_(\S+?)\.csv', i)
    # print(mObj.group(1))
    tags.append(mObj.group(1))

tags.sort()
# print(tags)

# for i in tags[:1]:
for i in tags:
    afile = 'data/' + a + '_' + i + '.csv'
    bfile = 'data/' + b + '_' + i + '.csv'
    # print((afile, bfile))
    # break
    adf = pd.read_csv(afile)[['keyword', 'sum', 'zero', 'count', 'avg']]
    adf.columns = ['keyword', 'adscount', 'zerokwcount', 'kwcount', 'avg']
    # print(adf)
    # break
    bdf = pd.read_csv(bfile)[['keyword', 'sum', 'zero', 'count', 'avg']]
    bdf.columns = ['keyword', 'adscount', 'zerokwcount', 'kwcount', 'avg']

    aset = {x for x in adf['keyword']}
    bset = {x for x in bdf['keyword']}
    logger.info('num of element in aset: ' + str(len(aset)))
    logger.info('num of element in bset: ' + str(len(bset)))
    uset = set.intersection(aset, bset)
    logger.info('num of element intersect: ' + str(len(uset)))

    # union
    outfile = 'info/' + a + '_vs_' + b + '_outer_' + i + '.csv'
    mdf = adf.join(bdf.set_index('keyword'),
                   on='keyword', how='outer', lsuffix='_' + a, rsuffix='_' + b)
    mdf = mdf.reset_index(drop=True)
    mdf = mdf[['keyword', 'adscount_' + a, 'adscount_' + b,
               'kwcount_' + a, 'kwcount_' + b, 'avg_' + a, 'avg_' + b]]
    # print(mdf.head())
    mdf.to_csv(outfile)

    # intersect
    outfile = 'info/' + a + '_vs_' + b + '_inner_' + i + '.csv'
    mdf = adf.join(bdf.set_index('keyword'),
                   on='keyword', how='inner', lsuffix='_' + a, rsuffix='_' + b)
    mdf = mdf.reset_index(drop=True)
    mdf = mdf[['keyword', 'adscount_' + a, 'adscount_' + b,
               'kwcount_' + a, 'kwcount_' + b, 'avg_' + a, 'avg_' + b]]
    # print(mdf.head())
    mdf.to_csv(outfile)

    # print(adf)


# stop here
