#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
# from elasticsearch_dsl import Search, Q
import datetime
# import csv
import sys
import multiprocessing as mp
sys.path.append('./')

# v1, make use of script_fields, reduce the work load of python

'''=================== self-defined functions start ==================='''

# optimize the script with function
def collectData(responseDict, outputDict, scroll_size):
  for i in range(scroll_size):
    sourceDict = responseDict['hits']['hits'][i]['_source']
    key = sourceDict['ads_keyword']     
    val = len(sourceDict['ads'])
    if key in outputDict:
      # if it's true, append
      outputDict[key].append(val)
    else: 
      # else define as empty list
      outputDict[key] = []

def calKey2Avg(inDic, outDic):
  for k in inDic.keys():
    # the number of elements in a list
    n = len(inDic[k])
    # sum up
    s = sum(inDic[k])
    if (n >= 1):
      avg = s/n
      if (avg >= 2):
        outDic[k] = avg

def log(msg):
  print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

'''=================== self-defined functions end ==================='''

'''=================== global variables start ==================='''
# connection to ELK
esCluster = Elasticsearch(
    hosts = ['192.168.21.20'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

interval = sys.argv[1]
filename = 'k2avgAd_last'+str(interval)+'.csv'
fromdate = 'now-'+str(interval)
# log(filename)

# max_slice = 5 
'''=================== global variables end ==================='''


# initiate the variable 
body = {
          "_source" : ["ads_keyword"],
          "query": {
            "bool":{
              "must": [
                {
                  "match_all": {}
                },
                {
                  "range": {
                    "updated": {
                      "gte": fromdate,
                      "lte": "now"
                    }
                  }
                }
              ]            
            }
          },
          "script_fields" : {
            "ads_count" : {
              "script" : "params['_source']['ads'].length"
            },
            "ads_keyword":{
              "script" : "params['_source']['ads_keyword']"
            }
          }          
}

# res = esCluster.search(index="ads", doc_type="ad", scroll="3m", search_type="query_then_fetch", size=scroll_size, body=body, request_timeout=300)
res = helpers.scan(
        client = esCluster,
        scroll = '20m',
        size = 5000,
        query = body,
        index = 'ads',
        doc_type= "ad",
        request_timeout = 5000
)
log('type returned from helpers.scan: '+str(type(res)))

# count = 0
# for doc in res:
#   count+=1
#   # print(type(doc))
#   # print(doc.keys())
#   key = doc['_source']['ads_keyword']
#   val = doc['fields']['ads_count'][0]
#   print(key+','+str(val))
#   if (count > 10):
#     sys.exit()

# sys.exit()

# print(len(res))
outDic = {}
count = 0
for doc in res:
  count+=1
  key = doc['_source']['ads_keyword']
  val = doc['fields']['ads_count'][0]
  if key in outDic:
    # if it's true, append
    outDic[key].append(val)
  else: 
    # else define as empty list
    outDic[key] = []
  # if (count > 1000):
  #   log('stop here!')
  #   sys.exit()
  # else:
  #   log(key+','+str(val))  
  if (count % 10000 ==0):  
    log('number of doc processed: '+str(count))
    log('number of keyword: '+str(len(outDic)))

log('total number of doc processed: '+str(count))
log('total number of keyword before calculation: '+str(len(outDic)))
# log("Got %d Hits:" % res['hits']['total'])
# sys.exit()


# calculate the avg
key2avg = {}
calKey2Avg(outDic, key2avg)

sortedKeys = sorted(key2avg, key=key2avg.get, reverse=True)
with open(filename, "w") as file:
  for i in sortedKeys:
    file.write(i + ",%.2f" % key2avg[i] + "\n")

log('total number of keyword after calculation: '+str(len(key2avg)))
